/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.validator;

import org.joda.time.LocalDate;

import org.bootcamp.yum.exception.ApiException;

public class PathParamValidator {

    public static int[] validateWeekYear(String weekYear) throws ApiException {

        int[] weekYearIntArray = new int[2];
        String[] weekYearStringArray = null;

        try {
            // Check format WW-YYYY
            if (!weekYear.matches("\\d{2}-\\d{4}")) {
                throw new ApiException(400, "Bad request");
            }
            // Split weekYear string
            weekYearStringArray = weekYear.split("-");

            // Put week in weekYearIntArray[0]
            weekYearIntArray[0] = Integer.parseInt(weekYearStringArray[0]);

            // Put year in weekYearIntArray[1]
            weekYearIntArray[1] = Integer.parseInt(weekYearStringArray[1]);

            // Validate week range [1-52/53]
            LocalDate lastMondayOfYear = new LocalDate().withYear(weekYearIntArray[1]).withDayOfWeek(1).weekOfWeekyear().withMaximumValue();
            if (weekYearIntArray[0] < 1 || weekYearIntArray[0] > lastMondayOfYear.getWeekOfWeekyear()) {
                throw new ApiException(400, "Bad request");
            }

        } catch (NumberFormatException e) {
            throw new ApiException(400, "Bad request");
        }

        return weekYearIntArray;
    }

    public static int[] validateMonthYear(String monthYear) throws ApiException {

        int[] monthYearIntArray = new int[2];
        String[] monthYearStringArray = null;

        try {
            // Check format MM-YYYY
            if (!monthYear.matches("\\d{2}-\\d{4}")) {
                throw new ApiException(400, "Bad request");
            }
            // Split monthYear string
            monthYearStringArray = monthYear.split("-");

            // Put month in monthYearIntArray[0]
            monthYearIntArray[0] = Integer.parseInt(monthYearStringArray[0]);

            // Put year in monthYearIntArray[1]
            monthYearIntArray[1] = Integer.parseInt(monthYearStringArray[1]);

            // Validate month range [1-12]
            if (monthYearIntArray[0] < 1 || monthYearIntArray[0] > 12) {
                throw new ApiException(400, "Bad request");
            }

        } catch (NumberFormatException e) {
            throw new ApiException(400, "Bad request");
        }

        return monthYearIntArray;
    }

}
