/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 *
 * @author user
 */
public class CheckCurrencyValidator implements ConstraintValidator<CheckCurrency, String> {

    @Override
    public void initialize(CheckCurrency a) {

    }

    @Override
    public boolean isValid(String currency, ConstraintValidatorContext cvc) {

        return currency.equals("&euro;") || currency.equals("&dollar;")
                || currency.equals("&pound;") || currency.equals("&yen;")
                || currency.equals("&#8381;") || currency.equals("&#8377;")
                || currency.equals("&#20803;");
    }

}
