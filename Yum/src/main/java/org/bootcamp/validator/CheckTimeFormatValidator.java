/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.joda.time.LocalTime;

/**
 *
 * @author user
 */
public class CheckTimeFormatValidator implements ConstraintValidator<CheckTimeFormat, String> {

    @Override
    public void initialize(CheckTimeFormat a) {

    }

    @Override
    public boolean isValid(String t, ConstraintValidatorContext cvc) {
        try {
            LocalTime.parse(t);
            return true;
        } catch (java.lang.IllegalArgumentException ex) {
            return false;
        }
    }

}
