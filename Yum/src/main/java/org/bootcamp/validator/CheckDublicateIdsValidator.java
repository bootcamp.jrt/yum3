/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.validator;

import java.util.List;
import java.util.ArrayList;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.bootcamp.yum.api.model.OrderItem;

/**
 *
 * @author user
 */
public class CheckDublicateIdsValidator implements ConstraintValidator<CheckDublicateIds, List<OrderItem>> {

    @Override
    public void initialize(CheckDublicateIds constraintAnnotation) {
    }

    @Override
    public boolean isValid(List<OrderItem> orderItems, ConstraintValidatorContext constraintContext) {
        // keep the food ids to check for dublicates
        ArrayList<Long> foodIds = new ArrayList<>();
        for (OrderItem orderItem : orderItems) {
            if (foodIds.contains(orderItem.getFoodId())) {
                return false;
            }
            foodIds.add(orderItem.getFoodId());
        }
        return true;
    }

}
