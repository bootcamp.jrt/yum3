/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.exception;

import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import org.bootcamp.yum.api.model.Error;
import org.bootcamp.yum.api.model.ErrorWithDto;

/*
 * That class is responsible for handling all exceptions happening before or during the handling of an API call.
 */
@ControllerAdvice
public class ExceptionControllerAdvice extends ResponseEntityExceptionHandler {

    // The Exception HttpMessageNotReadableException happens before we get access to a controller, 
    // and is caused by malformed json in the request body. We override the default handler for this
    // exception to response with the appropriate message.
    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        return handleApiException(new ApiException(400, "Bad Request"));
    }

    @ExceptionHandler(value = {AccessDeniedException.class})
    protected ResponseEntity<Object> handleAccessDeniedException(AccessDeniedException ex) {
        Error e = new Error();
        e.setError("403");
        e.setMessage("Access Denied");
        return new ResponseEntity<>(e, HttpStatus.FORBIDDEN);
    }

    // The Exception MethodArgumentTypeMismatchException happens before we get access to a controller, 
    // during the convertion of a string from a path parameter into a long or int.
    // For example /foods/{id}. Param {id} is supposed to be a long and if we give a String
    // the code will break before we are called inside the controller.
    // In that case we have to prevent the error to bubble up to the default Exception Handler of Spring. This is the way to handle it
    // That default handler displays way too much information to the API end user. 
    @ExceptionHandler(value = {MethodArgumentTypeMismatchException.class})
    protected ResponseEntity<Object> handleBadRequest(RuntimeException ex) {
        return handleApiException(new ApiException(400, "Invalid parameter in URL"));
    }

    @ExceptionHandler(value = {ConcurrentCreationException.class})
    protected ResponseEntity<Object> handleConcurrentCreationException(ConcurrentCreationException ex) {
        ErrorWithDto e = new ErrorWithDto();
        e.setError("" + ex.getCode());
        e.setMessage(ex.getMessage());
        e.setDto(ex.getDto());
        return new ResponseEntity<>(e, HttpStatus.valueOf(ex.getCode()));
    }

    @ExceptionHandler(value = {ConcurrentModificationException.class})
    protected ResponseEntity<Object> handleConcurrentModificationException(ConcurrentModificationException ex) {
        return new ResponseEntity<>(ex.getDto(), HttpStatus.valueOf(ex.getCode()));
    }

    @ExceptionHandler(value = {ConcurrentDeletionException.class})
    protected ResponseEntity<Object> handleConcurrentDeletionException(ConcurrentDeletionException ex) {
        ErrorWithDto e = new ErrorWithDto();
        e.setError("" + ex.getCode());
        e.setMessage(ex.getMessage());
        e.setDto(ex.getDto());
        return new ResponseEntity<>(e, HttpStatus.valueOf(ex.getCode()));
    }

    @ExceptionHandler(value = {ApiException.class})
    protected ResponseEntity<Object> handleApiException(ApiException ex) {
        if (ex.getFieldError() == null) {
            Error e = new Error();
            e.setError("" + ex.getCode());
            e.setMessage(ex.getMessage());
            return new ResponseEntity<>(e, HttpStatus.valueOf(ex.getCode()));
        } else {
            ErrorWithDto e = new ErrorWithDto();
            e.setError("" + ex.getCode());
            e.setMessage(ex.getMessage());
            e.setDto(ex.getFieldError());
            return new ResponseEntity<>(e, HttpStatus.valueOf(ex.getCode()));
        }

    }

    @ExceptionHandler(value = {Exception.class})
    protected ResponseEntity<Object> handleException(Exception ex) {
        ex.printStackTrace();
        Error e = new Error();
        e.setError("500");
        e.setMessage("Internal Server Error");
        return new ResponseEntity<>(e, HttpStatus.INTERNAL_SERVER_ERROR);
    }

}
