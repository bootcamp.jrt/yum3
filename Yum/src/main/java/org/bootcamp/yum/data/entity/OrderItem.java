/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.data.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EmbeddedId;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.Table;

@Entity
@Table(name = "order_item")
public class OrderItem implements Serializable {

    @EmbeddedId
    private OrderItemKey key;

    @Column(name = "quantity")
    private int quantity;

    //multiplicity relationship with DailyOrder
    @ManyToOne
    @JoinColumn(name = "daily_order_id", insertable = false, updatable = false)
    private DailyOrder dailyOrder;

    //method related to multiplicity with DailyOrder
    public DailyOrder getDailyOrder() {
        return dailyOrder;
    }

    //multiplicity relationship with Food
    @ManyToOne
    @JoinColumn(name = "food_id", insertable = false, updatable = false)
    private Food food;

    public OrderItem() {
    }

    public OrderItem(OrderItemKey key, int quantity) {
        this.key = key;
        this.quantity = quantity;
    }

    public OrderItem(Food food, int quantity) {
        this.quantity = quantity;
        this.food = food;
    }

    //method related to multiplicity with Food
    public Food getFood() {
        return food;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setKey(OrderItemKey key) {
        this.key = key;
    }

    public void setDailyOrder(DailyOrder dailyOrder) {
        this.dailyOrder = dailyOrder;
    }

    public void setFood(Food food) {
        this.food = food;
    }

    @PrePersist
    private void prePersist() {
        OrderItemKey orderItemKey = new OrderItemKey();
        orderItemKey.setDailyOrderId(this.dailyOrder.getId());
        orderItemKey.setFoodId(this.food.getId());
        this.setKey(orderItemKey);
    }

    public org.bootcamp.yum.api.model.OrderItem getOrderItemDTO() {
        return new org.bootcamp.yum.api.model.OrderItem(this.food.getId(), this.quantity);
    }

    @Override
    public String toString() {
        return "OrderItem{" + "key=" + key + ", quantity=" + quantity + ", food=" + food + '}';
    }

}
