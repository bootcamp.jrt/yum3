/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.data.entity;

import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.util.Objects;
import java.util.Iterator;
import java.util.ArrayList;

import javax.persistence.Convert;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;
import org.joda.time.LocalDateTime;

import org.springframework.transaction.annotation.Transactional;

import org.bootcamp.converter.LocalDateAttributeConverter;
import org.bootcamp.enums.FoodEnum;
import org.bootcamp.yum.api.model.chef.DailyMenuChef;
import org.bootcamp.yum.api.model.chef.GroupedFoods;

@Entity
@Table(name = "daily_menu")
public class DailyMenu {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "of_date")
    @Convert(converter = LocalDateAttributeConverter.class)
    private LocalDate ofDate;

    @Column(name = "expired")
    private boolean expired;

    @Version
    @Column(name = "version")
    private int version;

    public DailyMenu() {
    }

    public DailyMenu(long id) {
        this.id = id;
    }

    //multiplicity relationship with DailyOrder
    @OneToMany(mappedBy = "dailyMenu")
    private List<DailyOrder> orders;

    //method related to multiplicity with DailyOrder
    public List<DailyOrder> getDailyOrders() {
        if (this.orders == null) {
            orders = new ArrayList<>();
        }
        return orders;
    }

    //method related to multiplicity with DailyOrder
    public void addDailyOrder(DailyOrder order) {
        if (this.orders == null) {
            orders = new ArrayList<>();
        }
        orders.add(order);
    }

    //multiplicity relationship with Food
    @ManyToMany(mappedBy = "dailyMenus")
    private List<Food> foods;

    //method related to multiplicity with Food
    public List<Food> getFoods() {
        if (this.foods == null) {
            foods = new ArrayList<>();
        }
        return foods;
    }

    public List<Long> getFoodsIds() {
        List<Long> foodsIds = new ArrayList<>();
        for (Food food : foods) {
            foodsIds.add(food.getId());
        }
        return foodsIds;
    }

    //method related to multiplicity with Food
    public void addFood(Food food) {
        if (this.foods == null) {
            foods = new ArrayList<>();
        }
        foods.add(food);
    }

    public void removeFood(Food food) {
        foods.remove(food);
    }

    public void removeAllFoods() {
        foods.clear();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDate getOfDate() {
        return ofDate;
    }

    public void setOfDate(LocalDate ofDate) {
        this.ofDate = ofDate;
    }

    @Transactional
    public boolean isExpired(LocalTime yumDeadline) {

        if (expired) {
            return expired;
        } else {
            LocalDateTime dailyMenuExpirationDate
                    = new LocalDateTime(this.ofDate.minusDays(1).getYear(), this.ofDate.minusDays(1).getMonthOfYear(),
                            this.ofDate.minusDays(1).getDayOfMonth(), yumDeadline.getHourOfDay(), yumDeadline.getMinuteOfHour());
            if (LocalDateTime.now().isAfter(dailyMenuExpirationDate)) {
                expired = true;
            }
        }
        return expired;
    }

    public void setExpired(boolean expired) {
        this.expired = expired;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public DailyMenuChef getDailyMenuChefDTO(LocalTime yumDeadline) {

        DailyMenuChef dailyMenuChef = new DailyMenuChef();

        dailyMenuChef.setId(this.getId());
        dailyMenuChef.setDate(this.getOfDate());
        dailyMenuChef.setVersion(this.getVersion());
        dailyMenuChef.setFinal(this.isExpired(yumDeadline));

        List<org.bootcamp.yum.api.model.OrderItem> foods = new ArrayList<org.bootcamp.yum.api.model.OrderItem>();

        // Iterate Foods of DailyMenuDAO
        for (Food foodDAO : this.getFoods()) {

            // Initially set food's ordered quantity to 0
            int quantity = 0;

            // Iterate DailyMenu's DailyOrders
            for (DailyOrder dailyOrderDAO : this.getDailyOrders()) {

                // Iterate DailyOrder's Order Items
                for (org.bootcamp.yum.data.entity.OrderItem orderItem : dailyOrderDAO.getOrderItems()) {

                    if (orderItem.getFood().getId() == foodDAO.getId()) {
                        // Set ordered quantity
                        quantity += orderItem.getQuantity();
                    }
                }
            }

            // Add food to foods list
            foods.add(new org.bootcamp.yum.api.model.OrderItem(foodDAO.getId(), quantity));
        }

        dailyMenuChef.foods(foods);

        return dailyMenuChef;

    }

    // Returns an unordered DailyMenuDTO (all food quantities are zero)
    public org.bootcamp.yum.api.model.hungry.DailyMenu getUnOrderedMenuDTO(LocalTime yumDeadline) {
        org.bootcamp.yum.api.model.hungry.DailyMenu menuDTO = new org.bootcamp.yum.api.model.hungry.DailyMenu();
        menuDTO.setId(this.getId());
        menuDTO.setDate(this.getOfDate());
        menuDTO.setFinal(this.isExpired(yumDeadline));
        menuDTO.setVersion(this.getVersion());
        // Set order id initially to -1
        menuDTO.setOrderId(-1L);
        // Set order version initially to -1
        menuDTO.setOrderVersion(-1);
        // Set the menu's food items without quantity
        menuDTO.setFoods(getFoodDTOList(null));
        return menuDTO;
    }

    public org.bootcamp.yum.api.model.hungry.DailyMenu getOrderedMenuDTO(long userId, LocalTime yumDeadline) {
        org.bootcamp.yum.api.model.hungry.DailyMenu menuDTO = this.getUnOrderedMenuDTO(yumDeadline);
        // Set user's OrderItemsDAOList initially to null
        List<OrderItem> userOrderItemsDAOList = null;
        // Iterate dailyOrdersEntityList
        for (DailyOrder dailyOrderDAO : this.getDailyOrders()) {
            if (dailyOrderDAO.getUser().getId() == userId) {
                // User has made a dailyOrder for that dailyMenu
                menuDTO.setOrderId(dailyOrderDAO.getId());
                menuDTO.setOrderVersion(dailyOrderDAO.getVersion());
                // Get the user's ordered items
                userOrderItemsDAOList = dailyOrderDAO.getOrderItems();
                break;
            }
        }
        // Set foods (call getFoodsModelList passing dailyMenu foods and user's ordered items to get quantity)
        menuDTO.setFoods(getFoodDTOList(userOrderItemsDAOList));
        return menuDTO;

    }

    public List<org.bootcamp.yum.api.model.chef.Food> getFoodDTOList(List<OrderItem> userOrderItemsDAOList) {
        // Create a new foodsModelList
        List<org.bootcamp.yum.api.model.chef.Food> foodsDTOList = new ArrayList<>();

        for (Food foodEntity : this.getFoods()) {
            // Create a new foodModel
            org.bootcamp.yum.api.model.chef.Food foodDTO = new org.bootcamp.yum.api.model.chef.Food();
            // Set id
            foodDTO.setId(foodEntity.getId());
            // Set foodType
            foodDTO.setFoodType(foodEntity.getType().name());
            // Set setName
            foodDTO.setName(foodEntity.getName());
            // Set Description
            foodDTO.setDescription(foodEntity.getDescription());
            // Set price
            foodDTO.setPrice(foodEntity.getPrice());
            // Set quantity initially to 0
            foodDTO.setQuantity(0);
            // Iterate userOrderItemsEntityList
            if (userOrderItemsDAOList != null) {
                for (OrderItem orderItem : userOrderItemsDAOList) {
                    if (orderItem.getFood().getId() == foodEntity.getId()) {
                        // User has ordered that food
                        // Set ordered quantity
                        foodDTO.setQuantity(orderItem.getQuantity());
                    }
                }
            }
            // Add to foodsModelList
            foodsDTOList.add(foodDTO);
        }

        return foodsDTOList;
    }

    public org.bootcamp.yum.api.model.chef.DailyOrders getDailyOrdersDTO() {
        // A map to store the food type by id
        HashMap<Long, FoodEnum> foodTypeMap = new HashMap<>();
        // A map to store the total quantity by food id
        HashMap<Long, Integer> foodQuantityMap = new HashMap<>();
        List<org.bootcamp.yum.api.model.chef.DailyOrder> orderDTOList = new ArrayList<org.bootcamp.yum.api.model.chef.DailyOrder>();
        for (DailyOrder order : this.orders) {
            // Add OrderDTO to list
            orderDTOList.add(order.getOrderDTO());
            order.getOrderItems().forEach((v) -> {
                if (foodQuantityMap.containsKey(v.getFood().getId())) {
                    foodQuantityMap.put(v.getFood().getId(), foodQuantityMap.get(v.getFood().getId()) + v.getQuantity());
                } else {
                    foodQuantityMap.put(v.getFood().getId(), v.getQuantity());
                }
                foodTypeMap.put(v.getFood().getId(), v.getFood().getType());
            });
        }
        // Instantiate GroupedFoods DTO per type
        GroupedFoods mainFoods = new GroupedFoods(FoodEnum.MAIN.toString());
        GroupedFoods salads = new GroupedFoods(FoodEnum.SALAD.toString());
        GroupedFoods drinks = new GroupedFoods(FoodEnum.DRINK.toString());
        Iterator iter = foodQuantityMap.entrySet().iterator();
        // Iterate through foods and gropup them by type
        while (iter.hasNext()) {
            Map.Entry pair = (Map.Entry) iter.next();
            long id = (long) pair.getKey();
            FoodEnum type = foodTypeMap.get(id);
            switch (type) {
                case MAIN:
                    mainFoods.addOrderItemsItem(new org.bootcamp.yum.api.model.OrderItem(id, foodQuantityMap.get(id)));
                    break;
                case SALAD:
                    salads.addOrderItemsItem(new org.bootcamp.yum.api.model.OrderItem(id, foodQuantityMap.get(id)));
                    break;
                case DRINK:
                    drinks.addOrderItemsItem(new org.bootcamp.yum.api.model.OrderItem(id, foodQuantityMap.get(id)));
                    break;
                default:
                    break;
            }
        }
        org.bootcamp.yum.api.model.chef.DailyOrders ordersDTO = new org.bootcamp.yum.api.model.chef.DailyOrders();
        if (!mainFoods.getOrderItems().isEmpty()) {
            ordersDTO.addSummaryItem(mainFoods);
        }
        if (!salads.getOrderItems().isEmpty()) {
            ordersDTO.addSummaryItem(salads);
        }
        if (!drinks.getOrderItems().isEmpty()) {
            ordersDTO.addSummaryItem(drinks);
        }
        //Set the order list
        ordersDTO.setOrders(orderDTOList);
        return ordersDTO;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 67 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 67 * hash + Objects.hashCode(this.ofDate);
        hash = 67 * hash + (this.expired ? 1 : 0);
        hash = 67 * hash + this.version;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        return obj.hashCode() == hashCode();
    }

    @Override
    public String toString() {
        return "DailyMenu{" + "id=" + id + ", ofDate=" + ofDate + ", expired=" + expired + ", version=" + version + '}';
    }

}
