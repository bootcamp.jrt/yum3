/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.data.entity;

import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.imageio.ImageIO;
import javax.persistence.Basic;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Entity;
import javax.persistence.Column;
import javax.persistence.Convert;
import static javax.persistence.FetchType.LAZY;
import javax.persistence.Version;
import javax.persistence.OneToMany;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Lob;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;

import net.coobird.thumbnailator.Thumbnails;
import net.coobird.thumbnailator.geometry.Positions;

import org.springframework.security.crypto.bcrypt.BCrypt;

import org.bootcamp.enums.UserRoleEnum;
import org.bootcamp.yum.exception.ApiException;
import org.bootcamp.converter.UserRoleConverter;
import org.bootcamp.yum.api.model.AccountSettings;
import org.bootcamp.converter.LocalDateTimeConverter;
import org.bootcamp.yum.api.model.admin.UpdateAccountSettings;
import org.bootcamp.converter.LocalDateAttributeConverter;

@Entity
@Table(name = "user")
public class User implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "email")
    private String email;

    @Column(name = "first_name")
    private String firstName;

    @Column(name = "last_name")
    private String lastName;

    @Column(name = "role")
    @Convert(converter = UserRoleConverter.class)
    private UserRoleEnum role;

    @Column(name = "password")
    private String password;

    @Column(name = "registration_date")
    @Convert(converter = LocalDateAttributeConverter.class)
    private LocalDate registrationDate;

    @Basic(fetch = LAZY)
    @Lob
    @Column(name = "picture")
    private byte[] picture;

    @Column(name = "approved")
    private boolean approved;

    @Column(name = "secret")
    private String secret;

    @Column(name = "secret_creation")
    @Convert(converter = LocalDateTimeConverter.class)
    private LocalDateTime secretCreation;

    @Version
    @Column(name = "version")
    private int version;

    //multiplicity relationship with DailyOrder
    @OneToMany(mappedBy = "user")
    private List<DailyOrder> orders;

    public User() {
    }

    public User(long id) {
        this.id = id;
    }

    public User(String firstName, String lastName, String email, String password, String role) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = BCrypt.hashpw(password.trim(), BCrypt.gensalt());
        this.role = UserRoleEnum.valueOf(role);
        this.registrationDate = LocalDate.now();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public UserRoleEnum getRole() {
        return role;
    }

    public void setRole(UserRoleEnum role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LocalDate getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(LocalDate registrationDate) {
        this.registrationDate = registrationDate;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    public boolean hasPicture() {
        return (this.picture != null && this.picture.length > 0);
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    public LocalDateTime getSecretCreation() {
        return secretCreation;
    }

    public void setSecretCreation(LocalDateTime secretCreation) {
        this.secretCreation = secretCreation;
    }

    public int getVersion() {
        return version;
    }

    //should be removed???
    public void setVersion(int version) {
        this.version = version;
    }

    //method related to multiplicity DailyOrder
    public List<DailyOrder> getDailyOrders() {
        return orders;
    }

    //method related to multiplicity DailyOrder
    public void addDailyOrder(DailyOrder order) {
        if (this.orders == null) {
            orders = new ArrayList<>();
        }
        orders.add(order);
    }

    // enum for representing the state of the user's order list
    public enum OrdersStatus {
        NO_ORDERS, ONLY_NOT_FINAL, MIXED, ONLY_FINAL
    }

    // get the state of the user's order list
    public OrdersStatus getOrdersStatus(LocalTime yumDeadline) {

        if (this.orders.isEmpty()) {
            return OrdersStatus.NO_ORDERS;
        }

        boolean hasFinal = false;
        boolean hasNotFinal = false;

        for (DailyOrder order : this.orders) {
            if (order.isFinalOrder(yumDeadline)) {
                hasFinal = true;
                if (hasNotFinal) {
                    return OrdersStatus.MIXED;
                }
            } else {
                hasNotFinal = true;
                if (hasFinal) {
                    return OrdersStatus.MIXED;
                }
            }
        }
        return (hasFinal) ? OrdersStatus.ONLY_FINAL : OrdersStatus.ONLY_NOT_FINAL;
    }

    // get the user's orders that are not final
    public List<DailyOrder> getNonFinalOrders(LocalTime yumDeadline) {
        ArrayList<DailyOrder> nonFinalOrders = new ArrayList<>();
        for (DailyOrder order : this.orders) {
            if (!order.isFinalOrder(yumDeadline)) {
                nonFinalOrders.add(order);
            }
        }
        return nonFinalOrders;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 97 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 97 * hash + Objects.hashCode(this.email);
        hash = 97 * hash + Objects.hashCode(this.firstName);
        hash = 97 * hash + Objects.hashCode(this.lastName);
        hash = 97 * hash + Objects.hashCode(this.role);
        hash = 97 * hash + Objects.hashCode(this.password);
        hash = 97 * hash + Objects.hashCode(this.registrationDate);
        hash = 97 * hash + Arrays.hashCode(this.picture);
        hash = 97 * hash + (this.approved ? 1 : 0);
        hash = 97 * hash + Objects.hashCode(this.secret);
        hash = 97 * hash + Objects.hashCode(this.secretCreation);
        hash = 97 * hash + this.version;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        return obj.hashCode() == hashCode();
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", email=" + email + ", firstName=" + firstName + ", lastName=" + lastName + ", role=" + role + ", password=" + password + ", registrationDate=" + registrationDate + ", picture=" + picture + ", approved=" + approved + ", secret=" + secret + ", secretCreation=" + secretCreation + ", version=" + version + '}';
    }

    //That method populates the user's accounts settings
    public AccountSettings populateAccountSettings() {

        return new AccountSettings(
                this.firstName,
                this.lastName,
                this.email,
                this.role.toString(),
                this.hasPicture(),
                this.approved,
                this.version);
    }

    //That metdod updates the user's account settings
    public void updateAccountSettings(UpdateAccountSettings updatedAccountSettings) throws ApiException {
        this.firstName = updatedAccountSettings.getFirstName().trim();
        this.lastName = updatedAccountSettings.getLastName().trim();
        this.email = updatedAccountSettings.getEmail().trim();
        this.role = UserRoleEnum.valueOf(updatedAccountSettings.getRole().trim().toUpperCase());
    }

    public void addPicture(byte[] pictureInBytes) throws ApiException {
        try {
            // get the inpustream of the uploaded picture bytes
            InputStream in = new ByteArrayInputStream(pictureInBytes);
            BufferedImage image = ImageIO.read(in);
            int min = 0;

            if (image.getWidth() > image.getHeight()) {
                min = image.getHeight();
            } else {
                min = image.getWidth();
            }

            //
            ByteArrayOutputStream output = new ByteArrayOutputStream();
            //resize, crop the image and exported as outputStream
            Thumbnails.of(image)
                    .sourceRegion(Positions.CENTER, min, min)
                    .size(180, 180)
                    .outputFormat("jpeg")
                    .toOutputStream(output);

            byte[] outBytes = output.toByteArray();
            setPicture(outBytes);
        } catch (IOException ex) {
            throw new ApiException(400, "Bad request");
        }

    }

}
