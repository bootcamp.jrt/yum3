/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.data.entity;

import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Table;
import javax.persistence.Version;

import org.joda.time.LocalTime;

import org.bootcamp.converter.LocalTimeConverter;
import org.bootcamp.yum.exception.ApiException;
import org.bootcamp.yum.exception.ConcurrentModificationException;
import org.bootcamp.yum.api.model.GlobalSettings;

@Entity
@Table(name = "yum")
public class Yum {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "terms")
    private String terms;

    @Column(name = "privacy")
    private String privacy;

    @Column(name = "notes")
    private String notes;

    @Column(name = "deadline")
    @Convert(converter = LocalTimeConverter.class)
    private LocalTime deadline;

    @Column(name = "currency")
    private String currency;

    @Version
    @Column(name = "version")
    private int version;

    @Column(name = "foods_version")
    private int foodsVersion;

    public Yum() {
    }

    public Yum(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    public String getPrivacy() {
        return privacy;
    }

    public void setPrivacy(String privacy) {
        this.privacy = privacy;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public LocalTime getDeadline() {
        return deadline;
    }

    public void setDeadline(LocalTime deadline) {
        this.deadline = deadline;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public int getFoodsVersion() {
        return foodsVersion;
    }

    public void setFoodsVersion(int foodsVersion) {
        this.foodsVersion = foodsVersion;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 97 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 97 * hash + Objects.hashCode(this.terms);
        hash = 97 * hash + Objects.hashCode(this.privacy);
        hash = 97 * hash + Objects.hashCode(this.notes);
        hash = 97 * hash + Objects.hashCode(this.deadline);
        hash = 97 * hash + Objects.hashCode(this.currency);
        hash = 97 * hash + this.version;
        hash = 97 * hash + this.foodsVersion;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        return obj.hashCode() == hashCode();
    }

    @Override
    public String toString() {
        return "Yum{" + "id=" + id + ", terms=" + terms + ", privacy=" + privacy + ", notes=" + notes + ", deadline=" + deadline + ", currency=" + currency + ", version=" + version + ", foodsVersion=" + foodsVersion + '}';
    }

    public GlobalSettings generateGlobalSettings() {
        return new GlobalSettings(deadline.toString(),
                currency.toString(),
                notes,
                terms,
                privacy,
                version);
    }

    public void updateGlobalSettings(GlobalSettings updatedGlobalSettings) throws ApiException {
        if (updatedGlobalSettings.getVersion() != this.version) {
            throw new ConcurrentModificationException(this.generateGlobalSettings());
        }
        this.deadline = LocalTime.parse(updatedGlobalSettings.getDeadline());
        this.currency = updatedGlobalSettings.getCurrency().trim();
        this.notes = updatedGlobalSettings.getNotes();
        this.terms = updatedGlobalSettings.getTerms();
        this.privacy = updatedGlobalSettings.getPrivacy();

    }

}
