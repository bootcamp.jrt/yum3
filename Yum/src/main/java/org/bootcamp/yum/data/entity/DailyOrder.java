/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.data.entity;

import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;
import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Id;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import org.joda.time.LocalTime;

import org.springframework.transaction.annotation.Transactional;

import org.bootcamp.enums.FoodEnum;
import org.bootcamp.yum.api.model.chef.GroupedFoods;

@Entity
@Table(name = "daily_order")
public class DailyOrder implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @Column(name = "final")
    private boolean finalOrder;
    @Version
    @Column(name = "version")
    private int version;

    //multiplicity relationship with User
    @ManyToOne
    private User user;

    //multiplicity relationship with DailyMenu
    @ManyToOne
    private DailyMenu dailyMenu;

    //multiplicity relationship with OrderItem
    @OneToMany(mappedBy = "dailyOrder", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<OrderItem> orderItems;

    public DailyOrder(User user, DailyMenu dailyMenu, List<OrderItem> orderItems) {
        this.user = user;
        this.dailyMenu = dailyMenu;
        this.finalOrder = false;
        //Populate order items
        for (OrderItem item : orderItems) {
            item.setDailyOrder(this);
            this.addOrderItem(item);
        }
    }

    public DailyOrder() {
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Transactional
    public boolean isFinalOrder(LocalTime yumDeadline) {
        if (this.finalOrder) {
            return finalOrder;
        } else {
            if (dailyMenu.isExpired(yumDeadline)) {
                this.finalOrder = true;
            }
            return this.finalOrder;
        }
    }

    public void setFinalOrder(boolean finalOrder) {
        this.finalOrder = finalOrder;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    //method related to multiplicity with User
    public User getUser() {
        return user;
    }

    //method related to multiplicity with User
    public void setUser(User userInfo) {
        user = userInfo;
    }

    //method related to multiplicity with DailyMenu
    public DailyMenu getDailyMenu() {
        return dailyMenu;
    }

    //method related to multiplicity with DailyMenu
    public void setDailyMenu(DailyMenu dailyMenu) {
        this.dailyMenu = dailyMenu;
    }

    //method related to multiplicity with OrderItem
    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    //method related to multiplicity with OrderItem
    public void addOrderItem(OrderItem orderItem) {
        if (this.orderItems == null) {
            orderItems = new ArrayList<>();
        }
        orderItems.add(orderItem);
    }

    public void updateItems(List<OrderItem> newItems) {

        //Manual increment of the optimistic lock field,  in order to trigger an update command for this order.
        //This value will be ignored and replaced by the initial version retrieved by the database.
        //this.setVersion(this.getVersion() + 1);
        this.setVersion(this.getVersion() + 1);
        //Temporal helper lists
        ArrayList<OrderItem> retainedItems = new ArrayList<>();
        ArrayList<OrderItem> dublicateItems = new ArrayList<>();
        //Iterate through new items
        Iterator iter = newItems.iterator();
        while (iter.hasNext()) {
            OrderItem newItem = (OrderItem) iter.next();
            newItem.setDailyOrder(this);
            for (OrderItem existedItem : this.orderItems) {
                if (newItem.getFood().getId() == existedItem.getFood().getId()) {
                    existedItem.setQuantity(newItem.getQuantity());
                    retainedItems.add(existedItem);
                    dublicateItems.add(newItem);
                }
            }
        }
        this.orderItems.retainAll(retainedItems);
        newItems.removeAll(dublicateItems);
        this.orderItems.addAll(newItems);
    }

    public boolean isOwnedBy(User user) {
        return user.getId() == this.user.getId();
    }

    public org.bootcamp.yum.api.model.hungry.DailyMenu getOrderedMenuDto(LocalTime deadline) {
        org.bootcamp.yum.api.model.hungry.DailyMenu menuDTO = this.getDailyMenu().getUnOrderedMenuDTO(deadline);
        menuDTO.setOrderId(this.getId());
        menuDTO.setOrderVersion(this.getVersion());
        menuDTO.setFoods(this.getDailyMenu().getFoodDTOList(this.getOrderItems()));
        return menuDTO;
    }

    public org.bootcamp.yum.api.model.chef.DailyOrder getOrderDTO() {
        // Instantiate GroupedFoods DTO per type
        GroupedFoods mainFoods = new GroupedFoods(FoodEnum.MAIN.toString());
        GroupedFoods salads = new GroupedFoods(FoodEnum.SALAD.toString());
        GroupedFoods drinks = new GroupedFoods(FoodEnum.DRINK.toString());
        // Create new DailyOrderDTO
        org.bootcamp.yum.api.model.chef.DailyOrder orderDTO = new org.bootcamp.yum.api.model.chef.DailyOrder();
        orderDTO.setFirstName(this.user.getFirstName());
        orderDTO.setLastName(this.user.getLastName());
        for (OrderItem item : this.orderItems) {
            FoodEnum type = item.getFood().getType();
            switch (type) {
                case MAIN:
                    mainFoods.addOrderItemsItem(item.getOrderItemDTO());
                    break;
                case SALAD:
                    salads.addOrderItemsItem(item.getOrderItemDTO());
                    break;
                case DRINK:
                    drinks.addOrderItemsItem(item.getOrderItemDTO());
                    break;
                default:
                    break;
            }
        }
        if (!mainFoods.getOrderItems().isEmpty()) {
            orderDTO.addFoodsItem(mainFoods);
        }
        if (!salads.getOrderItems().isEmpty()) {
            orderDTO.addFoodsItem(salads);
        }
        if (!drinks.getOrderItems().isEmpty()) {
            orderDTO.addFoodsItem(drinks);
        }
        return orderDTO;
    }

    public org.bootcamp.yum.api.model.Version getVersionDTO() {
        org.bootcamp.yum.api.model.Version version = new org.bootcamp.yum.api.model.Version();
        version.setVersion(this.getVersion());
        return version;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 71 * hash + (int) (this.id ^ (this.id >>> 32));
        hash = 71 * hash + (this.finalOrder ? 1 : 0);
        hash = 71 * hash + this.version;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        return obj.hashCode() == hashCode();
    }

    @Override
    public String toString() {
        return "DailyOrder{" + "id=" + id + ", finalOrder=" + finalOrder + ", version=" + version + '}';
    }

}
