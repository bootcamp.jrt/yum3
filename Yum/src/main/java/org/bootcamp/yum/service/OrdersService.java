/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.service;

import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDate;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import org.bootcamp.yum.exception.ApiException;
import org.bootcamp.yum.exception.ConcurrentCreationException;
import org.bootcamp.yum.exception.ConcurrentDeletionException;
import org.bootcamp.yum.exception.ConcurrentModificationException;
import org.bootcamp.yum.api.model.hungry.DailyMenu;
import org.bootcamp.yum.api.model.OrderItem;
import org.bootcamp.yum.api.model.chef.DailyOrders;
import org.bootcamp.yum.api.model.hungry.OrderDetails;
import org.bootcamp.yum.api.model.chef.DailyMenuChef;
import org.bootcamp.yum.api.model.chef.DailyOrderChef;
import org.bootcamp.yum.api.model.chef.DailyMenusChef;
import org.bootcamp.yum.api.model.chef.DailyOrdersChef;
import org.bootcamp.yum.api.model.hungry.MenuInfo;
import org.bootcamp.yum.api.model.hungry.UpdateDailyOrder;
import org.bootcamp.yum.data.entity.DailyOrder;
import org.bootcamp.yum.data.entity.User;
import org.bootcamp.yum.data.repository.FoodRepository;
import org.bootcamp.yum.data.repository.DailyMenuRepository;
import org.bootcamp.yum.data.repository.DailyOrderRepository;

/**
 *
 * @author user
 */
@Service
public class OrdersService {

    @Autowired
    private DailyMenuRepository dailyMenuRepo;
    @Autowired
    private DailyMenusService dailyMenusService;
    @Autowired
    private DailyOrderRepository dailyOrderRepo;
    @Autowired
    private FoodRepository foodRepo;
    @Autowired
    private YumService yumService;
    @Autowired
    private UserService userService;
    @Autowired
    private SendMailService mailService;

    @Transactional
    public org.bootcamp.yum.api.model.Version ordersIdPut(Long id, UpdateDailyOrder updateDailyOrder) throws ApiException {

        DailyOrder order = authorizedQueryById(id);
        if (order == null) {
            checkConcurrentDeletion(updateDailyOrder.getDate(), updateDailyOrder.getMenuId(), updateDailyOrder.getMenuVersion());
        }

        //Validate the version
        if (order.getVersion() != updateDailyOrder.getVersion() || order.getDailyMenu().getVersion() != updateDailyOrder.getMenuVersion()) {
            DailyMenu menu = order.getOrderedMenuDto(yumService.getDeadline());
            throw new ConcurrentModificationException(menu);
        }

        //Generate the requestd items
        List<org.bootcamp.yum.data.entity.OrderItem> newItems = generateValidItems(updateDailyOrder.getOrderItems(), order.getDailyMenu());

        //Update items
        order.updateItems(newItems);

        //if the send email is check send an email with order's
        // to user
        if (updateDailyOrder.getSendMail()) {
            mailService.sendConfirmOrderEmailToHungry(order);
        }
        // return the updated version
        return order.getVersionDTO();
    }

    public DailyMenu ordersIdGet(Long id, Long menuId, Integer menuVersion, LocalDate date) throws ApiException {

        DailyOrder order = authorizedQueryById(id);
        if (order == null) {
            checkConcurrentDeletion(date, menuId, menuVersion);
        }

        return order.getOrderedMenuDto(yumService.getDeadline());

    }

    @Transactional
    public DailyMenu ordersPost(OrderDetails orderDetails) throws ApiException {
        DailyOrder order = generateValidOrder(orderDetails);
        dailyOrderRepo.save(order);
        //if the send email is check send an email with order's
        // to user
        if (orderDetails.getSendMail()) {
            mailService.sendConfirmOrderEmailToHungry(order);
        }
        return order.getOrderedMenuDto(yumService.getDeadline());
    }

    @Transactional
    public void ordersIdDelete(Long id, MenuInfo menuInfo) throws ApiException {

        DailyOrder order = authorizedQueryById(id);
        if (order == null) {
            checkConcurrentDeletion(menuInfo.getDate(), menuInfo.getMenuId(), menuInfo.getMenuVersion());
        }

        dailyOrderRepo.delete(order);
    }

    public DailyOrders ordersDailyDayGet(LocalDate day) throws ApiException {
        // Get the requested menu
        org.bootcamp.yum.data.entity.DailyMenu menu = dailyMenuRepo.findByOfDate(day);
        if (menu == null) {
            throw new ApiException(400, "Invalid request. The requested menu does not exists");
        }

        return menu.getDailyOrdersDTO();
    }

    public DailyOrdersChef ordersMonthlyMonthYearGet(int[] monthYearIntArray) throws ApiException {
        DailyMenusChef dailyMenusChef = dailyMenusService.dailyMenusMonthlyMonthYearGet(monthYearIntArray[0], monthYearIntArray[1]);
        return convertDailyMenuCheftoDailyOrderChef(dailyMenusChef);
    }

    public DailyOrdersChef ordersMonthlyGet() throws ApiException {
        DailyMenusChef dailyMenusChef = dailyMenusService.dailyMenusMonthlyGet();
        return convertDailyMenuCheftoDailyOrderChef(dailyMenusChef);
    }

    // Converts DailyMenusChef DTO to DailyOrdersChef DTO
    private DailyOrdersChef convertDailyMenuCheftoDailyOrderChef(DailyMenusChef dailyMenusChef) {
        DailyOrdersChef dailyOrdersChef = new DailyOrdersChef();
        for (DailyMenuChef dailyMenuChef : dailyMenusChef) {

            DailyOrderChef dailyOrderChef = new DailyOrderChef()
                    .dailyMenuId(dailyMenuChef.getId())
                    .date(dailyMenuChef.getDate());
            // Keep only items with quantity>0
            for (org.bootcamp.yum.api.model.OrderItem item : dailyMenuChef.getFoods()) {
                if (item.getQuantity() > 0) {
                    dailyOrderChef.addFoodsItem(item);
                }
            }
            dailyOrdersChef.add(dailyOrderChef);
        }
        return dailyOrdersChef;
    }

    private DailyOrder authorizedQueryById(Long id) throws ApiException {
        DailyOrder order = dailyOrderRepo.findOne(id);
        if (order == null) {
            return null;
        }
        //check that the order belongs to the logged in user
        if (!order.isOwnedBy(userService.getLoggedInUser())) {
            throw new ApiException(403, "Access Denied");
        }
        if (order.isFinalOrder(yumService.getDeadline())) {
            throw new ApiException(412, "Order is final");
        }
        return order;
    }

    private ArrayList<org.bootcamp.yum.data.entity.OrderItem> generateValidItems(List<OrderItem> itemsDto, org.bootcamp.yum.data.entity.DailyMenu menu) throws ApiException {
        ArrayList<org.bootcamp.yum.data.entity.OrderItem> itemsDao = new ArrayList<>();
        for (OrderItem item : itemsDto) {
            org.bootcamp.yum.data.entity.Food food = foodRepo.findById(item.getFoodId());
            if (food == null || !menu.getFoods().contains(food)) {
                throw new ApiException(400, "The requested food is not in the menu");
            }
            itemsDao.add(new org.bootcamp.yum.data.entity.OrderItem(food, item.getQuantity()));
        }
        return itemsDao;
    }

    private DailyOrder generateValidOrder(OrderDetails orderDetails) throws ApiException {
        //grab the current logged in user
        User user = userService.getLoggedInUser();
        org.bootcamp.yum.data.entity.DailyMenu menu = dailyMenuRepo.findById(orderDetails.getMenuId());
        // check if menu for the requested date exists.
        if (menu == null) {
            //check if there is a new menu for that date
            org.bootcamp.yum.data.entity.DailyMenu newMenu = dailyMenuRepo.findByOfDate(orderDetails.getDate());
            if (newMenu == null) {
                throw new ConcurrentDeletionException(null, "No menu is proposed for this day anymore");
            } else {
                throw new ConcurrentDeletionException(newMenu.getUnOrderedMenuDTO(yumService.getDeadline()), "The chef changed the menu recently. Please place your order again");
            }
        } else {
            if (menu.getVersion() != orderDetails.getMenuVersion()) {
                throw new ConcurrentDeletionException(menu.getUnOrderedMenuDTO(yumService.getDeadline()), "The chef changed the menu recently. Please place your order again");
            }
        }
        if (menu.isExpired(yumService.getDeadline())) {
            throw new ApiException(412, "Order deadline has passed.");
        }
        // check if user has already placed an order at the specified date
        DailyOrder order;
        if ((order = dailyOrderRepo.findByUserIdAndDailyMenuId(user.getId(), menu.getId())) != null) {
            throw new ConcurrentCreationException(order.getOrderedMenuDto(yumService.getDeadline()), "You have already placed an order for that date");
        }
        // Generate valid order items
        ArrayList<org.bootcamp.yum.data.entity.OrderItem> items = generateValidItems(orderDetails.getOrderItems(), menu);
        // Create and return new order
        return new DailyOrder(user, menu, items);
    }

    private void checkConcurrentDeletion(LocalDate date, Long menuId, Integer menuVersion) throws ApiException {
        org.bootcamp.yum.data.entity.DailyMenu menu = dailyMenuRepo.findById(menuId);
        if (menu != null) {
            // Check if there is a new order for that menu (for this user)
            for (DailyOrder order : menu.getDailyOrders()) {
                if (order.isOwnedBy(userService.getLoggedInUser())) {
                    throw new ConcurrentDeletionException(order.getOrderedMenuDto(yumService.getDeadline()), "You cancelled this order earlier, and placed again. Here is the new one");
                }
            }
            // Check if the menu has changed? compare the menuVersion you got with the one in the menu
            if (menuVersion != menu.getVersion()) {
                // the DTO will not contain any order (orderId = -1)
                throw new ConcurrentDeletionException(menu.getUnOrderedMenuDTO(yumService.getDeadline()), "You cancelled this order earlier, and the menu has changed");
            } else {
                // the DTO will not contain any order (orderId = -1)
                throw new ConcurrentDeletionException(menu.getUnOrderedMenuDTO(yumService.getDeadline()), "You cancelled this order earlier");
            }

        } else {
            org.bootcamp.yum.data.entity.DailyMenu newMenu = dailyMenuRepo.findByOfDate(date);
            if (newMenu != null) {
                // the DTO will not contain any order (orderId = -1)
                throw new ConcurrentDeletionException(newMenu.getUnOrderedMenuDTO(yumService.getDeadline()), "You cancelled this order earlier, and the menu has changed");
            } else {
                throw new ConcurrentDeletionException(null, "You cancelled this order earlier, and no menu is proposed anymore");
            }
        }

    }
}
