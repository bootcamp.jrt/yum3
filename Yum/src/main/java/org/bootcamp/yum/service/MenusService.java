/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.service;

import org.joda.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import org.bootcamp.yum.exception.ApiException;
import org.bootcamp.yum.api.model.hungry.DailyMenus;
import org.bootcamp.yum.data.entity.User;
import org.bootcamp.yum.data.entity.DailyMenu;
import org.bootcamp.yum.data.repository.DailyMenuRepository;

@Service
public class MenusService {

    @Autowired
    private DailyMenuRepository dailyMenuRepo;

    @Autowired
    private UserService userService;
    @Autowired
    private YumService yumService;

    /**
     * Generates a list of the current week's daily menus.
     *
     * @return a DailyMenus DTO which is an ArrayList of DailyMenu DTO
     * @throws org.bootcamp.yum.exception.ApiException
     */
    public DailyMenus menusWeeklyGet() throws ApiException {

        LocalDate date = new LocalDate();

        // Retrieve current week's daily menus passing week and year
        DailyMenus dailyMenusWeeklyList = generateWeeklyDailyMenus(date.getWeekOfWeekyear(), date.getYear());

        return dailyMenusWeeklyList;

    }

    /**
     * Generates a list of the requested week's daily menus.
     *
     * @param week the week in format WW [01-53]
     * @param year the year in format YYYY
     * @return a DailyMenus DTO which is an ArrayList of DailyMenu DTO
     * @throws org.bootcamp.yum.exception.ApiException
     */
    public DailyMenus menusWeeklyWeekYearGet(int week, int year) throws ApiException {

        // Retrieve requested week's daily menus passing week and year
        DailyMenus dailyMenusWeeklyList = generateWeeklyDailyMenus(week, year);

        return dailyMenusWeeklyList;

    }

    /**
     * Generates a list of the current month's concerned weeks daily menus.
     *
     * @return a DailyMenus DTO which is an ArrayList of DailyMenu DTO
     * @throws org.bootcamp.yum.exception.ApiException
     */
    public DailyMenus menusMonthlyGet() throws ApiException {

        LocalDate date = new LocalDate();

        // Retrieve current month's concerned weeks daily menus passing month and year
        DailyMenus dailyMenusMonthlyList = generateMonthlyDailyMenus(date.getMonthOfYear(), date.getYear());

        return dailyMenusMonthlyList;
    }

    /**
     * Generates a list of the requested month's concerned weeks daily menus.
     *
     * @param month the month in format MM [01-12]
     * @param year the year in format YYYY
     * @return a DailyMenus DTO which is an ArrayList of DailyMenu DTO
     * @throws org.bootcamp.yum.exception.ApiException
     */
    public DailyMenus menusMonthlyMonthYearGet(int month, int year) throws ApiException {

        // Retrieve requested month's concerned weeks daily menus passing month and year
        DailyMenus dailyMenusMonthlyList = generateMonthlyDailyMenus(month, year);

        return dailyMenusMonthlyList;

    }

    /*
     * Returns a list of daily menus for the weeks concerned by the requested month.
     */
    private DailyMenus generateMonthlyDailyMenus(int month, int year) throws ApiException {

        LocalDate firstDayOfMonth = new LocalDate().withYear(year).withMonthOfYear(month).dayOfMonth().withMinimumValue();
        // Check if 1st of Month is Saturday or Sunday and set firstDayOfMonth to the following Monday
        if (firstDayOfMonth.getDayOfWeek() == 6 || firstDayOfMonth.getDayOfWeek() == 7) {
            firstDayOfMonth = firstDayOfMonth.plusDays(8 - firstDayOfMonth.getDayOfWeek());
        }

        LocalDate lastDayOfMonth = new LocalDate().withYear(year).withMonthOfYear(month).dayOfMonth().withMaximumValue();

        DailyMenus dailyMenusMonthlyList = new DailyMenus();

        int week = firstDayOfMonth.getWeekOfWeekyear();
        int weekYear = firstDayOfMonth.getWeekyear();
        int i = 0;

        // Weekly loop for retrieving requested month's daily menus
        do {
            // Move to the next week
            week = firstDayOfMonth.plusWeeks(i).getWeekOfWeekyear();
            weekYear = firstDayOfMonth.plusWeeks(i).getWeekyear();

            // Retrieve week's-in-loop daily menus
            DailyMenus dailyMenusWeeklyList = generateWeeklyDailyMenus(week, weekYear);

            // Add daily menus to monthly list
            dailyMenusMonthlyList.addAll(dailyMenusWeeklyList);

            i++;
        } while (week != lastDayOfMonth.getWeekOfWeekyear());

        return dailyMenusMonthlyList;
    }

    /*
     * Returns a list of daily menus for the requested week's working days
     * (Monday to Friday).
     */
    private DailyMenus generateWeeklyDailyMenus(int week, int year) throws ApiException {

        // Get the current user
        System.out.println("userService: " + userService);
        System.out.println("User: " + userService.getLoggedInUser());
        User user = userService.getLoggedInUser();
        if (user == null) {
            throw new ApiException(403, "Authorization failed");
        }

        DailyMenus dailyMenusWeeklyList = new DailyMenus();

        // Create a date of the given Year and inside the given week
        LocalDate date = new LocalDate().withYear(year).withWeekOfWeekyear(week);

        // Retrieve dailyMenuDTOs from Monday to Friday
        for (int i = 1; i < 6; i++) {

            // Set day from Monday to Friday for that week
            date = date.withDayOfWeek(i);

            // Ask the dailyMenuRepo if a dailyMenuDAO exists for that date
            DailyMenu dailyMenuDAO = dailyMenuRepo.findByOfDate(date);

            // If exists create a dailyMenuDTO and populate it with the DAO's data
            if (dailyMenuDAO != null) {

                // Create a new dailyMenuDTO
                org.bootcamp.yum.api.model.hungry.DailyMenu dailyMenuDTO
                        = dailyMenuDAO.getOrderedMenuDTO(user.getId(), yumService.getDeadline());
//                        = new org.bootcamp.yum.api.model.DailyMenu(
//                                dailyMenuDAO,
//                                yumService.getDeadline(),
//                                user.getId());

                // Add dailyMenuDTO to dailyMenusWeeklyList
                dailyMenusWeeklyList.add(dailyMenuDTO);
            }
        }

        return dailyMenusWeeklyList;
    }

}
