/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.service;

import java.util.ArrayList;
import java.util.UUID;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.transaction.annotation.Transactional;

import org.bootcamp.enums.UserRoleEnum;
import org.bootcamp.yum.api.model.auth.Email;
import org.bootcamp.yum.api.model.auth.Login;
import org.bootcamp.yum.exception.ApiException;
import org.bootcamp.yum.data.entity.User;
import org.bootcamp.yum.api.model.auth.LoggedUser;
import org.bootcamp.yum.api.model.auth.ChangePassword;
import org.bootcamp.yum.api.model.AccountDetails;
import org.bootcamp.yum.api.model.auth.TermsAndPolicy;
import org.bootcamp.yum.data.repository.UserRepository;
import org.bootcamp.yum.data.repository.YumRepository;
import org.bootcamp.JwtCodec;

@Service
public class AuthService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private YumRepository yumRepository;

    @Autowired
    private SendMailService mailService;

    public LoggedUser authLoginPost(Login credentials) throws ApiException {

        User user = userRepository.findByEmail(credentials.getEmail().trim());
        // check if the user (with the give e-mail) not exists. 
        // If exists check if the password is the right one
        if (user == null || !BCrypt.checkpw(credentials.getPassword(), user.getPassword())) {
            throw new ApiException(403, "Username or password is incorrect");
        }
        // check if the user is approved 
        if (!user.isApproved()) {
            throw new ApiException(403, "You are not approved yet");
        }

        // set loged in user his roles based on his "primary" role
        ArrayList<String> roles = setupRoles(user.getRole().name().toLowerCase());

        String subject = "" + user.getId();
        String compactJws = JwtCodec.encode(subject, roles);

        LoggedUser loggedUser = new LoggedUser();
        loggedUser.setToken(compactJws);
        loggedUser.setId(user.getId());
        loggedUser.setFirstName(user.getFirstName());
        loggedUser.setLastName(user.getLastName());
        loggedUser.setRole(user.getRole().toString());
        loggedUser.setHasPicture(user.hasPicture());

        return loggedUser;
    }

    @Transactional
    public void authRegisterPost(AccountDetails credentials) throws ApiException {

        User user = userRepository.findByEmail(credentials.getEmail().trim());
        // check if the user already exists in db
        if (user != null) {
            throw new ApiException(403, "The given email exists in the database or has invalid format or password length is less than six characters");
        }

        //add new user in db
        User newUser = new User();
        String hashedPassword = BCrypt.hashpw(credentials.getPassword(), BCrypt.gensalt());
        newUser.setEmail(credentials.getEmail());
        newUser.setPassword(hashedPassword);
        newUser.setFirstName(credentials.getFirstName());
        newUser.setLastName(credentials.getLastName());
        newUser.setRegistrationDate(LocalDate.now());
        newUser.setApproved(false);
        newUser.setRole(UserRoleEnum.HUNGRY);

        User savedUser = userRepository.save(newUser);

        // send mail to all admins to notify them for the newly created user
        //so they can approuve him.
        mailService.sendNewUserEmailToAllAdmins(savedUser);
    }

    /*
     *  set the user available roles based on his primary role.
     * 
     */
    private ArrayList<String> setupRoles(String userRole) {
        ArrayList<String> roles = new ArrayList<>();
        roles.add("hungry");

        switch (userRole) {

            case "chef":
                roles.add("chef");
                break;
            case "admin":
                roles.add("chef");
                roles.add("admin");
                break;
        }
        return roles;
    }

    @Transactional
    public void authForgotpwdPost(Email email) throws ApiException {
        User user = userRepository.findByEmail(email.getEmail().trim());
        // check if the user doesn't exists in db
        if (user == null) {
            throw new ApiException(404, "The given email doesn't exists in the database");
        }
        
        if(!user.isApproved()){
            throw new ApiException(400, "Bad request, user hasn't approved yet");
        }
        //generate a four digit random number and use it just for the secret unique string
        final String secret = UUID.randomUUID().toString();

        user.setSecret(secret);
        user.setSecretCreation(LocalDateTime.now());

        //send reset password email to user.
        mailService.sendResetPasswordLinkEmail(user);
    }

    @Transactional
    public void authChangepwdPut(ChangePassword changePassword) throws ApiException {

        User user = null;
        //get user by its secret
        user = userRepository.findBySecret(changePassword.getSecret());

        //check if the given is valid
        if (user == null) {
            throw new ApiException(400, "The  given secret is invalid");
        }
        //check if the secret has expire
        LocalDateTime expiredDate = user.getSecretCreation().plusDays(1);
        boolean isExpired = expiredDate.isAfter(LocalDateTime.now());

        if (!isExpired) {
            throw new ApiException(400, "The given secret is expired");
        }

        String hashedPassword = BCrypt.hashpw(changePassword.getPassword(), BCrypt.gensalt());
        user.setPassword(hashedPassword);
        user.setSecret(null);
    }

    public TermsAndPolicy authTermsGet() {

        TermsAndPolicy termsAndPolicy = new TermsAndPolicy();

        termsAndPolicy.setTerms(yumRepository.findOne(1L).getTerms());
        termsAndPolicy.setPolicy(yumRepository.findOne(1L).getPrivacy());

        return termsAndPolicy;
    }

}
