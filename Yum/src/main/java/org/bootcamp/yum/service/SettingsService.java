/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.service;

import java.io.IOException;

import org.springframework.stereotype.Service;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;

import org.bootcamp.yum.data.entity.User;
import org.bootcamp.yum.exception.ApiException;
import org.bootcamp.yum.api.model.AccountDetails;
import org.bootcamp.yum.api.model.AccountSettings;
import org.bootcamp.yum.data.repository.UserRepository;
import org.bootcamp.yum.exception.ConcurrentModificationException;
import org.bootcamp.JwtCodec;

@Service
public class SettingsService {

    @Autowired
    private UserService userService;

    @Autowired
    private UserRepository userRepo;

    /**
     * That method GETs the user's settings
     *
     * @return The user's AccountSettings
     * @throws ApiException
     */
    public AccountSettings settingsGet() throws ApiException {

        User user = userService.getLoggedInUser();

        if (user == null) {
            throw new ApiException(403, "Authorization Failed");
        }
        return user.populateAccountSettings();
    }

    /**
     * That method updates the user's settings
     *
     * @param accountDetails The updated account details
     * @return
     * @throws ApiException
     */
    @Transactional
    public void settingsPut(AccountDetails accountDetails) throws ApiException {

        User user = userService.getLoggedInUser();
        if (user == null) {
            throw new ApiException(403, "Authorization Failed");
        }
        //Check if the email is already used by another user.
        User userValidation = userRepo.findByEmail(accountDetails.getEmail());
        if (userValidation != null) {
            if (!(user.getFirstName().equals(userValidation.getFirstName()))) {
                throw new ApiException(400, "Bad Request");
            }
        }

        if (accountDetails.getVersion() != user.getVersion()) {
            throw new ConcurrentModificationException(this.settingsGet());
        }

        user.setEmail(accountDetails.getEmail().trim());
        user.setFirstName(accountDetails.getFirstName().trim());
        user.setLastName(accountDetails.getLastName().trim());
        if (!accountDetails.getPassword().trim().isEmpty() && accountDetails.getPassword().trim().length() >= 6) {
            user.setPassword(BCrypt.hashpw(accountDetails.getPassword().trim(), BCrypt.gensalt()));
        } else if (!accountDetails.getPassword().trim().isEmpty() && accountDetails.getPassword().trim().length() < 6) {
            throw new ApiException(400, "Bad Request");
        }

    }

    @Transactional
    public void settingsPicturePost(MultipartFile newpicture) throws ApiException {

        try {
            // get a byte array from the upload file
            byte[] bytes = newpicture.getBytes();

            // get the logged in user
            User loggedInUser = userService.getLoggedInUser();
            //proccess and save the picture
            loggedInUser.addPicture(bytes);
        } catch (IOException ex) {
            throw new ApiException(400, "Bad request");
        }

    }

    public byte[] settingsPictureTokenGet(String token) throws ApiException {

        try {
            Claims claims = JwtCodec.decode(token);
            //get user id from token
            User user = userRepo.findOne(Long.parseLong(claims.getSubject()));
            if (user.getPicture() == null || user.getPicture().length == 0) {
                throw new ApiException(404, "Picture not found");
            }

            return user.getPicture();
        } catch (JwtException ex) {
            throw new ApiException(403, "Invalid token");
        }
    }

    @Transactional
    public void removeUserPicture() throws ApiException {
        User loggedInUser = userService.getLoggedInUser();

        if (loggedInUser == null || (loggedInUser.getPicture() == null || loggedInUser.getPicture().length == 0)) {
            throw new ApiException(400, "Bad request");
        }
        loggedInUser.setPicture(null);
    }

}
