/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.service;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import org.joda.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;

import org.bootcamp.enums.UserRoleEnum;
import org.bootcamp.yum.api.model.admin.Users;
import org.bootcamp.yum.exception.ApiException;
import org.bootcamp.yum.api.model.admin.NewUser;
import org.bootcamp.yum.api.model.AccountSettings;
import org.bootcamp.yum.api.model.admin.UpdateAccountSettings;
import org.bootcamp.yum.exception.ConcurrentModificationException;
import org.bootcamp.yum.data.entity.DailyOrder;
import org.bootcamp.yum.data.entity.User;
import org.bootcamp.yum.data.entity.User.OrdersStatus;
import org.bootcamp.yum.data.repository.DailyOrderRepository;
import org.bootcamp.yum.data.repository.UserRepository;
import org.bootcamp.JwtCodec;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepo;
    @Autowired
    private DailyOrderRepository orderRepo;
    @Autowired
    private YumService yumService;
    @Autowired
    private SendMailService mailService;

    public User getLoggedInUser() throws ApiException {

        User user = userRepo.findById((Long) SecurityContextHolder.getContext().getAuthentication().getPrincipal());
        if (user == null) {
            throw new ApiException(403, "Authorization failed");
        }

        return user;
    }

    /**
     * That method GETs the requested user's account settings
     *
     * @param id The user's id
     * @return Account Settings
     * @throws ApiException
     */
    public AccountSettings usersIdGet(Long id) throws ApiException {

        User user = userRepo.findById(id);
        if (user != null) {
            return user.populateAccountSettings();
        } else {
            throw new ApiException(404, "User Not Found");
        }
    }

    /**
     *
     * @param id That method updates the requested user's account settings
     * @param updatedAccountSettings the new account settings
     * @throws ApiException
     */
    @Transactional
    public void usersIdPut(Long id, UpdateAccountSettings updatedAccountSettings) throws ApiException {

        User user = userRepo.findById(id);
        if (user != null) {
            if (user.getVersion() == updatedAccountSettings.getVersion()) {
                User existingUser;
                if ((existingUser = userRepo.findByEmail(updatedAccountSettings.getEmail())) != null && user.getId() != existingUser.getId()) {
                    throw new ApiException(412, "Email Already Exists");
                }

                if (validateRole(updatedAccountSettings.getRole())) {
                    user.updateAccountSettings(updatedAccountSettings);
                } else {
                    throw new ApiException(400, "Bad Request. Role is not valid");
                }

            } else {
                throw new ConcurrentModificationException(user.populateAccountSettings());
            }
        } else {
            throw new ApiException(404, "User Not Found");
        }
    }

    /**
     * That method creates a new user
     *
     * @param newUser The data of the new user to be created
     * @throws ApiException
     */
    @Transactional
    public void usersPost(NewUser newUser) throws ApiException {
        if (userRepo.findByEmail(newUser.getEmail().trim()) != null) {
            throw new ApiException(412, "Email Already Exists");
        }
        if (!validateRole(newUser.getRole())) {
            throw new ApiException(400, "Bad Request. Role is not valid");
        }

        User user = new User(
                newUser.getFirstName(),
                newUser.getLastName(),
                newUser.getEmail(),
                newUser.getPassword(),
                newUser.getRole().trim().toUpperCase());
        userRepo.save(user);

    }

    @Transactional
    public void usersIdForgotpwd(Long id) throws ApiException {

        User user = userRepo.findById(id);
        // check if the user already exists in db
        if (user == null) {
            throw new ApiException(404, "the user with the given id doesn't exists in the database");
        }
        //generate a four digit random number and use it just for the secret unique string
        final String secret = UUID.randomUUID().toString();

        user.setSecret(secret);
        user.setSecretCreation(LocalDateTime.now());

        mailService.sendResetPasswordLinkEmail(user);
    }

    /**
     * That method GETs all the users without any sorting/paging applied
     *
     * @return The users found in database
     */
    public Users getAllUsers() {
        Iterable<User> repoUsers = userRepo.findAll();
        Users users = new Users();
        for (User repoUser : repoUsers) {
            org.bootcamp.yum.api.model.admin.User user = new org.bootcamp.yum.api.model.admin.User();
            user.setId(repoUser.getId());
            user.setFirstName(repoUser.getFirstName());
            user.setLastName(repoUser.getLastName());
            user.setEmail(repoUser.getEmail());
            user.setRole(repoUser.getRole().toString());
            user.setRegistrationDate(repoUser.getRegistrationDate());
            user.setApproved(repoUser.isApproved());
            user.setHasPicture(repoUser.hasPicture());
            users.addUsers(user);
        }

        users.setTotalPagesNumber(0);

        return users;
    }

    /**
     * That method GETs all the users with sorting applied.
     *
     * @param sort Asceding or Descending sorting of results
     * @param page The requested page
     * @param size Number of results per page
     * @param order Order results by: name, registration date, role, status
     * @return Sorted and ordered users
     * @throws ApiException
     */
    public Users usersGet(String sort, Integer page, Integer size, String order) throws ApiException {
        order = order.trim().toLowerCase();
        sort = sort.trim().toLowerCase();
        //checking for valid query parameters
        if (validateQueryParams(sort, page, size, order)) {

            Page<User> findByParams;

            //Ascendind sorting
            if (sort.equals("asc")) {
                switch (order) {
                    case "name":
                        findByParams = (Page<User>) userRepo.findAll(
                                new PageRequest(page, size, Sort.Direction.ASC, "firstName"));
                        return populateUsers(findByParams, size);
                    case "regdate":
                        findByParams = (Page<User>) userRepo.findAll(
                                new PageRequest(page, size, Sort.Direction.ASC, "id"));
                        return populateUsers(findByParams, size);
                    case "role":
                        findByParams = (Page<User>) userRepo.findAll(
                                new PageRequest(page, size, Sort.Direction.ASC, "role"));
                        return populateUsers(findByParams, size);
                    case "status":
                        findByParams = (Page<User>) userRepo.findAll(
                                new PageRequest(page, size, Sort.Direction.ASC, "approved"));
                        return populateUsers(findByParams, size);
                }
                //Descending sorting
            } else {
                switch (order) {
                    case "name":
                        findByParams = (Page<User>) userRepo.findAll(
                                new PageRequest(page, size, Sort.Direction.DESC, "firstName"));
                        return populateUsers(findByParams, size);
                    case "regdate":
                        findByParams = (Page<User>) userRepo.findAll(
                                new PageRequest(page, size, Sort.Direction.DESC, "id"));
                        return populateUsers(findByParams, size);
                    case "role":
                        findByParams = (Page<User>) userRepo.findAll(
                                new PageRequest(page, size, Sort.Direction.DESC, "role"));
                        return populateUsers(findByParams, size);
                    case "status":
                        findByParams = (Page<User>) userRepo.findAll(
                                new PageRequest(page, size, Sort.Direction.DESC, "approved"));
                        return populateUsers(findByParams, size);
                }

            }
        }
        throw new ApiException(400, "Bad Request. Parameters are not valid");
    }

    @Transactional
    public void setApprovedStatus(Long id, Boolean approved, Boolean force) throws ApiException {
        User user = userRepo.findById(id);
        if (user == null) {
            throw new ApiException(404, "the user with the given id doesn't exists in the database");
        }

        if (!approved) {
            // cannot disapprove super user or himself
            if (id == 1 || id == this.getLoggedInUser().getId()) {
                throw new ApiException(400, "Bad request");
            }
            // if force == true, force user disapproval
            if (force) {
                forceDisapprove(user);
                return;
            }
            // Check the user's ordersStatus
            OrdersStatus ordersStatus = user.getOrdersStatus(yumService.getDeadline());

            if (ordersStatus.equals(OrdersStatus.MIXED) || ordersStatus.equals(OrdersStatus.ONLY_NOT_FINAL)) {
                throw new ApiException(409, "The user has pending orders");
            } else {
                user.setApproved(false);
            }
        } else {
            user.setApproved(true);
            mailService.sendApprovedUserEmail(user);
        }

    }

    @Transactional
    public void deleteUser(Long id, Boolean force) throws ApiException {

        // cannot disapprove super user or himself
        if (id == 1 || id == this.getLoggedInUser().getId()) {
            throw new ApiException(400, "Bad request");
        }

        User user = userRepo.findById(id);
        if (user == null) {
            throw new ApiException(404, "the user with the given id doesn't exists in the database");
        }

        // Get the user's order list status
        OrdersStatus ordersStatus = user.getOrdersStatus(yumService.getDeadline());

        if (force) {
            // Check if deletion is permmited
            if (ordersStatus.equals(OrdersStatus.MIXED) || ordersStatus.equals(OrdersStatus.ONLY_FINAL)) {
                throw new ApiException(400, "User cannot be deleted (pending orders)");
            }
            // delete user and his order's list
            forceDisapprove(user);
            userRepo.delete(user);
            return;
        }

        // Check the user's ordersStatus
        switch (ordersStatus) {
            case NO_ORDERS:
                userRepo.delete(user);
                break;
            case ONLY_NOT_FINAL:
                throw new ApiException(409, "User has only non-final orders");
            case MIXED:
                throw new ApiException(402, "User has final and non-final orders");
            case ONLY_FINAL:
                throw new ApiException(412, "User has only final orders");
            default:
                break;
        }
    }

    // Disapproves the user and deletes his non-final orders
    @Transactional
    private void forceDisapprove(User user) {
        // delete non-final orders
        List<DailyOrder> ordersTorRemove = user.getNonFinalOrders(yumService.getDeadline());
        if (!ordersTorRemove.isEmpty()) {
            orderRepo.delete(ordersTorRemove);
        }
        // dispapprove user
        user.setApproved(false);
    }

    //That method validates the role to be ADMIN/HUNGRY/CHEF
    private boolean validateRole(String role) {
        if (role.trim().toUpperCase().equals("ADMIN")
                || role.trim().toUpperCase().equals("HUNGRY")
                || role.trim().toUpperCase().equals("CHEF")) {
            return true;
        }
        return false;
    }

    //That method validates the query params 
    private boolean validateQueryParams(String sort, Integer page, Integer size, String order) {
        if ((sort.equals("asc") || sort.equals("desc"))
                && (page >= 0)
                && (size > 0)
                && (order.equals("name") || order.equals("role") || order.equals("regdate") || order.equals("status"))) {
            return true;
        }
        return false;
    }

    //That method populates the users found in the database
    private Users populateUsers(Page<User> repoUsers, Integer size) {

        Users users = new Users();
        for (User repoUser : repoUsers) {
            org.bootcamp.yum.api.model.admin.User user = new org.bootcamp.yum.api.model.admin.User();
            user.setId(repoUser.getId());
            user.setFirstName(repoUser.getFirstName());
            user.setLastName(repoUser.getLastName());
            user.setEmail(repoUser.getEmail());
            user.setRole(repoUser.getRole().toString());
            user.setRegistrationDate(repoUser.getRegistrationDate());
            user.setApproved(repoUser.isApproved());
            user.setHasPicture(repoUser.hasPicture());
            users.addUsers(user);
        }
        if (size == 0) {
            users.setTotalPagesNumber(0);
        } else {
            users.setTotalPagesNumber(repoUsers.getTotalPages());
        }

        return users;
    }

    public byte[] usersIdPictureGet(Long id, String token) throws ApiException {
        try {
            Claims claims = JwtCodec.decode(token);
            //get user id from token
            User admin = userRepo.findOne(Long.parseLong(claims.getSubject()));

            // if the token isn't from admin user
            if (!admin.getRole().equals(UserRoleEnum.ADMIN)) {
                throw new ApiException(403, "Unauthorized user");
            }
            System.out.println("ADMIN");
            User user = userRepo.findOne(id);

            if (user == null) {
                throw new ApiException(404, "User not found");
            }

            if (user.getPicture() == null || user.getPicture().length == 0) {
                throw new ApiException(404, "Picture not found");
            }

            return user.getPicture();
        } catch (JwtException ex) {
            throw new ApiException(403, "Invalid token");
        }
    }

    @Transactional
    public void usersIdPicturePost(Long id, MultipartFile newpicture) throws ApiException {
        try {
            // get a byte array from the upload file
            byte[] bytes = newpicture.getBytes();

            // get the logged in user
            User user = userRepo.findOne(id);

            if (user == null) {
                throw new ApiException(404, "User not found");
            }

            //proccess and save the picture
            user.addPicture(bytes);
        } catch (IOException ex) {
            throw new ApiException(400, "Bad request");
        }
    }

    @Transactional
    public void removeUserPicture(Long id) throws ApiException {

        User user = userRepo.findOne(id);
        if (user == null) {
            throw new ApiException(404, "User not found");
        }

        if ((user.getPicture() == null || user.getPicture().length == 0)) {
            throw new ApiException(400, "Bad request");
        }
        user.setPicture(null);
    }

}
