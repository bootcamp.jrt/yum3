/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.service;

import java.util.List;
import java.util.ArrayList;

import org.joda.time.LocalDate;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.bootcamp.yum.exception.ApiException;
import org.bootcamp.yum.exception.ConcurrentCreationException;
import org.bootcamp.yum.exception.ConcurrentDeletionException;
import org.bootcamp.yum.exception.ConcurrentModificationException;
import org.bootcamp.yum.api.model.chef.FoodList;
import org.bootcamp.yum.api.model.chef.DailyMenuChef;
import org.bootcamp.yum.api.model.chef.DailyMenusChef;
import org.bootcamp.yum.api.model.chef.DailyMenusFoods;
import org.bootcamp.yum.api.model.chef.UpdateDailyMenu;
import org.bootcamp.yum.data.repository.FoodRepository;
import org.bootcamp.yum.data.entity.Food;
import org.bootcamp.yum.data.entity.DailyMenu;
import org.bootcamp.yum.data.entity.OrderItem;
import org.bootcamp.yum.data.repository.DailyMenuRepository;
import org.bootcamp.yum.data.repository.OrderItemRepository;

@Service
public class DailyMenusService {

    @Autowired
    private DailyMenuRepository dailyMenuRepo;
    @Autowired
    private FoodRepository foodRepo;
    @Autowired
    private OrderItemRepository orderItemRepo;
    @Autowired
    private YumService yumService;

    /**
     * Generates a list of the current month's concerned weeks DailyMenuChef.
     *
     * @return a DailyMenusChef DTO which is an ArrayList of DailyMenuChef DTO
     * @throws org.bootcamp.yum.exception.ApiException
     */
    public DailyMenusChef dailyMenusMonthlyGet() throws ApiException {

        LocalDate date = new LocalDate();

        // Retrieve current month's concerned weeks DailyMenusChef passing month and year
        DailyMenusChef dailyMenusChefMonthlyList = generateMonthlyDailyMenusChef(date.getMonthOfYear(), date.getYear());

        return dailyMenusChefMonthlyList;

    }

    /**
     *
     * Generates a list of the requested month's concerned weeks DailyMenuChef.
     *
     * @param month the month in format MM [01-12]
     * @param year the year in format YYYY
     * @return a DailyMenusChef DTO which is an ArrayList of DailyMenuChef DTO
     * @throws org.bootcamp.yum.exception.ApiException
     */
    public DailyMenusChef dailyMenusMonthlyMonthYearGet(int month, int year) throws ApiException {

        // Retrieve requested month's concerned weeks daily menus passing month and year
        DailyMenusChef dailyMenusChefMonthlyList = generateMonthlyDailyMenusChef(month, year);

        return dailyMenusChefMonthlyList;

    }

    /*
     * Returns a list of DailyMenuChef for the weeks concerned by the requested month.
     */
    private DailyMenusChef generateMonthlyDailyMenusChef(int month, int year) throws ApiException {

        LocalDate firstDayOfMonth = new LocalDate().withYear(year).withMonthOfYear(month).dayOfMonth().withMinimumValue();
        // Check if 1st of Month is Saturday or Sunday and set firstDayOfMonth to the following Monday
        if (firstDayOfMonth.getDayOfWeek() == 6 || firstDayOfMonth.getDayOfWeek() == 7) {
            firstDayOfMonth = firstDayOfMonth.plusDays(8 - firstDayOfMonth.getDayOfWeek());
        }

        LocalDate lastDayOfMonth = new LocalDate().withYear(year).withMonthOfYear(month).dayOfMonth().withMaximumValue();

        DailyMenusChef dailyMenusChefMonthlyList = new DailyMenusChef();

        int week = firstDayOfMonth.getWeekOfWeekyear();
        int weekYear = firstDayOfMonth.getWeekyear();
        int i = 0;

        // Weekly loop for retrieving requested month's DailyMenusChef
        do {
            // Move to the next week
            week = firstDayOfMonth.plusWeeks(i).getWeekOfWeekyear();
            weekYear = firstDayOfMonth.plusWeeks(i).getWeekyear();

            // Retrieve week's-in-loop DailyMenusChef
            DailyMenusChef dailyMenusChefWeeklyList = generateWeeklyDailyMenusChef(week, weekYear);

            // Add dailyMenusChef to monthly list
            dailyMenusChefMonthlyList.addAll(dailyMenusChefWeeklyList);

            i++;
        } while (week != lastDayOfMonth.getWeekOfWeekyear());

        return dailyMenusChefMonthlyList;
    }

    /*
     * Returns a list of DailyMenuChef for the requested week's working days
     * (Monday to Friday).
     */
    private DailyMenusChef generateWeeklyDailyMenusChef(int week, int year) throws ApiException {

        DailyMenusChef dailyMenusChefWeeklyList = new DailyMenusChef();

        // Create a date of the given Year and inside the given week
        LocalDate date = new LocalDate().withYear(year).withWeekOfWeekyear(week);

        // Retrieve DailyMenusChef from Monday to Friday
        for (int i = 1; i < 6; i++) {

            // Set day from Monday to Friday for that week
            date = date.withDayOfWeek(i);

            // Ask the dailyMenuRepo if a dailyMenuDAO exists for that date
            DailyMenu dailyMenuDAO = dailyMenuRepo.findByOfDate(date);
            // If exists create a dailyMenuDTO and populate it with the DAO's data
            if (dailyMenuDAO != null) {

                // Create a new DailyMenuChef
                //DailyMenuChef dailyMenuChef = new DailyMenuChef(dailyMenuDAO);
                DailyMenuChef dailyMenuChef = dailyMenuDAO.getDailyMenuChefDTO(yumService.getDeadline());

                // Add dailyMenuChef to dailyMenusChefWeeklyList
                dailyMenusChefWeeklyList.add(dailyMenuChef);

            }

        }

        return dailyMenusChefWeeklyList;
    }

    @Transactional
    public DailyMenuChef dailyMenusPost(FoodList foodList) throws ApiException {

        DailyMenu dailyMenuDAO;

        /* Validate that 
         *  - the requested date is in the future
         *  - the requested date is not on Saturday or Sunday
         *  - there is at least one food
         */
        if (foodList.getDate().isBefore(LocalDate.now().plusDays(1))
                || foodList.getDate().getDayOfWeek() == 6
                || foodList.getDate().getDayOfWeek() == 7
                || foodList.getFoods().size() < 1) {
            throw new ApiException(400, "Bad request");
        }

        // Check there is other DailyMenu on that date
        if ((dailyMenuDAO = dailyMenuRepo.findByOfDate(foodList.getDate())) != null) {
            throw new ConcurrentCreationException(dailyMenuDAO.getDailyMenuChefDTO(yumService.getDeadline()), "A menu was already created for that day");
        }

        // Check the foods of the request
        validateFoodList(foodList.getFoods());

        // Generate new DailyMenu
        dailyMenuDAO = new DailyMenu();

        dailyMenuDAO.setOfDate(foodList.getDate());

        // Check that the deadline hasn't passed
        if (dailyMenuDAO.isExpired(yumService.getDeadline())) {
            throw new ApiException(412, "Daily Menu creation deadline has passed.");
        }

        // Save new DailyMenu
        dailyMenuRepo.save(dailyMenuDAO);

        for (DailyMenusFoods foodId : foodList.getFoods()) {

            Food food = foodRepo.findById(foodId.getId());
            // Add DailyMenu in food's dailyMenu list
            food.addDailyMenu(dailyMenuDAO);
            // Add food in DailyMenu's food list
            dailyMenuDAO.addFood(food);

        }

        // Construct the new dailyMenuChef
        //DailyMenuChef dailyMenuChef = new DailyMenuChef(dailyMenuDAO);
        DailyMenuChef dailyMenuChef = dailyMenuDAO.getDailyMenuChefDTO(yumService.getDeadline());

        return dailyMenuChef;
    }

    @Transactional
    public DailyMenuChef dailyMenusIdPut(Long id, UpdateDailyMenu updateDailyMenu) throws ApiException {

        DailyMenu dailyMenuDAO = dailyMenuRepo.findById(id);

        // Check that the id refers to an existing DailyMenu
        if (dailyMenuDAO == null) {

            if ((dailyMenuDAO = dailyMenuRepo.findByOfDate(updateDailyMenu.getDate())) != null) {
                throw new ConcurrentDeletionException(dailyMenuDAO.getDailyMenuChefDTO(yumService.getDeadline()), "You deleted this menu earlier and recreated it. Here is the new one");
            } else {
                throw new ConcurrentDeletionException(null, "You deleted this menu earlier. Please recreate it again if this is what was intended");
            }

        }
        System.out.println("date recieved" + updateDailyMenu);
        // Check that the DailyMenu isn't final
        if (dailyMenuDAO.isExpired(yumService.getDeadline())) {
            throw new ApiException(412, "Daily Menu modification deadline has passed.");
        }

        // Check that the requested version is valid
        if (updateDailyMenu.getVersion() != dailyMenuDAO.getVersion()) {
            throw new ConcurrentModificationException(dailyMenusIdGet(id));
        }

        /* Check that in the foods of the request
         * - the foodIds are unique
         * - the foodIds exist in the database
         * - if new foods are archived but didn't exist previously in the dailyMenu
         */
        List<Long> checkFoods = new ArrayList<>();
        for (DailyMenusFoods food : updateDailyMenu.getFoods()) {
            if (checkFoods.contains(food.getId())
                    || foodRepo.findById(food.getId()) == null) {
                throw new ApiException(400, "Bad Request");
            }

            if (foodRepo.findById(food.getId()).isArchived()
                    && !dailyMenuDAO.getFoodsIds().contains(food.getId())) {
                throw new ApiException(400, "Bad Request");
            }

            checkFoods.add(food.getId());
        }

        // Check that the foods list of the request contains the already bought foods
        for (Food food : dailyMenuDAO.getFoods()) {
            List<OrderItem> orderItems = orderItemRepo.findByFoodIdAndDailyOrderIn(food.getId(), dailyMenuDAO.getDailyOrders());
            if (!orderItems.isEmpty() && !updateDailyMenu.getFoodsIds().contains(food.getId())) {
                throw new ApiException(400, "Bad Request");
            }
        }

        // Remove from each food the DailyMenuDAO
        for (Food food : dailyMenuDAO.getFoods()) {
            food.removeDailyMenu(dailyMenuDAO);
        }

        // Change version of DailyMenu to a random number to trigger
        // the automatic version changing
        dailyMenuDAO.setVersion(dailyMenuDAO.getVersion() + 1);

        // Remove all previous foods from the DailyMenuDAO
        dailyMenuDAO.removeAllFoods();

        // Add the new foods in the dailyMenuDAO
        for (Long foodId : updateDailyMenu.getFoodsIds()) {
            Food newfood = foodRepo.findById(foodId);
            dailyMenuDAO.addFood(newfood);
            newfood.addDailyMenu(dailyMenuDAO);
        }

        // Check if dailyMenuDAO has still foods associated with it
        if (dailyMenuDAO.getFoods().size() > 0) {
            // Construct the new dailyMenuChef
            DailyMenuChef dailyMenuChef = dailyMenuDAO.getDailyMenuChefDTO(yumService.getDeadline());
            return dailyMenuChef;
        } else {
            dailyMenuRepo.delete(dailyMenuDAO);
            return null;
        }

    }

    /**
     * @param id - a DailyMenuDAO's Id
     * @return - Returns a DailyMenuChefDTO
     */
    public DailyMenuChef dailyMenusIdGet(Long id) throws ApiException {

        DailyMenu dailyMenuDAO = dailyMenuRepo.findById(id);

        if (dailyMenuDAO != null) {
            DailyMenuChef dailyMenuChef = dailyMenuDAO.getDailyMenuChefDTO(yumService.getDeadline());
            return dailyMenuChef;
        } else {
            throw new ApiException(400, "Bad Request");
        }
    }

    /**
     * Validate that - the foodIds are unique - the foodIds are existing in the
     * database - the foods are not archived
     *
     * @param foodList - The list of the requested foods
     * @throws ApiException
     */
    private void validateFoodList(List<DailyMenusFoods> foodList) throws ApiException {

        List<Long> checkFoods = new ArrayList<>();
        for (DailyMenusFoods food : foodList) {
            if (checkFoods.contains(food.getId())
                    || foodRepo.findById(food.getId()) == null
                    || foodRepo.findById(food.getId()).isArchived()) {
                throw new ApiException(400, "Bad request");
            }
            checkFoods.add(food.getId());
        }
    }

}
