/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.service;

import java.text.DecimalFormat;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.joda.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;

import org.bootcamp.enums.UserRoleEnum;
import org.bootcamp.yum.data.entity.DailyOrder;
import org.bootcamp.yum.data.entity.OrderItem;
import org.bootcamp.yum.data.entity.User;
import org.bootcamp.yum.data.repository.UserRepository;

@Service
public class SendMailService {

    @Autowired
    private JavaMailSender mailSender;

    @Value("${yum.mail.senderEmailAddress}")
    private String senderEmailAddress;

    @Value("${yum.hostname}")
    private String yumHostname;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private YumService yumService;

    public void sendNewUserEmailToAllAdmins(User newUser) {

        //check that the injected mailing service is not null 
        if (mailSender == null) {
            return;
        }

        //setup the content of the mail
        StringBuilder mailBody = new StringBuilder();
        mailBody.append("<p>");
        mailBody.append("A new user just registered.<br>Here are the details: <br>");
        mailBody.append("First name: <strong>").append(newUser.getFirstName()).append("</strong><br>");
        mailBody.append("Last name: <strong>").append(newUser.getLastName()).append("</strong><br>");
        mailBody.append("Email: <strong>").append(newUser.getEmail()).append("</strong>");
        mailBody.append("</p>");
        mailBody.append("<p>");
        mailBody.append("You have to approve this user so he/she can log in.");
        mailBody.append("</p>");
        mailBody.append("<p>");
        mailBody.append("Click on this link to approve the user: <br>");
        mailBody.append("<a href=\"");
        mailBody.append(yumHostname).append("/admin/users/").append(newUser.getId());
        mailBody.append("\">");
        mailBody.append(yumHostname).append("/admin/users/").append(newUser.getId());
        mailBody.append("</a>");
        mailBody.append("</p>");
        mailBody.append("<p>");
        mailBody.append("Best Regards,<br>The Yum Meal Manager Engine");
        mailBody.append("</p>");

        List<User> adminUsers = userRepository.findByRole(UserRoleEnum.ADMIN);

        // iterate over a list of admin users and for each admin user
        // send the mail
        for (User admin : adminUsers) {
            try {
                MimeMessage message = mailSender.createMimeMessage();
                String messageBody = "<h4> Dear " + admin.getFirstName() + " " + admin.getLastName() + ",</h4>" + mailBody;
                MimeMessageHelper helper = new MimeMessageHelper(message);
                helper.setTo(InternetAddress.parse(admin.getEmail()));
                helper.setFrom(senderEmailAddress);
                helper.setSubject("[Yum] New user registered");
                helper.setText(messageBody, true);
                mailSender.send(message);
            } catch (AddressException ex) {
                Logger.getLogger(SendMailService.class.getName()).log(Level.WARNING, null, ex);
            } catch (MessagingException ex) {
                Logger.getLogger(SendMailService.class.getName()).log(Level.WARNING, null, ex);
            }
        }

    }

    public void sendConfirmOrderEmailToHungry(DailyOrder order) {

        //check that the injected mailing service is not null 
        if (mailSender == null) {
            return;
        }

        StringBuilder mailBody = new StringBuilder();

        User user = order.getUser();

        //setup the content of the mail
        mailBody.append("<h4> Dear ").append(user.getFirstName()).append(" ").append(user.getLastName()).append(",</h4>");
        mailBody.append("<p> You just placed this order for the day ").append(order.getDailyMenu().getOfDate()).append("</p><p>");

        List<OrderItem> orderItems = order.getOrderItems();
        double total = 0;
        for (OrderItem orderItem : orderItems) {
            mailBody.append(" -");
            mailBody.append(orderItem.getFood().getName())
                    .append(" qty ")
                    .append(orderItem.getQuantity())
                    .append(" x ").append(orderItem.getFood().getPrice())
                    .append(" ").append(yumService.getCurrency())
                    .append("<br>");
            total += orderItem.getFood().getPrice().doubleValue() * orderItem.getQuantity();
        }

        DecimalFormat decimalFormat = new DecimalFormat("00.00");

        mailBody.append("</p><p><strong style='border-top: solid 1px;'>Total: ").append(decimalFormat.format(total)).append(" ")
                .append(yumService.getCurrency())
                .append(" </strong></p>");

        mailBody.append("<p> You can modify this order until  ")
                .append(order.getDailyMenu().getOfDate().minusDays(1)).append(" at ")
                .append(yumService.getDeadline().toString("HH:mm"))
                .append(", by going to the link: <br>");

        mailBody.append(" <a href=\"").append(yumHostname).append("/hungry/")
                .append(order.getDailyMenu().getOfDate().getYear()).append("/")
                .append(order.getDailyMenu().getOfDate().getWeekOfWeekyear())
                .append("#").append(order.getDailyMenu().getOfDate().getDayOfMonth())
                .append(" \">")
                .append(yumHostname).append("/hungry/")
                .append(order.getDailyMenu().getOfDate().getYear()).append("/")
                .append(order.getDailyMenu().getOfDate().getWeekOfWeekyear())
                .append("#").append(order.getDailyMenu().getOfDate().getDayOfMonth())
                .append("</a>");

        mailBody.append("<p> Thank you for your order! <br> The Yum Meal Manager Engine");

        try {
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message);
            helper.setTo(InternetAddress.parse(user.getEmail()));
            helper.setFrom(senderEmailAddress);
            helper.setSubject("[Yum] Confirm order");
            helper.setText(mailBody.toString(), true);
            mailSender.send(message);
        } catch (AddressException ex) {
            Logger.getLogger(SendMailService.class.getName()).log(Level.WARNING, null, ex);
        } catch (MessagingException ex) {
            Logger.getLogger(SendMailService.class.getName()).log(Level.WARNING, null, ex);
        }
    }

    public void sendResetPasswordLinkEmail(User user) {

        //check that the injected mailing service is not null 
        if (mailSender == null) {
            return;
        }

        LocalDateTime expiredDate = user.getSecretCreation().plusDays(1);
        StringBuilder mailBody = new StringBuilder();
        mailBody.append("<h4> Dear ").append(user.getFirstName()).append(" ").append(user.getLastName()).append(",</h4>");

        mailBody.append("<p>You just requested your password to be reset. If that was not you, please discard this message. </p>");

        mailBody.append("<p> To enter your new password, please visit this link: <br>");
        mailBody.append("<a href=\"")
                .append(yumHostname).append("/changepwd/").append(user.getSecret())
                .append("\">")
                .append(yumHostname).append("/changepwd/").append(user.getSecret())
                .append("</a>");

        mailBody.append("<p>").append("You can reset your password before ")
                .append(expiredDate.toLocalDate().toString("YYYY-MM-dd"))
                .append(" at ")
                .append(expiredDate.toLocalTime().toString("HH:mm"));

        mailBody.append(".</p><p>").append("Thank you for using Yum! <br> The Yum Meal Manager Engine");

        try {
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message);
            helper.setTo(InternetAddress.parse(user.getEmail()));
            helper.setFrom(senderEmailAddress);
            helper.setSubject("[Yum] Request to reset password");
            helper.setText(mailBody.toString(), true);
            mailSender.send(message);
        } catch (AddressException ex) {
            Logger.getLogger(SendMailService.class.getName()).log(Level.WARNING, null, ex);
        } catch (MessagingException ex) {
            Logger.getLogger(SendMailService.class.getName()).log(Level.WARNING, null, ex);
        }
    }

    public void sendApprovedUserEmail(User user) {

        //check that the injected mailing service is not null 
        if (mailSender == null) {
            return;
        }

        StringBuilder mailBody = new StringBuilder();

        mailBody.append("<h4> Dear ").append(user.getFirstName()).append(" ").append(user.getLastName()).append(",</h4>");
        mailBody.append("<p>The admin has approved you!  </p>");
        mailBody.append("<p>You can login by going to this URL: ")
                .append("<a href=\"")
                .append(yumHostname).append("/login")
                .append(("\">"))
                .append(yumHostname).append("/login")
                .append("</a></p>");

        mailBody.append("<p>");
        mailBody.append("Best Regards,<br>The Yum Meal Manager Engine");
        mailBody.append("</p>");

        try {
            MimeMessage message = mailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message);
            helper.setTo(InternetAddress.parse(user.getEmail()));
            helper.setFrom(senderEmailAddress);
            helper.setSubject("[Yum] User approved");
            helper.setText(mailBody.toString(), true);
            mailSender.send(message);
        } catch (AddressException ex) {
            Logger.getLogger(SendMailService.class.getName()).log(Level.WARNING, null, ex);
        } catch (MessagingException ex) {
            Logger.getLogger(SendMailService.class.getName()).log(Level.WARNING, null, ex);
        }
    }
}
