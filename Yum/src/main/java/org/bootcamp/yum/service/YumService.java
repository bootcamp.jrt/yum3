/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.service;

import javax.annotation.PostConstruct;

import org.joda.time.LocalTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import org.bootcamp.yum.api.model.GlobalSettings;
import org.bootcamp.yum.exception.ApiException;
import org.bootcamp.yum.data.entity.Yum;
import org.bootcamp.yum.data.repository.YumRepository;

@Service
public class YumService {

    @Autowired
    private YumRepository yumRepo;

    private Yum yum;

    @PostConstruct
    public void init() {
        System.out.println("Initializing global Yum object.");
        yum = yumRepo.findById(1);
    }

    public LocalTime getDeadline() {

        return yum.getDeadline();

    }

    @Transactional
    public void updateFoodsVersion() {
        yum.setFoodsVersion(yum.getFoodsVersion() + 1);
        yum = yumRepo.save(yum);
    }

    @Transactional
    public void globalsettingsPut(GlobalSettings updatedGlobalSettings) throws ApiException {
        yum.updateGlobalSettings(updatedGlobalSettings);
        yum = yumRepo.save(yum);

    }

    public GlobalSettings getGlobalsettingsDto() {
        return yum.generateGlobalSettings();
    }

    public void setYum(Yum yum) {
        this.yum = yum;
    }

    public int getFoodsVersion() {
        return yum.getFoodsVersion();
    }

    public String getCurrency() {
        return yum.getCurrency();
    }
}
