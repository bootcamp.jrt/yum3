/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.api.model.chef;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.validation.constraints.*;
import javax.validation.Valid;

import org.joda.time.LocalDate;

import io.swagger.annotations.ApiModelProperty;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * UpdateDailyMenu
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-05-16T15:54:16.690+03:00")

public class UpdateDailyMenu {

    @NotNull
    @JsonProperty("date")
    private LocalDate date = null;

    @NotNull
    @JsonProperty("version")
    private Integer version = null;

    @NotNull
    @Size(min = 0)
    @Valid
    @JsonProperty("foods")
    private List<DailyMenusFoods> foods = new ArrayList<DailyMenusFoods>();

    public List<Long> getFoodsIds() {
        List<Long> foodsIds = new ArrayList<>();
        for (DailyMenusFoods food : foods) {
            foodsIds.add(food.getId());
        }
        return foodsIds;
    }

    public UpdateDailyMenu date(LocalDate date) {
        this.date = date;
        return this;
    }

    /**
     * Get date
     *
     * @return date
     *
     */
    @ApiModelProperty(value = "")
    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public UpdateDailyMenu version(Integer version) {
        this.version = version;
        return this;
    }

    /**
     * Get version
     *
     * @return version
     *
     */
    @ApiModelProperty(value = "")
    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public UpdateDailyMenu foods(List<DailyMenusFoods> foods) {
        this.foods = foods;
        return this;
    }

    public UpdateDailyMenu addFoodsItem(DailyMenusFoods foodsItem) {
        this.foods.add(foodsItem);
        return this;
    }

    /**
     * Get foods
     *
     * @return foods
     *
     */
    @ApiModelProperty(value = "")
    public List<DailyMenusFoods> getFoods() {
        return foods;
    }

    public void setFoods(List<DailyMenusFoods> foods) {
        this.foods = foods;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UpdateDailyMenu updateDailyMenu = (UpdateDailyMenu) o;
        return Objects.equals(this.date, updateDailyMenu.date)
                && Objects.equals(this.version, updateDailyMenu.version)
                && Objects.equals(this.foods, updateDailyMenu.foods);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, version, foods);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class UpdateDailyMenu {\n");

        sb.append("    date: ").append(toIndentedString(date)).append("\n");
        sb.append("    version: ").append(toIndentedString(version)).append("\n");
        sb.append("    foods: ").append(toIndentedString(foods)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
