/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.api;

import javax.validation.Valid;
import javax.persistence.OptimisticLockException;

import io.swagger.annotations.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;

import org.bootcamp.validator.PathParamValidator;
import org.bootcamp.yum.api.model.chef.FoodList;
import org.bootcamp.yum.api.model.chef.DailyMenuChef;
import org.bootcamp.yum.api.model.chef.DailyMenusChef;
import org.bootcamp.yum.api.model.chef.UpdateDailyMenu;
import org.bootcamp.yum.exception.ConcurrentModificationException;
import org.bootcamp.yum.exception.ApiException;
import org.bootcamp.yum.service.DailyMenusService;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-27T16:35:28.866+03:00")

@Controller
public class DailyMenusApiController implements DailyMenusApi {

    private DailyMenusService dailyMenusService;

    @Autowired
    public DailyMenusApiController(DailyMenusService dailyMenusService) {
        this.dailyMenusService = dailyMenusService;
    }

    @PreAuthorize("hasAuthority('chef')")
    @Override
    public ResponseEntity<DailyMenusChef> dailyMenusMonthlyGet() throws ApiException {

        // Retrieve current month's concerned weeks dailyMenusChef
        DailyMenusChef dailyMenusChef = dailyMenusService.dailyMenusMonthlyGet();

        return new ResponseEntity<>(dailyMenusChef, HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('chef')")
    @Override
    public ResponseEntity<DailyMenusChef> dailyMenusMonthlyMonthYearGet(@ApiParam(value = "The requested month-Year in format MM-YYYY.", required = true)
            @PathVariable("monthYear") String monthYear) throws ApiException {

        // Validate monthYear parameter
        int[] monthYearArray = PathParamValidator.validateMonthYear(monthYear);

        // Retrieve requested month's concerned weeks dailyMenusChef
        DailyMenusChef dailyMenusChef = dailyMenusService.dailyMenusMonthlyMonthYearGet(monthYearArray[0], monthYearArray[1]);

        return new ResponseEntity<>(dailyMenusChef, HttpStatus.OK);

    }

    @PreAuthorize("hasAuthority('chef')")
    @Override
    public ResponseEntity<DailyMenuChef> dailyMenusIdPut(@ApiParam(value = "The daily menu's unique id.", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "The updated list of foods.", required = true) @Valid @RequestBody UpdateDailyMenu updateDailyMenu, Errors errors) throws ApiException {

        if (errors.hasErrors()) {
            throw new ApiException(errors.getFieldError().getField(), 400);
        }

        if (id <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        try {
            // Update DailyMenu
            DailyMenuChef dailyMenuChef = dailyMenusService.dailyMenusIdPut(id, updateDailyMenu);
            if (dailyMenuChef != null) {
                return new ResponseEntity<>(dailyMenuChef, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(HttpStatus.NO_CONTENT);
            }

        } catch (OptimisticLockException ex) {
            //DailyMenuChef dailyMenuChef = dailyMenusService.dailyMenusIdGet(id);
            //return new ResponseEntity<>(dailyMenuChef, HttpStatus.CONFLICT);
            throw new ConcurrentModificationException(dailyMenusService.dailyMenusIdGet(id));
        }
    }

    @PreAuthorize("hasAuthority('chef')")
    @Override
    public ResponseEntity<DailyMenuChef> dailyMenusPost(@ApiParam(value = "The list of foods.") @Valid @RequestBody FoodList foodList, Errors errors) throws ApiException {

        if (errors.hasErrors()) {
            throw new ApiException(errors.getFieldError().getField(), 400);
        }

        // Generate and store DailyMenu
        DailyMenuChef dailyMenuChef = dailyMenusService.dailyMenusPost(foodList);

        return new ResponseEntity<>(dailyMenuChef, HttpStatus.OK);
    }

}
