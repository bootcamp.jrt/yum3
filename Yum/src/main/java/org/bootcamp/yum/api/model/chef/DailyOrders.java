/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.api.model.chef;

import java.util.List;
import java.util.Objects;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * DailyOrders
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-05-02T17:54:48.065+03:00")

public class DailyOrders {

    @JsonProperty("summary")
    private List<GroupedFoods> summary = new ArrayList<GroupedFoods>();

    @JsonProperty("orders")
    private List<DailyOrder> orders = new ArrayList<DailyOrder>();

    public DailyOrders summary(List<GroupedFoods> summary) {
        this.summary = summary;
        return this;
    }

    public DailyOrders addSummaryItem(GroupedFoods summaryItem) {
        this.summary.add(summaryItem);
        return this;
    }

    /**
     * Get summary
     *
     * @return summary
     *
     */
    @ApiModelProperty(value = "")
    public List<GroupedFoods> getSummary() {
        return summary;
    }

    public void setSummary(List<GroupedFoods> summary) {
        this.summary = summary;
    }

    public DailyOrders orders(List<DailyOrder> orders) {
        this.orders = orders;
        return this;
    }

    public DailyOrders addOrdersItem(DailyOrder ordersItem) {
        this.orders.add(ordersItem);
        return this;
    }

    /**
     * Get orders
     *
     * @return orders
     *
     */
    @ApiModelProperty(value = "")
    public List<DailyOrder> getOrders() {
        return orders;
    }

    public void setOrders(List<DailyOrder> orders) {
        this.orders = orders;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DailyOrders dailyOrders = (DailyOrders) o;
        return Objects.equals(this.summary, dailyOrders.summary)
                && Objects.equals(this.orders, dailyOrders.orders);
    }

    @Override
    public int hashCode() {
        return Objects.hash(summary, orders);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class DailyOrders {\n");

        sb.append("    summary: ").append(toIndentedString(summary)).append("\n");
        sb.append("    orders: ").append(toIndentedString(orders)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
