/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.api.model;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * AccountSettings
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-27T16:35:28.866+03:00")

public class AccountSettings {

    @JsonProperty("firstName")
    private String firstName = null;

    @JsonProperty("lastName")
    private String lastName = null;

    @JsonProperty("email")
    private String email = null;

    @JsonProperty("role")
    private String role = null;

    @JsonProperty("approved")
    private Boolean approved = null;

    @JsonProperty("version")
    private Integer version = null;

    @JsonProperty("hasPicture")
    private Boolean hasPicture = null;

    public AccountSettings(String firstName, String lastName, String email, String role, boolean hasPicture, boolean approved, int version) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.role = role;
        this.hasPicture = hasPicture;
        this.approved = approved;
        this.version = version;
    }

    public AccountSettings() {
    }

    public AccountSettings firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    /**
     * Get firstName
     *
     * @return firstName
     *
     */
    @ApiModelProperty(value = "")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public AccountSettings lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    /**
     * Get lastName
     *
     * @return lastName
     *
     */
    @ApiModelProperty(value = "")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public AccountSettings email(String email) {
        this.email = email;
        return this;
    }

    /**
     * Get email
     *
     * @return email
     *
     */
    @ApiModelProperty(value = "")
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public AccountSettings role(String role) {
        this.role = role;
        return this;
    }

    /**
     * Get role
     *
     * @return role
     *
     */
    @ApiModelProperty(value = "")
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public AccountSettings approved(Boolean approved) {
        this.approved = approved;
        return this;
    }

    /**
     * Get approved
     *
     * @return approved
     *
     */
    @ApiModelProperty(value = "")
    public Boolean getApproved() {
        return approved;
    }

    public void setApproved(Boolean approved) {
        this.approved = approved;
    }

    public AccountSettings version(Integer version) {
        this.version = version;
        return this;
    }

    /**
     * Get version
     *
     * @return version
     *
     */
    @ApiModelProperty(value = "")
    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public AccountSettings hasPicture(Boolean hasPicture) {
        this.hasPicture = hasPicture;
        return this;
    }

    /**
     * Get hasPicture
     *
     * @return hasPicture
     *
     */
    @ApiModelProperty(value = "")
    public Boolean getHasPicture() {
        return hasPicture;
    }

    public void setHasPicture(Boolean hasPicture) {
        this.hasPicture = hasPicture;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        AccountSettings accountSettings = (AccountSettings) o;
        return Objects.equals(this.firstName, accountSettings.firstName)
                && Objects.equals(this.lastName, accountSettings.lastName)
                && Objects.equals(this.email, accountSettings.email)
                && Objects.equals(this.role, accountSettings.role)
                && Objects.equals(this.approved, accountSettings.approved)
                && Objects.equals(this.version, accountSettings.version)
                && Objects.equals(this.hasPicture, accountSettings.hasPicture);
    }

    @Override
    public int hashCode() {
        return Objects.hash(firstName, lastName, email, role, approved, version, hasPicture);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class AccountSettings {\n");

        sb.append("    firstName: ").append(toIndentedString(firstName)).append("\n");
        sb.append("    lastName: ").append(toIndentedString(lastName)).append("\n");
        sb.append("    email: ").append(toIndentedString(email)).append("\n");
        sb.append("    role: ").append(toIndentedString(role)).append("\n");
        sb.append("    approved: ").append(toIndentedString(approved)).append("\n");
        sb.append("    version: ").append(toIndentedString(version)).append("\n");
        sb.append("    hasPicture: ").append(toIndentedString(hasPicture)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
