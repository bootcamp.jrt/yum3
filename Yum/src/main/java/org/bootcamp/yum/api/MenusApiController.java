/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;

import io.swagger.annotations.*;

import org.bootcamp.yum.api.model.hungry.DailyMenus;
import org.bootcamp.yum.exception.ApiException;
import org.bootcamp.yum.service.MenusService;
import org.bootcamp.validator.PathParamValidator;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-03T16:57:49.718+03:00")

@Controller
public class MenusApiController implements MenusApi {

    private MenusService menusService;

    @Autowired
    public MenusApiController(MenusService menusService) {
        this.menusService = menusService;
    }

    @PreAuthorize("hasAuthority('hungry')")
    @Override
    public ResponseEntity<DailyMenus> menusWeeklyGet() throws ApiException {
        // Retrieve current week's daily menus
        return new ResponseEntity<>(menusService.menusWeeklyGet(), HttpStatus.OK);

    }

    @PreAuthorize("hasAuthority('hungry')")
    @Override
    public ResponseEntity<DailyMenus> menusWeeklyWeekYearGet(@ApiParam(value = "The week of the weekily menu in format WW-YYYY.", required = true) @PathVariable("weekYear") String weekYear) throws ApiException {

        // Validate weekYear parameter
        int[] weekYearArray = PathParamValidator.validateWeekYear(weekYear);
        // Retrieve requested week's daily menus passing week and year
        DailyMenus dailyMenusWeeklyList = menusService.menusWeeklyWeekYearGet(weekYearArray[0], weekYearArray[1]);
        return new ResponseEntity<>(dailyMenusWeeklyList, HttpStatus.OK);

    }

    @PreAuthorize("hasAuthority('hungry')")
    @Override
    public ResponseEntity<DailyMenus> menusMonthlyGet() throws ApiException {

        // Retrieve current month's concerned weeks daily menus
        DailyMenus dailyMenusMonthlyList = menusService.menusMonthlyGet();
        return new ResponseEntity<>(dailyMenusMonthlyList, HttpStatus.OK);

    }

    @PreAuthorize("hasAuthority('hungry')")
    @Override
    public ResponseEntity<DailyMenus> menusMonthlyMonthYearGet(@ApiParam(value = "The month-Year of the weekily menus in format MM-YYYY.", required = true) @PathVariable("monthYear") String monthYear) throws ApiException {

        // Validate monthYear parameter
        int[] monthYearArray = PathParamValidator.validateMonthYear(monthYear);
        // Retrieve requested month's concerned weeks daily menus
        DailyMenus dailyMenusMonthlyList = menusService.menusMonthlyMonthYearGet(monthYearArray[0], monthYearArray[1]);
        return new ResponseEntity<>(dailyMenusMonthlyList, HttpStatus.OK);

    }

}
