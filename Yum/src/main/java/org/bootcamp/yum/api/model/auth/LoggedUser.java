package org.bootcamp.yum.api.model.auth;

import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

/**
 * LoggedUser
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-05-24T11:19:29.836+03:00")

public class LoggedUser {

    @JsonProperty("id")
    private Long id = null;

    @JsonProperty("firstName")
    private String firstName = null;

    @JsonProperty("lastName")
    private String lastName = null;

    @JsonProperty("role")
    private String role = null;

    @JsonProperty("token")
    private String token = null;

    @JsonProperty("hasPicture")
    private Boolean hasPicture = null;

    public LoggedUser id(Long id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
  *
     */
    @ApiModelProperty(value = "")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public LoggedUser firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    /**
     * Get firstName
     *
     * @return firstName
  *
     */
    @ApiModelProperty(value = "")
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public LoggedUser lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    /**
     * Get lastName
     *
     * @return lastName
  *
     */
    @ApiModelProperty(value = "")
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public LoggedUser role(String role) {
        this.role = role;
        return this;
    }

    /**
     * Get role
     *
     * @return role
  *
     */
    @ApiModelProperty(value = "")
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public LoggedUser token(String token) {
        this.token = token;
        return this;
    }

    /**
     * Get token
     *
     * @return token
  *
     */
    @ApiModelProperty(value = "")
    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public LoggedUser hasPicture(Boolean hasPicture) {
        this.hasPicture = hasPicture;
        return this;
    }

    /**
     * Get hasPicture
     *
     * @return hasPicture
  *
     */
    @ApiModelProperty(value = "")
    public Boolean getHasPicture() {
        return hasPicture;
    }

    public void setHasPicture(Boolean hasPicture) {
        this.hasPicture = hasPicture;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LoggedUser loggedUser = (LoggedUser) o;
        return Objects.equals(this.id, loggedUser.id)
                && Objects.equals(this.firstName, loggedUser.firstName)
                && Objects.equals(this.lastName, loggedUser.lastName)
                && Objects.equals(this.role, loggedUser.role)
                && Objects.equals(this.token, loggedUser.token)
                && Objects.equals(this.hasPicture, loggedUser.hasPicture);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, role, token, hasPicture);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class LoggedUser {\n");

        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    firstName: ").append(toIndentedString(firstName)).append("\n");
        sb.append("    lastName: ").append(toIndentedString(lastName)).append("\n");
        sb.append("    role: ").append(toIndentedString(role)).append("\n");
        sb.append("    token: ").append(toIndentedString(token)).append("\n");
        sb.append("    hasPicture: ").append(toIndentedString(hasPicture)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
