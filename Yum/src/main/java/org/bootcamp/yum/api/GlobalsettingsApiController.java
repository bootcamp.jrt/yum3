/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.api;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.OptimisticLockException;
import javax.validation.Valid;

import io.swagger.annotations.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestBody;

import org.bootcamp.yum.api.model.GlobalSettings;
import org.bootcamp.yum.exception.ConcurrentModificationException;
import org.bootcamp.yum.exception.ApiException;
import org.bootcamp.yum.service.YumService;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-27T16:35:28.866+03:00")

@Controller
public class GlobalsettingsApiController implements GlobalsettingsApi {

    private YumService yumService;

    @Autowired
    public GlobalsettingsApiController(YumService yumService) {
        this.yumService = yumService;
    }

    @PreAuthorize("hasAuthority('hungry')")
    public ResponseEntity<GlobalSettings> globalsettingsGet() throws ApiException {

        return new ResponseEntity<GlobalSettings>(yumService.getGlobalsettingsDto(), HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('admin')")
    public ResponseEntity<Void> globalsettingsPut(@ApiParam(value = "The updated Global settings.", required = true) @Valid @RequestBody GlobalSettings updatedGlobalSettings, Errors errors) throws ApiException {

        if (errors.hasErrors()) {
            System.out.println(errors.getAllErrors());
            throw new ApiException(errors.getFieldError().getField(), 400);
        }
        try {
            yumService.globalsettingsPut(updatedGlobalSettings);
            return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
        } catch (OptimisticLockException ex) {
            try {
                GlobalSettings globalSettings = yumService.getGlobalsettingsDto();
                throw new ConcurrentModificationException(globalSettings);
            } catch (ApiException ex1) {
                Logger.getLogger(GlobalsettingsApiController.class.getName()).log(Level.SEVERE, null, ex1);
                throw new ApiException(500, "Concurrent modification exception: internal error");
            }
        }

    }

}
