/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.api;

import javax.validation.Valid;

import io.swagger.annotations.*;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;

import org.bootcamp.yum.api.model.GlobalSettings;
import org.bootcamp.yum.exception.ApiException;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-27T16:35:28.866+03:00")

@Api(value = "globalsettings", description = "the globalsettings API")
@RequestMapping(value = "/api")
public interface GlobalsettingsApi {

    @CrossOrigin
    @ApiOperation(value = "Gets the Global settings.", notes = "Returns the system's Global settings.", response = GlobalSettings.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"admin",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The Global settings.", response = GlobalSettings.class)})
    @RequestMapping(value = "/globalsettings",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<GlobalSettings> globalsettingsGet() throws ApiException;

    @CrossOrigin
    @ApiOperation(value = "Update the Global settings", notes = "Updates the Global settings", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"admin",})
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "Global settings updated.", response = Void.class)
        ,
        @ApiResponse(code = 400, message = "Bad request.", response = Void.class)
        ,
        @ApiResponse(code = 409, message = "Concurrent modification error.", response = Void.class)})
    @RequestMapping(value = "/globalsettings",
            produces = {"application/json"},
            method = RequestMethod.PUT)
    ResponseEntity<Void> globalsettingsPut(@ApiParam(value = "The updated Global settings.", required = true) @Valid @RequestBody GlobalSettings updatedGlobalSettings, Errors errors) throws ApiException;

}
