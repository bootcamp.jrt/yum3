/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.api;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.OptimisticLockException;
import javax.validation.Valid;

import io.swagger.annotations.ApiParam;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PathVariable;

import org.bootcamp.yum.api.model.Version;
import org.bootcamp.yum.api.model.chef.FoodItem;
import org.bootcamp.yum.api.model.chef.FoodItems;
import org.bootcamp.yum.service.FoodsService;
import org.bootcamp.yum.api.model.chef.FoodDetails;
import org.bootcamp.yum.api.model.chef.FoodItemEditable;
import org.bootcamp.yum.api.model.chef.UpdateFoodDetails;
import org.bootcamp.yum.exception.ConcurrentModificationException;
import org.bootcamp.yum.exception.ApiException;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-03-30T16:59:14.126+03:00")

@Controller
public class FoodsApiController implements FoodsApi {

    private FoodsService foodService;

    @Autowired
    public FoodsApiController(FoodsService foodService) {
        this.foodService = foodService;
    }

    @PreAuthorize("hasAuthority('chef')")
    @Override
    public ResponseEntity<FoodItems> foodsGet(@ApiParam(value = "A string to describe the client's foods version.") @RequestParam(value = "version", required = false) Integer version,
            @ApiParam(value = "A boolean to describe if the client requests the archived foods.") @RequestParam(value = "archived", required = false) Boolean archived,
            @ApiParam(value = "A string to describe the filtering.") @RequestParam(value = "type", required = false) String type,
            @ApiParam(value = "A string to describe the sorting preference.") @RequestParam(value = "sort", required = false) String sort,
            @ApiParam(value = "An integer to describe the requested page.") @RequestParam(value = "page", required = false) Integer page,
            @ApiParam(value = "An integer to describe the number of foods to retrieve.") @RequestParam(value = "size", required = false) Integer size) throws ApiException {

        if (version == null && archived == null && type == null && sort == null && page == null && size == null) {
            FoodItems foods = foodService.getDefaultFoodList();
            return new ResponseEntity<>(foods, HttpStatus.OK);
        }

        //set default values  in query params if its not defined
        version = version == null ? -1 : version;
        type = type == null ? "" : type;
        archived = archived == null ? false : archived;
        sort = sort == null ? "" : sort;
        page = page == null ? 0 : page >= 1 ? page - 1 : page;
        size = size == null ? 10 : size;

        FoodItems foods = foodService.foodsGet(version, archived, type, sort, page, size);

        return new ResponseEntity<>(foods, HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('chef')")
    @Override
    public ResponseEntity<FoodItemEditable> foodsIdGet(@ApiParam(value = "The food's unique id", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "A boolean to describe if the Food object is editable.") @RequestParam(value = "editable", required = false) Boolean editable) throws ApiException {

        if (editable != null && editable == true) {

            if (id <= 0) {
                throw new ApiException(400, "Bad Request");
            }

            return new ResponseEntity<FoodItemEditable>(foodService.foodsIdGet(id), HttpStatus.OK);

        }
        throw new ApiException(400, "Bad Request");
    }

    @PreAuthorize("hasAuthority('chef')")
    @Override
    public ResponseEntity<Object> foodsIdPut(@ApiParam(value = "The food's unique id.", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "The updated fields of the selected food object.", required = true) @RequestBody UpdateFoodDetails updateFoodDetails,
            @ApiParam(value = "A boolean to describe if the Food object has to be cloned.") @RequestParam(value = "clone", required = false) Boolean clone) throws ApiException {

        if (id <= 0) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        Version version;
        try {
            version = foodService.foodsIdPut(id, updateFoodDetails, clone);
            return new ResponseEntity<>(version, HttpStatus.OK);
        } catch (OptimisticLockException ex) {
            try {
                version = foodService.getFoodVersionById(id);
                throw new ConcurrentModificationException(version);
            } catch (ApiException ex1) {
                Logger.getLogger(OrdersApiController.class.getName()).log(Level.SEVERE, null, ex1);
                throw new ApiException(500, "Concurrent modification exception: internal error");
            }

        }
    }

    @PreAuthorize("hasAuthority('chef')")
    @Override
    public ResponseEntity<FoodItem> foodsFindByNameNameGet(@ApiParam(value = "The name of the requested food.", required = true)
            @PathVariable("name") String name) throws ApiException {

        return new ResponseEntity<>(foodService.foodsFindByNameNameGet(name), HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('chef')")
    @Override
    public ResponseEntity<Void> foodsPost(@ApiParam(value = "The food to create.")
            @Valid
            @RequestBody FoodDetails foodDetails, Errors errors) throws ApiException {

        if (errors.hasErrors()) {
            System.out.println(errors.getAllErrors());
            throw new ApiException(errors.getFieldError().getField(), 400);
        }

        // Generate and store food
        foodService.foodsPost(foodDetails);

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);

    }

    /**
     * Delete a food with the given id if it's not ordered yet, else set the
     * food as archived.
     *
     * @param id the id of the food.
     * @param archived optional param that let's user to decide if he want to
     * archive the food or not.
     * @return return ok if the food has successfully deleted or archived.
     */
    @PreAuthorize("hasAuthority('chef')")
    @Override
    public ResponseEntity<Void> foodsIdDelete(@ApiParam(value = "The food's unique id.", required = true)
            @PathVariable("id") Long id,
            @ApiParam(value = "A boolean to describe if the Food object has to be archived.") @RequestParam(value = "archived", required = false) Boolean archived) throws ApiException {

        foodService.foodsIdDelete(id, archived);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
