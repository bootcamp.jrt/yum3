/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.api;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.joda.time.LocalDate;

import io.swagger.annotations.*;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import org.bootcamp.yum.api.model.ErrorWithDto;
import org.bootcamp.yum.api.model.hungry.DailyMenu;
import org.bootcamp.yum.api.model.chef.DailyOrders;
import org.bootcamp.yum.api.model.chef.DailyOrdersChef;
import org.bootcamp.yum.api.model.hungry.MenuInfo;
import org.bootcamp.yum.api.model.hungry.OrderDetails;
import org.bootcamp.yum.api.model.Version;
import org.bootcamp.yum.api.model.hungry.UpdateDailyOrder;
import org.bootcamp.yum.exception.ApiException;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-27T16:35:28.866+03:00")

@Api(value = "orders", description = "the orders API")
@RequestMapping(value = "/api")
public interface OrdersApi {

    @CrossOrigin
    @ApiOperation(value = "Gets the current days's orders.", notes = "Returns the current days's orders.", response = DailyOrders.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"chef",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Requested day's orders.", response = DailyOrders.class)
        ,
        @ApiResponse(code = 400, message = "Bad request.", response = DailyOrders.class)})
    @RequestMapping(value = "/orders/daily/{day}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<DailyOrders> ordersDailyDayGet(@ApiParam(value = "The requested day.", required = true) @PathVariable("day") @DateTimeFormat(pattern = "YYYY-MM-dd") LocalDate day) throws ApiException;

    @CrossOrigin
    @ApiOperation(value = "Deletes the order with the given id.", notes = "Deletes an order by its id.", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"hungry",})
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "Order deleted.", response = Void.class)
        ,
        @ApiResponse(code = 410, message = "Concurrent deletion error.", response = ErrorWithDto.class)
        ,
        @ApiResponse(code = 412, message = "The order is final.", response = Void.class)})
    @RequestMapping(value = "/orders/{id}",
            produces = {"application/json"},
            method = RequestMethod.DELETE)
    ResponseEntity<Void> ordersIdDelete(@ApiParam(value = "The order's unique id.", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "Holds information about the related menu (menu id, menu version, date).", required = true) @Valid @RequestBody MenuInfo menuInfo, Errors errors) throws ApiException;

    @CrossOrigin
    @ApiOperation(value = "Gets the requested daily order's details.", notes = "Gets the requested daily order's details.", response = DailyMenu.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"hungry",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The last version of the requested daily order's DailyMenu.", response = DailyMenu.class)
        ,
        @ApiResponse(code = 410, message = "Concurrent deletion error.", response = ErrorWithDto.class)
        ,
        @ApiResponse(code = 412, message = "Order deadline has passed.", response = Void.class)})
    @RequestMapping(value = "/orders/{id}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<DailyMenu> ordersIdGet(@ApiParam(value = "The order's unique id", required = true) @PathVariable("id") Long id,
            @NotNull @ApiParam(value = "The menu's unique id", required = true) @RequestParam(value = "menuId", required = true) Long menuId,
            @NotNull @ApiParam(value = "The menu's version", required = true) @RequestParam(value = "menuVersion", required = true) Integer menuVersion,
            @NotNull @ApiParam(value = "The menu's date", required = true) @RequestParam(value = "date", required = true) @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date) throws ApiException;

    @CrossOrigin
    @ApiOperation(value = "Updates the specified Daily Order.", notes = "Updates the modified fields for a specific Daily Order.", response = Version.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"hungry",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Order updated.", response = Version.class)
        ,
        @ApiResponse(code = 400, message = "Bad Request.", response = Void.class)
        ,
        @ApiResponse(code = 409, message = "Concurrent modification error.", response = DailyMenu.class)
        ,
        @ApiResponse(code = 410, message = "Concurrent deletion error.", response = ErrorWithDto.class)
        ,
        @ApiResponse(code = 412, message = "Order deadline has passed.", response = Void.class)})
    @RequestMapping(value = "/orders/{id}",
            produces = {"application/json"},
            method = RequestMethod.PUT)
    ResponseEntity<Version> ordersIdPut(@ApiParam(value = "Daily Order's Id.", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "The updated fields of the selected Daily Order object.", required = true) @Valid @RequestBody UpdateDailyOrder updateDailyOrder, Errors errors) throws ApiException;

    @CrossOrigin
    @ApiOperation(value = "Gets the current month's concerned weeks dailyMenus and order history.", notes = "Returns a list with current month's concerned weeks dailyMenus and orders.", response = DailyOrdersChef.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"chef",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Current month's concerned weeks list of dailyMenus and orders.", response = DailyOrdersChef.class)})
    @RequestMapping(value = "/orders/monthly",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<DailyOrdersChef> ordersMonthlyGet() throws ApiException;

    @CrossOrigin
    @ApiOperation(value = "Gets requested month's concerned weeks dailyMenus and order history.", notes = "Returns a list with the requested month's concerned weeks dailyMenus and orders.", response = DailyOrdersChef.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"chef",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Requested month's concerned weeks list of dailyMenus and orders.", response = DailyOrdersChef.class)
        ,
        @ApiResponse(code = 400, message = "Requested month-Year doesn't exist.", response = DailyOrdersChef.class)})
    @RequestMapping(value = "/orders/monthly/{monthYear}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<DailyOrdersChef> ordersMonthlyMonthYearGet(@ApiParam(value = "The requested month-Year in format MM-YYYY.", required = true) @PathVariable("monthYear") String monthYear) throws ApiException;

    @CrossOrigin
    @ApiOperation(value = "Places a new order.", notes = "Places a new order with the given order details.", response = DailyMenu.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"hungry",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Order placed successfully. Returns the updated Daily Menu.", response = DailyMenu.class)
        ,
        @ApiResponse(code = 400, message = "Invalid request.", response = Void.class)
        ,
        @ApiResponse(code = 409, message = "Order already exists.", response = ErrorWithDto.class)
        ,
        @ApiResponse(code = 410, message = "Concurrent deletion error.", response = ErrorWithDto.class)
        ,
        @ApiResponse(code = 412, message = "Order deadline has passed.", response = Void.class)})
    @RequestMapping(value = "/orders",
            produces = {"application/json"},
            method = RequestMethod.POST)
    ResponseEntity<DailyMenu> ordersPost(@ApiParam(value = "An array of order items (food id, quantity) for the specific date.") @Valid @RequestBody OrderDetails orderDetails, Errors errors) throws ApiException;

}
