/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.api.model.chef;

import java.util.List;
import java.util.Objects;
import java.util.ArrayList;

import org.joda.time.LocalDate;

import io.swagger.annotations.ApiModelProperty;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.bootcamp.yum.api.model.OrderItem;

/**
 * DailyOrderChef
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-27T16:35:28.866+03:00")

public class DailyOrderChef {

    @JsonProperty("dailyMenuId")
    private Long dailyMenuId = null;

    @JsonProperty("date")
    private LocalDate date = null;

    @JsonProperty("foods")
    private List<OrderItem> foods = new ArrayList<OrderItem>();

    public DailyOrderChef dailyMenuId(Long dailyMenuId) {
        this.dailyMenuId = dailyMenuId;
        return this;
    }

    /**
     * Get dailyMenuId
     *
     * @return dailyMenuId
     *
     */
    @ApiModelProperty(value = "")
    public Long getDailyMenuId() {
        return dailyMenuId;
    }

    public void setDailyMenuId(Long dailyMenuId) {
        this.dailyMenuId = dailyMenuId;
    }

    public DailyOrderChef date(LocalDate date) {
        this.date = date;
        return this;
    }

    /**
     * Get date
     *
     * @return date
     *
     */
    @ApiModelProperty(value = "")
    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public DailyOrderChef foods(List<OrderItem> foods) {
        this.foods = foods;
        return this;
    }

    public DailyOrderChef addFoodsItem(OrderItem foodsItem) {
        this.foods.add(foodsItem);
        return this;
    }

    /**
     * Get foods
     *
     * @return foods
     *
     */
    @ApiModelProperty(value = "")
    public List<OrderItem> getFoods() {
        return foods;
    }

    public void setFoods(List<OrderItem> foods) {
        this.foods = foods;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DailyOrderChef dailyOrderChef = (DailyOrderChef) o;
        return Objects.equals(this.dailyMenuId, dailyOrderChef.dailyMenuId)
                && Objects.equals(this.date, dailyOrderChef.date)
                && Objects.equals(this.foods, dailyOrderChef.foods);
    }

    @Override
    public int hashCode() {
        return Objects.hash(dailyMenuId, date, foods);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class DailyOrderChef {\n");

        sb.append("    dailyMenuId: ").append(toIndentedString(dailyMenuId)).append("\n");
        sb.append("    date: ").append(toIndentedString(date)).append("\n");
        sb.append("    foods: ").append(toIndentedString(foods)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
