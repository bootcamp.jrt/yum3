/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.api.model.hungry;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.swagger.annotations.ApiModelProperty;

import org.joda.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.bootcamp.yum.api.model.chef.Food;

/**
 * DailyMenu
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-05-10T18:56:23.977+03:00")

public class DailyMenu {

    @JsonProperty("id")
    private Long id = null;

    @JsonProperty("date")
    private LocalDate date = null;

    @JsonProperty("orderId")
    private Long orderId = null;

    @JsonProperty("orderVersion")
    private Integer orderVersion = null;

    @JsonProperty("final")
    private Boolean _final = null;

    @JsonProperty("version")
    private Integer version = null;

    @JsonProperty("foods")
    private List<Food> foods = new ArrayList<Food>();

    public DailyMenu id(Long id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
     *
     */
    @ApiModelProperty(value = "")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DailyMenu date(LocalDate date) {
        this.date = date;
        return this;
    }

    /**
     * Get date
     *
     * @return date
     *
     */
    @ApiModelProperty(value = "")
    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public DailyMenu orderId(Long orderId) {
        this.orderId = orderId;
        return this;
    }

    /**
     * Get orderId
     *
     * @return orderId
     *
     */
    @ApiModelProperty(value = "")
    public Long getOrderId() {
        return orderId;
    }

    public void setOrderId(Long orderId) {
        this.orderId = orderId;
    }

    public DailyMenu orderVersion(Integer orderVersion) {
        this.orderVersion = orderVersion;
        return this;
    }

    /**
     * Get orderVersion
     *
     * @return orderVersion
     *
     */
    @ApiModelProperty(value = "")
    public Integer getOrderVersion() {
        return orderVersion;
    }

    public void setOrderVersion(Integer orderVersion) {
        this.orderVersion = orderVersion;
    }

    public DailyMenu _final(Boolean _final) {
        this._final = _final;
        return this;
    }

    /**
     * Get _final
     *
     * @return _final
     *
     */
    @ApiModelProperty(value = "")
    public Boolean getFinal() {
        return _final;
    }

    public void setFinal(Boolean _final) {
        this._final = _final;
    }

    public DailyMenu version(Integer version) {
        this.version = version;
        return this;
    }

    /**
     * Get version
     *
     * @return version
     *
     */
    @ApiModelProperty(value = "")
    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public DailyMenu foods(List<Food> foods) {
        this.foods = foods;
        return this;
    }

    public DailyMenu addFoodsItem(Food foodsItem) {
        this.foods.add(foodsItem);
        return this;
    }

    /**
     * Get foods
     *
     * @return foods
     *
     */
    @ApiModelProperty(value = "")
    public List<Food> getFoods() {
        return foods;
    }

    public void setFoods(List<Food> foods) {
        this.foods = foods;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DailyMenu dailyMenu = (DailyMenu) o;
        return Objects.equals(this.id, dailyMenu.id)
                && Objects.equals(this.date, dailyMenu.date)
                && Objects.equals(this.orderId, dailyMenu.orderId)
                && Objects.equals(this.orderVersion, dailyMenu.orderVersion)
                && Objects.equals(this._final, dailyMenu._final)
                && Objects.equals(this.version, dailyMenu.version)
                && Objects.equals(this.foods, dailyMenu.foods);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, orderId, orderVersion, _final, version, foods);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class DailyMenu {\n");

        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    date: ").append(toIndentedString(date)).append("\n");
        sb.append("    orderId: ").append(toIndentedString(orderId)).append("\n");
        sb.append("    orderVersion: ").append(toIndentedString(orderVersion)).append("\n");
        sb.append("    _final: ").append(toIndentedString(_final)).append("\n");
        sb.append("    version: ").append(toIndentedString(version)).append("\n");
        sb.append("    foods: ").append(toIndentedString(foods)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
