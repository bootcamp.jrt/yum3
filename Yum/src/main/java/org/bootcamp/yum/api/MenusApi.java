/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.api;


import io.swagger.annotations.*;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;

import org.bootcamp.yum.exception.ApiException;
import org.bootcamp.yum.api.model.hungry.DailyMenus;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-27T16:35:28.866+03:00")

@Api(value = "menus", description = "the menus API")
@RequestMapping(value = "/api")
public interface MenusApi {

    @CrossOrigin
    @ApiOperation(value = "Gets the current month's concerned weeks dailyMenus and order history.", notes = "Returns a list with current month's concerned weeks dailyMenus and orders.", response = DailyMenus.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"hungry",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Current month's concerned weeks list of dailyMenus and orders.", response = DailyMenus.class)})
    @RequestMapping(value = "/menus/monthly",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<DailyMenus> menusMonthlyGet() throws ApiException;

    @CrossOrigin
    @ApiOperation(value = "Gets requested month's concerned weeks dailyMenus and order history.", notes = "Returns a list with the requested month's concerned weeks dailyMenus and orders.", response = DailyMenus.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"hungry",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Requested month's concerned weeks list of dailyMenus and orders.", response = DailyMenus.class)
        ,
        @ApiResponse(code = 400, message = "Bad Request.", response = DailyMenus.class)})
    @RequestMapping(value = "/menus/monthly/{monthYear}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<DailyMenus> menusMonthlyMonthYearGet(@ApiParam(value = "The month-Year of the weekily menus in format MM-YYYY.", required = true) @PathVariable("monthYear") String monthYear) throws ApiException;

    @CrossOrigin
    @ApiOperation(value = "Gets current weekly menu.", notes = "Returns a list of daily menus.", response = DailyMenus.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"hungry",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Current week's list of daily menus.", response = DailyMenus.class)})
    @RequestMapping(value = "/menus/weekly",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<DailyMenus> menusWeeklyGet() throws ApiException;

    @CrossOrigin
    @ApiOperation(value = "Gets requested week's menu.", notes = "Returns a  list of daily menus.", response = DailyMenus.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"hungry",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Requested week's list of daily menus.", response = DailyMenus.class)
        ,
        @ApiResponse(code = 400, message = "Bad Request.", response = DailyMenus.class)})
    @RequestMapping(value = "/menus/weekly/{weekYear}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<DailyMenus> menusWeeklyWeekYearGet(@ApiParam(value = "The week of the weekily menu in format WW-YYYY.", required = true) @PathVariable("weekYear") String weekYear) throws ApiException;

}
