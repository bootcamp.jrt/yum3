/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.api;

import java.io.ByteArrayInputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.validation.Valid;

import javax.validation.constraints.NotNull;
import javax.persistence.OptimisticLockException;

import io.swagger.annotations.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.stereotype.Controller;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import org.bootcamp.yum.api.model.AccountSettings;
import org.bootcamp.yum.api.model.admin.NewUser;
import org.bootcamp.yum.api.model.admin.Users;
import org.bootcamp.yum.api.model.admin.UpdateAccountSettings;
import org.bootcamp.yum.exception.ConcurrentModificationException;
import org.bootcamp.yum.exception.ApiException;
import org.bootcamp.yum.service.UserService;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-27T16:35:28.866+03:00")

@Controller
public class UsersApiController implements UsersApi {

    private UserService userService;

    @Autowired
    public UsersApiController(UserService userService) {
        this.userService = userService;
    }

    @PreAuthorize("hasAuthority('admin')")
    @Override
    public ResponseEntity<Users> usersGet(@ApiParam(value = "A string to describe the sorting preference.") @RequestParam(value = "sort", required = false) String sort,
            @ApiParam(value = "An integer to describe the requested page.") @RequestParam(value = "page", required = false) Integer page,
            @ApiParam(value = "An integer to describe the number of users to retrieve.") @RequestParam(value = "size", required = false) Integer size,
            @ApiParam(value = "A string to describe the sorting direction.") @RequestParam(value = "order", required = false) String order) throws ApiException {

        if (sort == null && page == null && size == null && order == null) {
            return new ResponseEntity<>(userService.getAllUsers(), HttpStatus.OK);
        } else {
            sort = sort == null ? "asc" : sort;
            page = page == null ? 0 : page;
            size = size == null ? 10 : size;
            order = order == null ? "regdate" : order;

            return new ResponseEntity<>(userService.usersGet(sort, page, size, order), HttpStatus.OK);
        }
    }

    @PreAuthorize("hasAuthority('admin')")
    @Override
    public ResponseEntity<Void> usersIdApprovePut(@ApiParam(value = "The user id to switch approve status", required = true) @PathVariable("id") Long id,
            @NotNull @ApiParam(value = "A boolean to describe the approve status preference for a user", required = true) @RequestParam(value = "approved", required = true) Boolean approved,
            @ApiParam(value = "A boolean to describe if the deletion of non-final orders is forced") @RequestParam(value = "force", required = false, defaultValue = "false") Boolean force) throws ApiException {
        if (id < 1) {
            throw new ApiException(400, "Bad Request");
        }
        userService.setApprovedStatus(id, approved, force);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PreAuthorize("hasAuthority('admin')")
    @Override
    public ResponseEntity<Void> usersIdDelete(@ApiParam(value = "The user's unique id.", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "A boolean to describe if the deletion of non-final orders is forced.") @RequestParam(value = "force", required = false, defaultValue = "false") Boolean force) throws ApiException {

        if (id < 1) {
            throw new ApiException(400, "Bad Request");
        }
        System.out.println("in controller");

        userService.deleteUser(id, force);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PreAuthorize("hasAuthority('admin')")
    @Override
    public ResponseEntity<Void> usersIdForgotpwdPost(@ApiParam(value = "The user id to reset password", required = true) @PathVariable("id") Long id) throws ApiException {

        userService.usersIdForgotpwd(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PreAuthorize("hasAuthority('admin')")
    @Override
    public ResponseEntity<AccountSettings> usersIdGet(@ApiParam(value = "The requested user id.", required = true) @PathVariable("id") Long id) throws ApiException {

        if (id < 1) {
            throw new ApiException(400, "Bad Request");
        }
        return new ResponseEntity<>(userService.usersIdGet(id), HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('admin')")
    @Override
    public ResponseEntity<Void> usersIdPictureDelete(@ApiParam(value = "The user's id to delete the picture", required = true) @PathVariable("id") Long id) throws ApiException {

        userService.removeUserPicture(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PreAuthorize("hasAuthority('admin')")
    @Override
    public ResponseEntity<Void> usersIdPicturePost(@ApiParam(value = "The user's id to upload the picture", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "file detail") @RequestPart("file") MultipartFile newpicture) throws ApiException {

        if (id <= 0 || newpicture.isEmpty()) {
            throw new ApiException(400, "Bad request");
        }
        userService.usersIdPicturePost(id, newpicture);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Override
    public ResponseEntity<Object> usersIdPictureGet(@ApiParam(value = "The requested user id.", required = true) @PathVariable("id") Long id,
            @NotNull @ApiParam(value = "The admin's token.", required = true) @RequestParam(value = "token", required = true) String token) {

        try {
            if (id <= 0 || token.isEmpty()) {
                throw new ApiException(400, "Bad request");
            }
            // get user picture as bytes array
            byte[] userPicBytes = userService.usersIdPictureGet(id, token);
            ByteArrayInputStream inputStream = new ByteArrayInputStream(userPicBytes);
            return ResponseEntity
                    .ok()
                    .contentLength(userPicBytes.length)
                    .contentType(MediaType.parseMediaType("image/jpeg"))
                    .body(new InputStreamResource(inputStream));
        } catch (ApiException ex) {
            return new ResponseEntity(HttpStatus.valueOf(ex.getCode()));
        }

    }

    @PreAuthorize("hasAuthority('admin')")
    @Override
    public ResponseEntity<Void> usersIdPut(@ApiParam(value = "The requested user id.", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "The updated account settings.", required = true) @Valid @RequestBody UpdateAccountSettings updatedAccountSettings, Errors errors) throws ApiException {

        if (id < 1 || errors.hasErrors()) {
            System.out.println(errors.getAllErrors());
            throw new ApiException(errors.getFieldError().getField(), 400);
        }
        try {
            userService.usersIdPut(id, updatedAccountSettings);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (OptimisticLockException ex) {
            try {
                AccountSettings accountSettings = userService.usersIdGet(id);
                throw new ConcurrentModificationException(accountSettings);
            } catch (ApiException ex1) {
                Logger.getLogger(UsersApiController.class.getName()).log(Level.SEVERE, null, ex1);
                throw new ApiException(500, "Concurrent modification exception: internal error");
            }
        }

    }

    @PreAuthorize("hasAuthority('admin')")
    @Override
    public ResponseEntity<Void> usersPost(@ApiParam(value = "The user to create.") @Valid @RequestBody NewUser newUser, Errors errors) throws ApiException {

        if (errors.hasErrors()) {
            System.out.println(errors.getAllErrors());
            throw new ApiException(errors.getFieldError().getField(), 400);
        }
        userService.usersPost(newUser);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
