/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.api.model.hungry;

import java.util.Objects;

import javax.validation.constraints.*;

import org.joda.time.LocalDate;

import io.swagger.annotations.ApiModelProperty;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * MenuInfo
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-05-15T10:41:58.894+03:00")

public class MenuInfo {

    @NotNull
    @JsonProperty("date")
    private LocalDate date = null;
    @NotNull
    @Min(0)
    @JsonProperty("menuVersion")
    private Integer menuVersion = null;
    @NotNull
    @Min(1)
    @JsonProperty("menuId")
    private Long menuId = null;

    public MenuInfo date(LocalDate date) {
        this.date = date;
        return this;
    }

    /**
     * Get date
     *
     * @return date
     *
     */
    @ApiModelProperty(value = "")
    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public MenuInfo menuVersion(Integer menuVersion) {
        this.menuVersion = menuVersion;
        return this;
    }

    /**
     * Get menuVersion
     *
     * @return menuVersion
     *
     */
    @ApiModelProperty(value = "")
    public Integer getMenuVersion() {
        return menuVersion;
    }

    public void setMenuVersion(Integer menuVersion) {
        this.menuVersion = menuVersion;
    }

    public MenuInfo menuId(Long menuId) {
        this.menuId = menuId;
        return this;
    }

    /**
     * Get menuId
     *
     * @return menuId
     *
     */
    @ApiModelProperty(value = "")
    public Long getMenuId() {
        return menuId;
    }

    public void setMenuId(Long menuId) {
        this.menuId = menuId;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MenuInfo menuInfo = (MenuInfo) o;
        return Objects.equals(this.date, menuInfo.date)
                && Objects.equals(this.menuVersion, menuInfo.menuVersion)
                && Objects.equals(this.menuId, menuInfo.menuId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, menuVersion, menuId);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class MenuInfo {\n");

        sb.append("    date: ").append(toIndentedString(date)).append("\n");
        sb.append("    menuVersion: ").append(toIndentedString(menuVersion)).append("\n");
        sb.append("    menuId: ").append(toIndentedString(menuId)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
