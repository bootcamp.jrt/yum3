/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.api.model.admin;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import io.swagger.annotations.ApiModelProperty;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Users
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-05-08T11:14:23.126+03:00")

public class Users {

    @JsonProperty("users")
    private List<User> users = new ArrayList<User>();

    @JsonProperty("totalPagesNumber")
    private Integer totalPagesNumber = null;

    public Users users(List<User> users) {
        this.users = users;
        return this;
    }

    public Users addUsersItem(User usersItem) {
        this.users.add(usersItem);
        return this;
    }

    /**
     * Get users
     *
     * @return users
     *
     */
    @ApiModelProperty(value = "")
    public List<User> getUsers() {
        return users;
    }

    public void addUsers(User user) {
        this.users.add(user);
    }

    public Users totalPagesNumber(Integer totalPagesNumber) {
        this.totalPagesNumber = totalPagesNumber;
        return this;
    }

    /**
     * Get totalPagesNumber
     *
     * @return totalPagesNumber
     *
     */
    @ApiModelProperty(value = "")
    public Integer getTotalPagesNumber() {
        return totalPagesNumber;
    }

    public void setTotalPagesNumber(Integer totalPagesNumber) {
        this.totalPagesNumber = totalPagesNumber;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Users users = (Users) o;
        return Objects.equals(this.users, users.users)
                && Objects.equals(this.totalPagesNumber, users.totalPagesNumber);
    }

    @Override
    public int hashCode() {
        return Objects.hash(users, totalPagesNumber);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Users {\n");

        sb.append("    users: ").append(toIndentedString(users)).append("\n");
        sb.append("    totalPagesNumber: ").append(toIndentedString(totalPagesNumber)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
