/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.api;

import java.io.File;

import javax.validation.constraints.NotNull;
import javax.validation.Valid;

import io.swagger.annotations.*;

import org.springframework.validation.Errors;
import org.springframework.http.ResponseEntity;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;

import org.bootcamp.yum.api.model.AccountSettings;
import org.bootcamp.yum.api.model.admin.NewUser;
import org.bootcamp.yum.api.model.admin.UpdateAccountSettings;
import org.bootcamp.yum.api.model.admin.Users;
import org.bootcamp.yum.exception.ApiException;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-27T16:35:28.866+03:00")

@Api(value = "users", description = "the users API")
@RequestMapping(value = "/api")
public interface UsersApi {

    @CrossOrigin
    @ApiOperation(value = "Gets the users of the system.", notes = "Returns a list with all the users of the system.", response = Users.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"admin",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The users of the system.", response = Users.class)})
    @RequestMapping(value = "/users",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<Users> usersGet(@ApiParam(value = "A string to describe the sorting preference.") @RequestParam(value = "sort", required = false) String sort,
            @ApiParam(value = "An integer to describe the requested page.") @RequestParam(value = "page", required = false) Integer page,
            @ApiParam(value = "An integer to describe the number of users to retrieve.") @RequestParam(value = "size", required = false) Integer size,
            @ApiParam(value = "A string to describe the sorting direction.") @RequestParam(value = "order", required = false) String order) throws ApiException;

    @CrossOrigin
    @ApiOperation(value = "", notes = "Allows admin to switch approve status of a user", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"admin",})
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "Approve status changed ( No orders OR only final orders )", response = Void.class)
        ,
        @ApiResponse(code = 404, message = "If the given id doesn't exists in the database", response = Void.class)
        ,
        @ApiResponse(code = 409, message = "Mixed orders OR only non-final orders", response = Void.class)})
    @RequestMapping(value = "/users/{id}/approve",
            produces = {"application/json"},
            method = RequestMethod.PUT)
    ResponseEntity<Void> usersIdApprovePut(@ApiParam(value = "The user id to switch approve status", required = true) @PathVariable("id") Long id,
            @NotNull @ApiParam(value = "A boolean to describe the approve status preference for a user", required = true) @RequestParam(value = "approved", required = true) Boolean approved,
            @ApiParam(value = "A boolean to describe if the deletion of non-final orders is forced") @RequestParam(value = "force", required = false, defaultValue = "false") Boolean force) throws ApiException;

    @CrossOrigin
    @ApiOperation(value = "Delete or archive the user with the given id.", notes = "Delete or archive a user by its id.", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"admin",})
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "User deleted.", response = Void.class)
        ,
        @ApiResponse(code = 402, message = "User has final and non-final orders.", response = Void.class)
        ,
        @ApiResponse(code = 404, message = "User not found.", response = Void.class)
        ,
        @ApiResponse(code = 409, message = "User has only non-final orders.", response = Void.class)
        ,
        @ApiResponse(code = 412, message = "User has only final orders.", response = Void.class)})
    @RequestMapping(value = "/users/{id}",
            produces = {"application/json"},
            method = RequestMethod.DELETE)
    ResponseEntity<Void> usersIdDelete(@ApiParam(value = "The user's unique id.", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "A boolean to describe if the deletion of non-final orders is forced.") @RequestParam(value = "force", required = false, defaultValue = "false") Boolean force) throws ApiException;

    @CrossOrigin
    @ApiOperation(value = "", notes = "Allow admin to reset a user's password", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"admin",})
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "Email address found and reset link sent to the user's email", response = Void.class)
        ,
        @ApiResponse(code = 404, message = "If the given id doesn't exists in the database", response = Void.class)})
    @RequestMapping(value = "/users/{id}/forgotpwd",
            produces = {"application/json"},
            method = RequestMethod.POST)
    ResponseEntity<Void> usersIdForgotpwdPost(@ApiParam(value = "The user id to reset password", required = true) @PathVariable("id") Long id) throws ApiException;

    @CrossOrigin
    @ApiOperation(value = "Gets the requested user.", notes = "Returns the requested user's profile details.", response = AccountSettings.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"admin",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The requested user.", response = AccountSettings.class)
        ,
        @ApiResponse(code = 404, message = "Requested user doesn't exist.", response = AccountSettings.class)})
    @RequestMapping(value = "/users/{id}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<AccountSettings> usersIdGet(@ApiParam(value = "The requested user id.", required = true) @PathVariable("id") Long id) throws ApiException;

    @CrossOrigin
    @ApiOperation(value = "Deletes the requested user's picture", notes = "Deletes the requested user's picture", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"admin",})
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "Requested user's picture deleted.", response = Void.class)
        ,
        @ApiResponse(code = 400, message = "Bad request.", response = Void.class)
        ,
        @ApiResponse(code = 404, message = "User not found.", response = Void.class)})
    @RequestMapping(value = "/users/{id}/picture",
            produces = {"application/json"},
            method = RequestMethod.DELETE)
    ResponseEntity<Void> usersIdPictureDelete(@ApiParam(value = "The user's id to delete the picture", required = true) @PathVariable("id") Long id) throws ApiException;

    @CrossOrigin
    @ApiOperation(value = "", notes = "Allow admin to upload a new user's picture", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"admin",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Returns requested user's picture", response = Void.class)
        ,
        @ApiResponse(code = 400, message = "Bad Request.", response = Void.class)
        ,
        @ApiResponse(code = 404, message = "If the given id doesn't exists in the database", response = Void.class)})
    @RequestMapping(value = "/users/{id}/picture",
            produces = {"application/json"},
            consumes = {"multipart/form-data"},
            method = RequestMethod.POST)
    ResponseEntity<Void> usersIdPicturePost(@ApiParam(value = "The user's id to upload the picture", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "file detail") @RequestPart("file") MultipartFile newpicture) throws ApiException;

    @CrossOrigin
    @ApiOperation(value = "Gets the requested user's picture.", notes = "Returns the requested user's picture.", response = File.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"admin",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Returns requested user's picture", response = File.class)
        ,
        @ApiResponse(code = 400, message = "Bad Request.", response = File.class)
        ,
        @ApiResponse(code = 404, message = "User or picture not found.", response = File.class)})
    @RequestMapping(value = "/users/{id}/picture/token",
            produces = {"image/jpeg"},
            method = RequestMethod.GET)
    ResponseEntity<Object> usersIdPictureGet(@ApiParam(value = "The requested user id.", required = true) @PathVariable("id") Long id,
            @NotNull @ApiParam(value = "The admin's token.", required = true) @RequestParam(value = "token", required = true) String token);

    @CrossOrigin
    @ApiOperation(value = "Update the user with the given id", notes = "Updates the user's with the specified id settings.", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"admin",})
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "User's settings updated.", response = Void.class)
        ,
        @ApiResponse(code = 400, message = "Bad request.", response = Void.class)
        ,
        @ApiResponse(code = 404, message = "User not found.", response = Void.class)
        ,
        @ApiResponse(code = 409, message = "Concurrent modification error.", response = Void.class)
        ,
        @ApiResponse(code = 412, message = "User already exists.", response = Void.class)})
    @RequestMapping(value = "/users/{id}",
            produces = {"application/json"},
            method = RequestMethod.PUT)
    ResponseEntity<Void> usersIdPut(@ApiParam(value = "The requested user id.", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "The updated account settings.", required = true) @Valid @RequestBody UpdateAccountSettings updatedAccountSettings, Errors errors) throws ApiException;

    @CrossOrigin
    @ApiOperation(value = "Adds a new user in the system.", notes = "Adds a new user in the system with the given details.", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"admin",})
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "User created successfully.", response = Void.class)
        ,
        @ApiResponse(code = 400, message = "User could not be created.", response = Void.class)
        ,
        @ApiResponse(code = 412, message = "User already exists.", response = Void.class)})
    @RequestMapping(value = "/users",
            produces = {"application/json"},
            method = RequestMethod.POST)
    ResponseEntity<Void> usersPost(@ApiParam(value = "The user to create.") @RequestBody NewUser newUser, Errors errors) throws ApiException;

}
