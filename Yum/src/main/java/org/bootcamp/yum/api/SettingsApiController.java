/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.api;

import java.io.ByteArrayInputStream;
import java.util.logging.Logger;
import java.util.logging.Level;

import javax.persistence.OptimisticLockException;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.*;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestPart;

import org.bootcamp.yum.api.model.AccountDetails;
import org.bootcamp.yum.api.model.AccountSettings;
import org.bootcamp.yum.exception.ConcurrentModificationException;
import org.bootcamp.yum.exception.ApiException;
import org.bootcamp.yum.service.SettingsService;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-27T16:35:28.866+03:00")

@Controller
public class SettingsApiController implements SettingsApi {

    private SettingsService settingsService;

    @Autowired
    public SettingsApiController(SettingsService settingsService) {
        this.settingsService = settingsService;
    }

    @PreAuthorize("hasAuthority('hungry')")
    @Override
    public ResponseEntity<AccountSettings> settingsGet() throws ApiException {

        return new ResponseEntity<AccountSettings>(settingsService.settingsGet(), HttpStatus.OK);
    }

    @PreAuthorize("hasAuthority('hungry')")
    public ResponseEntity<Void> settingsPictureDelete() throws ApiException {

        settingsService.removeUserPicture();
        return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
    }

    @PreAuthorize("hasAuthority('hungry')")
    public ResponseEntity<Void> settingsPicturePost(@ApiParam(value = "file detail") @RequestPart("file") MultipartFile newpicture) throws ApiException {

        if (newpicture.isEmpty()) {
            throw new ApiException(400, "Bad Request");
        }
        settingsService.settingsPicturePost(newpicture);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Override
    public ResponseEntity<Object> settingsPictureGet(@NotNull @ApiParam(value = "The token of the current session.", required = true) @RequestParam(value = "token", required = true) String token) {

        try {
            if (token.isEmpty()) {
                throw new ApiException(400, "Bad request");
            }
            // get user picture as bytes array
            byte[] userPicBytes = settingsService.settingsPictureTokenGet(token);
            ByteArrayInputStream inputStream = new ByteArrayInputStream(userPicBytes);
            return ResponseEntity
                    .ok()
                    .contentLength(userPicBytes.length)
                    .contentType(MediaType.parseMediaType("image/jpeg"))
                    .body(new InputStreamResource(inputStream));
        } catch (ApiException ex) {
            return new ResponseEntity(HttpStatus.valueOf(ex.getCode()));
        }
    }

    @PreAuthorize("hasAuthority('hungry')")
    public ResponseEntity<Void> settingsPut(@ApiParam(value = "The updated fields of the user's account settings.", required = true) @Valid @RequestBody AccountDetails accountDetails, Errors errors) throws ApiException {

        if (errors.hasErrors()) {
            System.out.println(errors.getAllErrors());
            //we must allow to pass an empty password
            if (accountDetails.getPassword().isEmpty() 
                    && !accountDetails.getEmail().isEmpty() 
                    && !accountDetails.getFirstName().isEmpty()
                    && !accountDetails.getLastName().isEmpty()
                    && accountDetails.getVersion() >= 0 ) {              
            }else{
                throw new ApiException(errors.getFieldError().getField(), 400);
            }
        }
        try {
            settingsService.settingsPut(accountDetails);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (OptimisticLockException ex) {
            try {
                AccountSettings accountSettings = settingsService.settingsGet();
                throw new ConcurrentModificationException(accountSettings);
            } catch (ApiException ex1) {
                Logger.getLogger(SettingsApiController.class.getName()).log(Level.SEVERE, null, ex1);
                throw new ApiException(500, "Concurrent modification exception: internal error");
            }
        }
    }

}
