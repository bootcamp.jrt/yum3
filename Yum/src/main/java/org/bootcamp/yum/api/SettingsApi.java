/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.api;

import java.io.File;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import io.swagger.annotations.*;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.Errors;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestPart;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import org.bootcamp.yum.api.model.AccountDetails;
import org.bootcamp.yum.api.model.AccountSettings;
import org.bootcamp.yum.exception.ApiException;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-27T16:35:28.866+03:00")

@Api(value = "settings", description = "the settings API")
@RequestMapping(value = "/api")
public interface SettingsApi {

    @CrossOrigin
    @ApiOperation(value = "Gets user's account settings", notes = "Returns user's account settings", response = AccountSettings.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"hungry",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Returns user's account settings", response = AccountSettings.class)})
    @RequestMapping(value = "/settings",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<AccountSettings> settingsGet() throws ApiException;

    @CrossOrigin
    @ApiOperation(value = "Deletes the user's picture.", notes = "Deletes the user's picture.", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"hungry",})
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "Picture deleted.", response = Void.class)
        ,
        @ApiResponse(code = 400, message = "Bad request.", response = Void.class)})
    @RequestMapping(value = "/settings/picture",
            produces = {"application/json"},
            method = RequestMethod.DELETE)
    ResponseEntity<Void> settingsPictureDelete() throws ApiException;

    @CrossOrigin
    @ApiOperation(value = "Uploads a new picture", notes = "Allow users to upload a new picture", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"hungry",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "The new picture.", response = Void.class)
        ,
        @ApiResponse(code = 400, message = "Bad Request.", response = Void.class)})
    @RequestMapping(value = "/settings/picture",
            produces = {"application/json"},
            consumes = {"multipart/form-data"},
            method = RequestMethod.POST)
    ResponseEntity<Void> settingsPicturePost(@ApiParam(value = "file detail") @RequestPart("file") MultipartFile file) throws ApiException;

    @CrossOrigin
    @ApiOperation(value = "Gets user's picture", notes = "Returns user's picture", response = File.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"hungry",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Returns user's picture", response = File.class)
        ,
        @ApiResponse(code = 400, message = "Bad Request", response = File.class)
        ,
        @ApiResponse(code = 404, message = "image not found", response = File.class)})
    @RequestMapping(value = "/settings/picture/token",
            produces = {"image/jpeg"},
            method = RequestMethod.GET)
    ResponseEntity<Object> settingsPictureGet(@NotNull @ApiParam(value = "The token of the current session.", required = true) @RequestParam(value = "token", required = true) String token);

    @CrossOrigin
    @ApiOperation(value = "Updates the user's account settings.", notes = "Updates the modified fields for the user's account settings.", response = Void.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"hungry",})
    @ApiResponses(value = {
        @ApiResponse(code = 204, message = "Account settings updated", response = Void.class)
        ,
        @ApiResponse(code = 400, message = "Bad Request", response = Void.class)
        ,
        @ApiResponse(code = 409, message = "Concurrent modification error", response = Void.class)})
    @RequestMapping(value = "/settings",
            produces = {"application/json"},
            method = RequestMethod.PUT)
    ResponseEntity<Void> settingsPut(@ApiParam(value = "The updated fields of the user's account settings.", required = true) @Valid @RequestBody AccountDetails accountDetails, Errors errors) throws ApiException;

}
