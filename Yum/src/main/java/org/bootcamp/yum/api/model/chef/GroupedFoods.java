/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.api.model.chef;

import java.util.List;
import java.util.Objects;
import java.util.ArrayList;

import com.fasterxml.jackson.annotation.JsonProperty;

import io.swagger.annotations.ApiModelProperty;

import org.bootcamp.yum.api.model.OrderItem;

/**
 * GroupedFoods
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-05-02T17:54:48.065+03:00")

public class GroupedFoods {

    @JsonProperty("foodType")
    private String foodType = null;

    @JsonProperty("orderItems")
    private List<OrderItem> orderItems = new ArrayList<OrderItem>();

    public GroupedFoods foodType(String foodType) {
        this.foodType = foodType;
        return this;
    }

    public GroupedFoods() {
    }

    public GroupedFoods(String foodType) {
        this.foodType = foodType;
    }

    /**
     * Get foodType
     *
     * @return foodType
     *
     */
    @ApiModelProperty(value = "")
    public String getFoodType() {
        return foodType;
    }

    public void setFoodType(String foodType) {
        this.foodType = foodType;
    }

    public GroupedFoods orderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
        return this;
    }

    public GroupedFoods addOrderItemsItem(OrderItem orderItemsItem) {
        this.orderItems.add(orderItemsItem);
        return this;
    }

    /**
     * Get orderItems
     *
     * @return orderItems
     *
     */
    @ApiModelProperty(value = "")
    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GroupedFoods groupedFoods = (GroupedFoods) o;
        return Objects.equals(this.foodType, groupedFoods.foodType)
                && Objects.equals(this.orderItems, groupedFoods.orderItems);
    }

    @Override
    public int hashCode() {
        return Objects.hash(foodType, orderItems);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class GroupedFoods {\n");

        sb.append("    foodType: ").append(toIndentedString(foodType)).append("\n");
        sb.append("    orderItems: ").append(toIndentedString(orderItems)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
