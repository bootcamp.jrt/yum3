/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.api;

import javax.validation.Valid;

import io.swagger.annotations.*;

import org.springframework.validation.Errors;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;

import org.bootcamp.yum.api.model.chef.DailyMenuChef;
import org.bootcamp.yum.api.model.chef.DailyMenusChef;
import org.bootcamp.yum.api.model.chef.FoodList;
import org.bootcamp.yum.api.model.chef.UpdateDailyMenu;
import org.bootcamp.yum.exception.ApiException;

@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-27T16:35:28.866+03:00")

@Api(value = "dailyMenus", description = "the dailyMenus API")
@RequestMapping(value = "/api")
public interface DailyMenusApi {

    @CrossOrigin
    @ApiOperation(value = "update the daily menu with the given id", notes = "updates the foods of a daily menu with the specified id.", response = DailyMenuChef.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"chef",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Daily menu food list updated.", response = DailyMenuChef.class)
        ,
        @ApiResponse(code = 204, message = "DailyMenu received without foods and deleted.", response = DailyMenuChef.class)
        ,
        @ApiResponse(code = 400, message = "Bad request.", response = DailyMenuChef.class)
        ,
        @ApiResponse(code = 409, message = "Concurrent modification error.", response = DailyMenuChef.class)
        ,
        @ApiResponse(code = 410, message = "Concurrent deletion error.", response = DailyMenuChef.class)
        ,
        @ApiResponse(code = 412, message = "Daily menu is final.", response = DailyMenuChef.class)})
    @RequestMapping(value = "/dailyMenus/{id}",
            produces = {"application/json"},
            method = RequestMethod.PUT)
    ResponseEntity<DailyMenuChef> dailyMenusIdPut(@ApiParam(value = "The daily menu's unique id.", required = true) @PathVariable("id") Long id,
            @ApiParam(value = "The updated list of foods.", required = true) @Valid @RequestBody UpdateDailyMenu updateDailyMenu, Errors errors) throws ApiException;

    @CrossOrigin
    @ApiOperation(value = "Gets the current month's concerned weeks dailyMenus and order history.", notes = "Returns a list with current month's concerned weeks dailyMenus and orders.", response = DailyMenusChef.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"chef",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Current month's concerned weeks list of dailyMenus and orders.", response = DailyMenusChef.class)})
    @RequestMapping(value = "/dailyMenus/monthly",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<DailyMenusChef> dailyMenusMonthlyGet() throws ApiException;

    @CrossOrigin
    @ApiOperation(value = "Gets requested month's concerned weeks dailyMenus and order history.", notes = "Returns a list with the requested month's concerned weeks dailyMenus and orders.", response = DailyMenusChef.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"chef",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Requested month's concerned weeks list of dailyMenus and orders.", response = DailyMenusChef.class)
        ,
        @ApiResponse(code = 400, message = "Requested month-Year doesn't exist.", response = DailyMenusChef.class)})
    @RequestMapping(value = "/dailyMenus/monthly/{monthYear}",
            produces = {"application/json"},
            method = RequestMethod.GET)
    ResponseEntity<DailyMenusChef> dailyMenusMonthlyMonthYearGet(@ApiParam(value = "The requested month-Year in format MM-YYYY.", required = true) @PathVariable("monthYear") String monthYear) throws ApiException;

    @CrossOrigin
    @ApiOperation(value = "Creates a new daily menu.", notes = "Adds a new daily menu with the given characteristics.", response = DailyMenuChef.class, authorizations = {
        @Authorization(value = "Bearer")
    }, tags = {"chef",})
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "Daily menu created successfully.", response = DailyMenuChef.class)
        ,
        @ApiResponse(code = 400, message = "Daily menu could not be created.", response = DailyMenuChef.class)
        ,
        @ApiResponse(code = 409, message = "Concurrent creation error.", response = DailyMenuChef.class)
        ,
        @ApiResponse(code = 412, message = "Deadline for creation has reached.", response = DailyMenuChef.class)})
    @RequestMapping(value = "/dailyMenus",
            produces = {"application/json"},
            method = RequestMethod.POST)
    ResponseEntity<DailyMenuChef> dailyMenusPost(@ApiParam(value = "The list of foods.") @Valid @RequestBody FoodList foodList, Errors errors) throws ApiException;

}
