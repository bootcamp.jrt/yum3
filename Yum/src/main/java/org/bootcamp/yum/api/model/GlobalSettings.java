/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.api.model;

import java.util.Objects;

import javax.validation.constraints.*;

import org.hibernate.validator.constraints.NotEmpty;

import io.swagger.annotations.ApiModelProperty;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.bootcamp.validator.CheckCurrency;
import org.bootcamp.validator.CheckTimeFormat;

/**
 * GlobalSettings
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-27T16:35:28.866+03:00")

public class GlobalSettings {

    @CheckTimeFormat
    @NotEmpty
    @JsonProperty("deadline")
    private String deadline = null;

    @NotEmpty
    @CheckCurrency(message = "Order items cannot contain dublicate food ids")
    @JsonProperty("currency")
    private String currency = null;

    @JsonProperty("notes")
    private String notes = null;

    @NotEmpty
    @JsonProperty("terms")
    private String terms = null;

    @NotEmpty
    @JsonProperty("privacy")
    private String privacy = null;

    @NotNull
    @Min(1)
    @JsonProperty("version")
    private Integer version = null;

    public GlobalSettings() {
    }

    public GlobalSettings(String deadline, String currency, String notes, String terms, String privacy, int version) {
        this.deadline = deadline;
        this.currency = currency;
        this.notes = notes;
        this.terms = terms;
        this.privacy = privacy;
        this.version = version;
    }

    public GlobalSettings deadline(String deadline) {
        this.deadline = deadline;
        return this;
    }

    /**
     * format HH:MM
     *
     * @return deadline
     *
     */
    @ApiModelProperty(value = "format HH:MM")
    public String getDeadline() {
        return deadline;
    }

    public void setDeadline(String deadline) {
        this.deadline = deadline;
    }

    public GlobalSettings currency(String currency) {
        this.currency = currency;
        return this;
    }

    /**
     * Get currency
     *
     * @return currency
     *
     */
    @ApiModelProperty(value = "")
    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public GlobalSettings notes(String notes) {
        this.notes = notes;
        return this;
    }

    /**
     * Get notes
     *
     * @return notes
     *
     */
    @ApiModelProperty(value = "")
    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public GlobalSettings terms(String terms) {
        this.terms = terms;
        return this;
    }

    /**
     * Get terms
     *
     * @return terms
     *
     */
    @ApiModelProperty(value = "")
    public String getTerms() {
        return terms;
    }

    public void setTerms(String terms) {
        this.terms = terms;
    }

    public GlobalSettings privacy(String privacy) {
        this.privacy = privacy;
        return this;
    }

    /**
     * Get privacy
     *
     * @return privacy
     *
     */
    @ApiModelProperty(value = "")
    public String getPrivacy() {
        return privacy;
    }

    public void setPrivacy(String privacy) {
        this.privacy = privacy;
    }

    public GlobalSettings version(Integer version) {
        this.version = version;
        return this;
    }

    /**
     * Get version
     *
     * @return version
     *
     */
    @ApiModelProperty(value = "")
    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        GlobalSettings globalSettings = (GlobalSettings) o;
        return Objects.equals(this.deadline, globalSettings.deadline)
                && Objects.equals(this.currency, globalSettings.currency)
                && Objects.equals(this.notes, globalSettings.notes)
                && Objects.equals(this.terms, globalSettings.terms)
                && Objects.equals(this.privacy, globalSettings.privacy)
                && Objects.equals(this.version, globalSettings.version);
    }

    @Override
    public int hashCode() {
        return Objects.hash(deadline, currency, notes, terms, privacy, version);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class GlobalSettings {\n");

        sb.append("    deadline: ").append(toIndentedString(deadline)).append("\n");
        sb.append("    currency: ").append(toIndentedString(currency)).append("\n");
        sb.append("    notes: ").append(toIndentedString(notes)).append("\n");
        sb.append("    terms: ").append(toIndentedString(terms)).append("\n");
        sb.append("    privacy: ").append(toIndentedString(privacy)).append("\n");
        sb.append("    version: ").append(toIndentedString(version)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
