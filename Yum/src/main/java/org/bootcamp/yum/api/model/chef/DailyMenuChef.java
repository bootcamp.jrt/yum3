/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.api.model.chef;

import java.util.Objects;
import java.util.ArrayList;
import java.util.List;

import org.joda.time.LocalDate;

import io.swagger.annotations.ApiModelProperty;

import com.fasterxml.jackson.annotation.JsonProperty;

import org.bootcamp.yum.api.model.OrderItem;

/**
 * DailyMenuChef
 */
@javax.annotation.Generated(value = "io.swagger.codegen.languages.SpringCodegen", date = "2017-04-27T16:35:28.866+03:00")

public class DailyMenuChef {

    @JsonProperty("id")
    private Long id = null;

    @JsonProperty("date")
    private LocalDate date = null;

    @JsonProperty("version")
    private Integer version = null;

    @JsonProperty("final")
    private Boolean _final = null;

    @JsonProperty("foods")
    private List<OrderItem> foods = new ArrayList<OrderItem>();

    public DailyMenuChef id(Long id) {
        this.id = id;
        return this;
    }

    /**
     * Get id
     *
     * @return id
     *
     */
    @ApiModelProperty(value = "")
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public DailyMenuChef date(LocalDate date) {
        this.date = date;
        return this;
    }

    /**
     * Get date
     *
     * @return date
     *
     */
    @ApiModelProperty(value = "")
    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public DailyMenuChef version(Integer version) {
        this.version = version;
        return this;
    }

    /**
     * Get version
     *
     * @return version
     *
     */
    @ApiModelProperty(value = "")
    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public DailyMenuChef _final(Boolean _final) {
        this._final = _final;
        return this;
    }

    /**
     * Get _final
     *
     * @return _final
     *
     */
    @ApiModelProperty(value = "")
    public Boolean getFinal() {
        return _final;
    }

    public void setFinal(Boolean _final) {
        this._final = _final;
    }

    public DailyMenuChef foods(List<OrderItem> foods) {
        this.foods = foods;
        return this;
    }

    public DailyMenuChef addFoodsItem(OrderItem foodsItem) {
        this.foods.add(foodsItem);
        return this;
    }

    /**
     * Get foods
     *
     * @return foods
     *
     */
    @ApiModelProperty(value = "")
    public List<OrderItem> getFoods() {
        return foods;
    }

    public void setFoods(List<OrderItem> foods) {
        this.foods = foods;
    }

    @Override
    public boolean equals(java.lang.Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        DailyMenuChef dailyMenuChef = (DailyMenuChef) o;
        return Objects.equals(this.id, dailyMenuChef.id)
                && Objects.equals(this.date, dailyMenuChef.date)
                && Objects.equals(this.version, dailyMenuChef.version)
                && Objects.equals(this._final, dailyMenuChef._final)
                && Objects.equals(this.foods, dailyMenuChef.foods);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, date, version, _final, foods);
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class DailyMenuChef {\n");

        sb.append("    id: ").append(toIndentedString(id)).append("\n");
        sb.append("    date: ").append(toIndentedString(date)).append("\n");
        sb.append("    version: ").append(toIndentedString(version)).append("\n");
        sb.append("    _final: ").append(toIndentedString(_final)).append("\n");
        sb.append("    foods: ").append(toIndentedString(foods)).append("\n");
        sb.append("}");
        return sb.toString();
    }

    /**
     * Convert the given object to string with each line indented by 4 spaces
     * (except the first line).
     */
    private String toIndentedString(java.lang.Object o) {
        if (o == null) {
            return "null";
        }
        return o.toString().replace("\n", "\n    ");
    }
}
