/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.converter;

import javax.persistence.Converter;
import javax.persistence.AttributeConverter;

import org.bootcamp.enums.FoodEnum;

@Converter(autoApply = true)
public class FoodConverter implements AttributeConverter<FoodEnum, String> {

    @Override
    public String convertToDatabaseColumn(FoodEnum food) {

        switch (food) {
            case MAIN:
                return "main dish";
            case SALAD:
                return "salad";
            case DRINK:
                return "soft drink";
        }
        throw new UnsupportedOperationException("Option " + food + "is not supported! Please use: MAIN/SALAD/DRINK/DESSERT");
    }

    @Override
    public FoodEnum convertToEntityAttribute(String foodString) {

        switch (foodString) {
            case ("main dish"):
                return FoodEnum.MAIN;
            case ("salad"):
                return FoodEnum.SALAD;
            case ("soft drink"):
                return FoodEnum.DRINK;
        }

        throw new UnsupportedOperationException("Option " + foodString + "is not supported! Please use: main dish/salad/drink/dessert ");
    }

}
