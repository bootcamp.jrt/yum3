/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.converter;

import javax.persistence.Converter;
import javax.persistence.AttributeConverter;

import org.bootcamp.enums.UserRoleEnum;

@Converter(autoApply = true)
public class UserRoleConverter implements AttributeConverter<UserRoleEnum, String> {

    @Override
    public String convertToDatabaseColumn(UserRoleEnum role) {
        switch (role) {
            case HUNGRY:
                return "hungry";
            case CHEF:
                return "chef";
            case ADMIN:
                return "admin";
        }
        throw new UnsupportedOperationException("Value " + role + " is not supported. Please use: HUNGRY/CHEF/ADMIN");
    }

    @Override
    public UserRoleEnum convertToEntityAttribute(String roleString) {

        switch (roleString) {

            case ("hungry"):
                return UserRoleEnum.HUNGRY;
            case ("chef"):
                return UserRoleEnum.CHEF;
            case ("admin"):
                return UserRoleEnum.ADMIN;
        }

        throw new UnsupportedOperationException("Value " + roleString + " is not supported!Please use: hungry/chef/admin");
    }

}
