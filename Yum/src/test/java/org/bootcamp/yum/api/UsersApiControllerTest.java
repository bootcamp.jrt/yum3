/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.api;

import java.math.BigDecimal;
import java.util.List;
import java.util.ArrayList;
import java.time.format.DateTimeFormatter;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;

import org.junit.Test;
import org.junit.Rule;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import org.mockito.Mock;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.hasSize;

import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;

import org.bootcamp.enums.FoodEnum;
import org.bootcamp.enums.UserRoleEnum;
import org.bootcamp.test.annotation.WithMockAuth;
import org.bootcamp.yum.api.model.admin.Users;
import org.bootcamp.yum.data.entity.User;
import org.bootcamp.yum.service.UserService;
import org.bootcamp.yum.api.model.AccountSettings;
import org.bootcamp.yum.data.entity.DailyMenu;
import org.bootcamp.yum.data.entity.DailyOrder;
import org.bootcamp.yum.data.entity.Food;
import org.bootcamp.yum.data.entity.OrderItem;
import org.bootcamp.yum.data.repository.UserRepository;
import org.bootcamp.yum.service.YumService;
import org.bootcamp.yum.exception.ExceptionControllerAdvice;
import org.bootcamp.yum.service.SendMailService;
import org.bootcamp.test.MockAuthRule;

/**
 *
 * @author user
 */
@RunWith(MockitoJUnitRunner.class)
public class UsersApiControllerTest {

    @Rule
    public final MockAuthRule mockAuth = new MockAuthRule();

    @InjectMocks
    private final UserService userService = new UserService();

    private MockMvc mockMvc;
    private static User user;
    private static AccountSettings accountSettings;
    private static List<User> usersList;
    private static List<DailyMenu> menusList;
    private static Users users = new Users();

    @Mock
    private UserRepository userRepo;
    
    @Mock
    private SendMailService sendMailService;

    @Mock
    private final YumService yumService = new YumService();

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(new UsersApiController(userService))
                .setControllerAdvice(new ExceptionControllerAdvice())
                .build();
    }

    @BeforeClass
    public static void initMockData() {
        //Create foods
        Food food1 = new Food(1);
        food1.setDescription("lalala");
        food1.setName("fakes");
        food1.setPrice(BigDecimal.valueOf(1000));
        food1.setType(FoodEnum.MAIN);
        Food food2 = new Food(2);
        food2.setDescription("lalala");
        food2.setName("arakas");
        food2.setPrice(BigDecimal.valueOf(1000));
        food2.setType(FoodEnum.MAIN);

        //Create menus
        DailyMenu menu1 = new DailyMenu(1);
        menu1.setOfDate(LocalDate.parse("2017-05-05"));
        menu1.addFood(food1);
        DailyMenu menu2 = new DailyMenu(2);
        menu2.setOfDate(LocalDate.parse("2017-06-10"));
        menu2.addFood(food2);
        menusList = new ArrayList<>();
        menusList.add(menu1);
        menusList.add(menu2);

        usersList = new ArrayList<>();
        user = new User(1);
        user.setEmail("email@email.com");
        user.setFirstName("User");
        user.setLastName("nobody");
        user.setRole(UserRoleEnum.HUNGRY);
        user.setPassword("123456789");
        user.setRegistrationDate(LocalDate.parse("2017-05-03"));
        user.setPicture(null);
        user.setApproved(true);
        user.setSecret("some secrete");

        String str = "1986-04-08T12:30";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime = LocalDateTime.parse(str);

        user.setSecretCreation(dateTime);
        user.setVersion(1);

        User user1 = new User(2);
        user1.setEmail("emailuser@email.com");
        user1.setFirstName("UserAnother");
        user1.setLastName("nobody");
        user1.setRole(UserRoleEnum.ADMIN);
        user1.setPassword("123456789");
        user1.setRegistrationDate(LocalDate.parse("2017-05-05"));
        user1.setPicture(null);
        user1.setApproved(true);
        user1.setSecret("some secret and maybe more");

        User user2 = new User(3);
        user2.setEmail("emailusedededr@email.com");
        user2.setFirstName("UserAnotherOther");
        user2.setLastName("nobody");
        user2.setRole(UserRoleEnum.ADMIN);
        user2.setPassword("123456789");
        user2.setRegistrationDate(LocalDate.parse("2017-05-05"));
        user2.setPicture(null);
        user2.setApproved(true);
        user2.setSecret("some secret and maybe more");

        String str1 = "1987-07-07T15:50";
        DateTimeFormatter formatter1 = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        LocalDateTime dateTime1 = LocalDateTime.parse(str1);

        user1.setSecretCreation(dateTime1);
        user1.setVersion(1);
        user2.setSecretCreation(dateTime1);
        user2.setVersion(1);

        usersList.add(user);
        usersList.add(user1);
        usersList.add(user2);

        accountSettings = new AccountSettings("User1", "nobody", "email@email.com", "HUNGRY", false, true, 1);
        //Create orders
        DailyOrder order1 = new DailyOrder();
        order1.setId(1);
        order1.setDailyMenu(menu1);
        order1.setFinalOrder(false);
        order1.setVersion(1);
        order1.setUser(user1);
        user1.addDailyOrder(order1);
        order1.addOrderItem(new OrderItem(food1, 2));

        DailyOrder order2 = new DailyOrder();
        order2.setId(2);
        order2.setDailyMenu(menu2);
        order2.setFinalOrder(false);
        order2.setVersion(1);
        order2.setUser(user2);
        user2.addDailyOrder(order2);
        order2.addOrderItem(new OrderItem(food2, 2));
    }

    /**
     * In that case, admin gets all the users successfully.
     */
    @Test
    public void testUsersGet() throws Exception {
        given(userRepo.findAll()).willReturn(usersList);
        mockMvc.perform(get("/api/users")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.users", hasSize(3)))
                .andExpect(jsonPath("$.totalPagesNumber", is(0)))
                .andExpect(jsonPath("$.users[0].id", is(1)))
                .andExpect(jsonPath("$.users[0].firstName", is("User")))
                .andExpect(jsonPath("$.users[1].id", is(2)))
                .andExpect(jsonPath("$.users[1].firstName", is("UserAnother")));

        // we verify that we called findAll method only once on the repo.
        verify(userRepo, times(1)).findAll();
        // we verify that we didnt call anything else on the repo
        verifyNoMoreInteractions(userRepo);
    }

    /**
     * Test of usersIdApprovePut method, of class UsersApiController.
     */
    @Test
    @WithMockAuth(id = "1")
    public void testUsersIdApprovePutOK() throws Exception {
        given(userRepo.findById(1L)).willReturn(usersList.get(0));
        given(userRepo.findById(2L)).willReturn(usersList.get(1));
        given(yumService.getDeadline()).willReturn(org.joda.time.LocalTime.MIDNIGHT.minusHours(12));
        mockMvc.perform(put("/api/users/{id}/approve?approved=false", 2))
                .andExpect(status().isNoContent())
                .andExpect(status().is(204));
    }

    @Test
    @WithMockAuth(id = "1")
    public void testUsersIdApprovePut409() throws Exception {
        given(userRepo.findById(1L)).willReturn(usersList.get(0));
        given(userRepo.findById(3L)).willReturn(usersList.get(2));
        given(yumService.getDeadline()).willReturn(org.joda.time.LocalTime.MIDNIGHT.minusHours(12));
        mockMvc.perform(put("/api/users/{id}/approve?approved=false", 3))
                .andExpect(status().is(409));

    }

    /**
     * Test of usersIdDelete method, of class UsersApiController.
     */
    @Test
    @WithMockAuth(id = "1")
    public void testUsersIdDelete409() throws Exception {
        given(userRepo.findById(1L)).willReturn(usersList.get(0));
        given(userRepo.findById(3L)).willReturn(usersList.get(2));
        given(yumService.getDeadline()).willReturn(org.joda.time.LocalTime.MIDNIGHT.minusHours(12));
        mockMvc.perform(delete("/api/users/{id}", 3))
                .andExpect(status().is(409));

    }

    @Test
    @WithMockAuth(id = "1")
    public void testUsersIdDelete412() throws Exception {
        given(userRepo.findById(1L)).willReturn(usersList.get(0));
        given(userRepo.findById(2L)).willReturn(usersList.get(1));
        given(yumService.getDeadline()).willReturn(org.joda.time.LocalTime.MIDNIGHT.minusHours(12));
        mockMvc.perform(delete("/api/users/{id}", 2))
                .andExpect(status().is(412));

    }

    /**
     * Test of usersIdForgotpwdPost method, of UsersApiController. Case reset
     * password secret created.
     */
    @Test
    public void testUsersIdForgotpwdPost() throws Exception {

        given(userRepo.findById(1)).willReturn(user);

        mockMvc.perform(post("/api/users/{id}/forgotpwd", 1))
                .andExpect(status().isNoContent());

        verify(userRepo, times(1)).findById(1);
        verifyNoMoreInteractions(userRepo);
    }

    /**
     * Test of usersIdForgotpwdPost method, of UsersApiController.Case returned
     * 404, reason user not found.
     */
    @Test
    public void testUsersIdForgotpwdPost_userNotFound() throws Exception {

        given(userRepo.findById(2)).willReturn(null);

        mockMvc.perform(post("/api/users/{id}/forgotpwd", 2))
                .andExpect(status().isNotFound());

        verify(userRepo, times(1)).findById(2);
        verifyNoMoreInteractions(userRepo);
    }

    /**
     * In that case the admin takes the account settings of a specific user
     */
    @Test
    public void testUsersIdGet() throws Exception {

        // telling Mockito to use this mock every time the findById method is called on the user repository.
        given(userRepo.findById(1L)).willReturn(user);
        mockMvc.perform(get("/api/users/1")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.firstName", is("User")))
                .andExpect(jsonPath("$.lastName", is("nobody")))
                .andExpect(jsonPath("$.role", is("HUNGRY")));

        // we verify that we called findById method only once on the repo.
        verify(userRepo, times(1)).findById(1L);
        // we verify that we didnt call anything else on the repo
        verifyNoMoreInteractions(userRepo);
    }

    /**
     * In that case the user is not found
     */
    @Test
    public void testUsersIdGet_notFound() throws Exception {
        // telling Mockito to use this mock every time the findById method is called on the user repository.
        given(userRepo.findById(10L)).willReturn(null);
        mockMvc.perform(get("/api/users/1")).andExpect(status().isNotFound());
        // we verify that we called findById method only once on the repo.
        verify(userRepo, times(1)).findById(1L);
        // we verify that we didnt call anything else on the repo
        verifyNoMoreInteractions(userRepo);
    }

    /**
     * In that case, the Admin updates successfully the user's account settings
     */
    @Test
    public void testUsersIdPut() throws Exception {

        // telling Mockito to use this mock
        given(userRepo.findById(1L)).willReturn(user);
        given(userRepo.findByEmail(any(String.class))).willReturn(user);

        mockMvc.perform(put("/api/users/{id}", 1L)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"firstName\": \"user\",\n"
                        + "  \"lastName\": \"lastname\",\n"
                        + "  \"email\": \"email@email.com\",\n"
                        + "  \"role\": \"HUNGRY\",\n"
                        + "  \"version\": 1 \n"
                        + "}"))
                .andExpect(status().isNoContent());

        verify(userRepo, times(1)).findById(1L);
        verify(userRepo, times(1)).findByEmail(any(String.class));
        // we verify that we didnt call anything else on the repo
        verifyNoMoreInteractions(userRepo);
    }

    /**
     * In that case, the required user is not found
     */
    @Test
    public void testUsersIdPut_notFound() throws Exception {

        // telling Mockito to use this mock
        given(userRepo.findById(10L)).willReturn(null);

        mockMvc.perform(put("/api/users/{id}", 1L)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"firstName\": \"user\",\n"
                        + "  \"lastName\": \"lastname\",\n"
                        + "  \"email\": \"email@email.com\",\n"
                        + "  \"role\": \"HUNGRY\",\n"
                        + "  \"version\": 1 \n"
                        + "}"))
                .andExpect(status().isNotFound());

        verify(userRepo, times(1)).findById(1L);
        // we verify that we didnt call anything else on the repo
        verifyNoMoreInteractions(userRepo);
    }

    /**
     * In that case, an body value is empty
     */
    @Test
    public void testUsersIdPut_empty() throws Exception {

        // telling Mockito to use this mock
        given(userRepo.findById(1L)).willReturn(user);

        mockMvc.perform(put("/api/users/{id}", 1L)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"firstName\": \"\",\n"
                        + "  \"lastName\": \"lastname\",\n"
                        + "  \"email\": \"email@email.com\",\n"
                        + "  \"role\": \"HUNGRY\",\n"
                        + "  \"version\": 1 \n"
                        + "}"))
                .andExpect(status().isBadRequest());

        verify(userRepo, times(0)).findById(1L);
        // we verify that we didnt call anything else on the repo
        verifyNoMoreInteractions(userRepo);
    }

    /**
     * In that case, invalid values are inserted at Email (required email
     * format: email@host.com) or/and the role is invalid
     */
    @Test
    public void testUsersIdPut_invalidValues() throws Exception {

        // telling Mockito to use this mock
        given(userRepo.findById(1L)).willReturn(user);

        mockMvc.perform(put("/api/users/{id}", 1L)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"firstName\": \"user1\",\n"
                        + "  \"lastName\": \"lastname1\",\n"
                        + "  \"email\": \"wrong email form\",\n"
                        + "  \"role\": \"wrong role\",\n"
                        + "  \"version\": 1 \n"
                        + "}"))
                .andExpect(status().isBadRequest());

        verify(userRepo, times(0)).findById(1L);
        // we verify that we didnt call anything else on the repo
        verifyNoMoreInteractions(userRepo);
    }

    /**
     * In that case, the user has different version than the database has.
     */
    @Test
    public void testUsersIdPut_concurrent() throws Exception {

        // telling Mockito to use this mock
        given(userRepo.findById(1L)).willReturn(user);

        mockMvc.perform(put("/api/users/{id}", 1L)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"firstName\": \"someone\",\n"
                        + "  \"lastName\": \"lastname\",\n"
                        + "  \"email\": \"email@email.com\",\n"
                        + "  \"role\": \"HUNGRY\",\n"
                        + "  \"version\": 5 \n"
                        + "}"))
                .andExpect(status().isConflict());

        verify(userRepo, times(1)).findById(1L);
        // we verify that we didnt call anything else on the repo
        verifyNoMoreInteractions(userRepo);
    }

    /**
     * In that case, the admin creates successfully a new user
     */
    @Test
    public void testUsersPost() throws Exception {

        // check for the email to be unique
        given(userRepo.findByEmail("newemail@email.com")).willReturn(null);
        given(userRepo.save(any(User.class))).willReturn(new User(1));

        mockMvc.perform(post("/api/users")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"firstName\": \"userA\",\n"
                        + "  \"lastName\": \"lastnamee\",\n"
                        + "  \"email\": \"newemail@email.com\",\n"
                        + "  \"password\": \"123456\",\n"
                        + "  \"role\": \"HUNGRY\"\n"
                        + "}"))
                .andExpect(status().isNoContent());

        verify(userRepo, times(1)).findByEmail("newemail@email.com");
        verify(userRepo, times(1)).save(any(User.class));
        verifyNoMoreInteractions(userRepo);
    }

    /**
     * In that case, another user with the same email already exists. The email
     * must be unique
     */
    @Test
    public void testUsersPost_emailExists() throws Exception {

        // check for the email to be unique
        given(userRepo.findByEmail("email@email.com")).willReturn(user);
        given(userRepo.save(any(User.class))).willReturn(new User(1));

        mockMvc.perform(post("/api/users")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"firstName\": \"userA\",\n"
                        + "  \"lastName\": \"lastnameA\",\n"
                        + "  \"email\": \"email@email.com\",\n"
                        + "  \"password\": \"123456\",\n"
                        + "  \"role\": \"HUNGRY\"\n"
                        + "}"))
                .andExpect(status().isPreconditionFailed());

        verify(userRepo, times(1)).findByEmail("email@email.com");
        verify(userRepo, times(0)).save(any(User.class));
        verifyNoMoreInteractions(userRepo);
    }

    /**
     * In that case, one or more values are empty
     */
    @Test
    public void testUsersPost_badRequest_emptyValues() throws Exception {

        // check for the email to be unique
        given(userRepo.findByEmail("newemail@email.com")).willReturn(null);
        given(userRepo.save(any(User.class))).willReturn(new User(1));

        mockMvc.perform(post("/api/users")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"firstName\": \"\",\n"
                        + "  \"lastName\": \"\",\n"
                        + "  \"email\": \"email@email.com\",\n"
                        + "  \"password\": \"123456\",\n"
                        + "  \"role\": \"HUNGRY\"\n"
                        + "}"))
                .andExpect(status().isBadRequest());

        verify(userRepo, times(0)).findByEmail("newemail@email.com");
        verify(userRepo, times(0)).save(any(User.class));
        verifyNoMoreInteractions(userRepo);
    }

    /**
     * In that case, the email is wrong Email format: something@somewhere.com
     */
    @Test
    public void testUsersPost_badRequest_invalidEmail() throws Exception {

        // check for the email to be unique
        given(userRepo.findByEmail("newemail.email.com")).willReturn(null);
        given(userRepo.save(any(User.class))).willReturn(new User(1));

        mockMvc.perform(post("/api/users")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"firstName\": \"user1\",\n"
                        + "  \"lastName\": \"lastname1\",\n"
                        + "  \"email\": \"newemail.email.com\",\n"
                        + "  \"password\": \"123456\",\n"
                        + "  \"role\": \"HUNGRY\"\n"
                        + "}"))
                .andExpect(status().isBadRequest());

        verify(userRepo, times(0)).findByEmail("newemail.email.com");
        verify(userRepo, times(0)).save(any(User.class));
        verifyNoMoreInteractions(userRepo);
    }

    /**
     * In that case, the role value is not supported. Role enums :
     * HUNGRY/CHEF/ADMIN
     */
    @Test
    public void testUsersPost_badRequest_invalidRole() throws Exception {

        // check for the email to be unique
        given(userRepo.findByEmail("newemail@email.com")).willReturn(null);
        given(userRepo.save(any(User.class))).willReturn(new User(1));

        mockMvc.perform(post("/api/users")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"firstName\": \"user1\",\n"
                        + "  \"lastName\": \"lastname1\",\n"
                        + "  \"email\": \"newemail@email@email.com\",\n"
                        + "  \"password\": \"123456\",\n"
                        + "  \"role\": \"another role\"\n"
                        + "}"))
                .andExpect(status().isBadRequest());

        verify(userRepo, times(0)).findByEmail("newemail@email.com");
        verify(userRepo, times(0)).save(any(User.class));
        verifyNoMoreInteractions(userRepo);
    }

    /**
     * In that case, the password length is not valid Password's length should
     * be minimum 6 characters.
     */
    @Test
    public void testUsersPost_badRequest_invalidPasswordLength() throws Exception {

        // check for the email to be unique
        given(userRepo.findByEmail("newemail@email.com")).willReturn(null);
        given(userRepo.save(any(User.class))).willReturn(new User(1));

        mockMvc.perform(post("/api/users")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"firstName\": \"user1\",\n"
                        + "  \"lastName\": \"lastname1\",\n"
                        + "  \"email\": \"newemail@email@email.com\",\n"
                        + "  \"password\": \"123\",\n"
                        + "  \"role\": \"another role\"\n"
                        + "}"))
                .andExpect(status().isBadRequest());

        verify(userRepo, times(0)).findByEmail("newemail@email.com");
        verify(userRepo, times(0)).save(any(User.class));
        verifyNoMoreInteractions(userRepo);
    }

}
