/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.api;

import org.junit.Test;
import org.junit.Rule;
import org.junit.Before;
import org.junit.BeforeClass;
import org.joda.time.LocalTime;
import org.junit.runner.RunWith;

import org.mockito.Mock;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import static org.hamcrest.Matchers.*;

import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import org.bootcamp.test.MockAuthRule;
import org.bootcamp.yum.api.model.GlobalSettings;
import org.bootcamp.yum.data.entity.Yum;
import org.bootcamp.yum.data.repository.YumRepository;
import org.bootcamp.yum.exception.ExceptionControllerAdvice;
import org.bootcamp.yum.service.YumService;

/**
 *
 * @author user
 */
@RunWith(MockitoJUnitRunner.class)
public class GlobalsettingsApiControllerTest {

    @Rule
    public final MockAuthRule mockAuth = new MockAuthRule();

    @InjectMocks
    private final YumService yumService = new YumService();

    @Mock
    private YumRepository mockYumRepository;

    private MockMvc mockMvc;
    private static Yum mockYum;

    private static GlobalSettings globalSettings;

    @Before
    public void setUp() {
        mockMvc = MockMvcBuilders.standaloneSetup(new GlobalsettingsApiController(yumService))
                .setControllerAdvice(new ExceptionControllerAdvice())
                .build();
        yumService.setYum(mockYum);
    }

    @BeforeClass
    public static void initMockData() {
        globalSettings = new GlobalSettings("12:00:00.000", "EURO", "notes", "terms", "privacy", 1);

        mockYum = new Yum(1);
        mockYum.setDeadline(LocalTime.parse("12:00:00.000"));
        mockYum.setCurrency("&euro;");
        mockYum.setFoodsVersion(1);
        mockYum.setNotes("notes");
        mockYum.setPrivacy("privacy");
        mockYum.setVersion(1);
        mockYum.setTerms("terms");

    }

    /**
     * In that case, the admin gets successfully the global settings
     */
    @Test
    public void testGlobalsettingsGet() throws Exception {
        // telling Mockito to use this mock  every time the findById method is called on the yum repository.
        // given(mockYumRepository.findById(1)).willReturn(mockYum);
        // We perform the API call, and check that the response status code, and the JSON response are corrects
        mockMvc.perform(get("/api/globalsettings")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.deadline", is("12:00:00.000")))
                .andExpect(jsonPath("$.currency", is("&euro;")))
                .andExpect(jsonPath("$.version", is(1)));

        // we verify that we called findById method once only on the repo.
        // verify(mockYumRepository, times(1)).findById(1);
        // we verify that we didnt call anything else on the repo
        verifyNoMoreInteractions(mockYumRepository);

    }

    /**
     * In that case, the admin updates successfully the global settings
     */
    @Test
    public void testGlobalsettingsPut() throws Exception {
        // telling Mockito to use this mock  every time the findById method is called on the yum repository.
        // given(mockYumRepository.findById(1)).willReturn(mockYum);
        mockMvc.perform(put("/api/globalsettings")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"deadline\": \"12:00:00\",\n"
                        + "  \"currency\": \"&euro;\",\n"
                        + "  \"notes\": \"notes\",\n"
                        + "  \"terms\": \"terms\",\n"
                        + "  \"privacy\": \"privacy\",\n"
                        + "  \"version\": \"1\" \n"
                        + "}"))
                .andExpect(status().isNoContent());
        // we verify that we called findById method once only on the repo.
        //verify(mockYumRepository, times(1)).findById(1);
        // we verify that we didnt call anything else on the repo
        //verifyNoMoreInteractions(mockYumRepository);
    }

    /**
     * In that case the currency will be not a valid value(euro,dollar)
     */
    @Test
    public void testGlobalsettingsPut_InternalServerError_currency() throws Exception {
        // telling Mockito to use this mock every time the findById method is called on the yum repository.
        //given(mockYumRepository.findById(1)).willReturn(mockYum);
        mockMvc.perform(put("/api/globalsettings")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"deadline\": \"17:00:00\",\n"
                        + "  \"currency\": \"money\",\n"
                        + "  \"notes\": \"notes\",\n"
                        + "  \"terms\": \"terms\",\n"
                        + "  \"privacy\": \"privacy\",\n"
                        + "  \"version\": \"1\" \n"
                        + "}"))
                .andExpect(status().isBadRequest());
        // we verify that we called findById method once only on the repo.
        // verify(mockYumRepository, times(1)).findById(1);
        // we verify that we didnt call anything else on the repo
        verifyNoMoreInteractions(mockYumRepository);
    }

    /**
     * In that case, one or more values of the request body are empty
     */
    @Test
    public void testGlobalsettingsPut_BadRequest_empty() throws Exception {
        // telling Mockito to use this mock every time the findById method is called on the yum repository.
        // given(mockYumRepository.findById(1)).willReturn(mockYum);
        mockMvc.perform(put("/api/globalsettings")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"deadline\": \"\",\n"
                        + "  \"currency\": \"\",\n"
                        + "  \"notes\": \"notes\",\n"
                        + "  \"terms\": \"terms\",\n"
                        + "  \"privacy\": \"privacy\",\n"
                        + "  \"version\": \"1\" \n"
                        + "}"))
                .andExpect(status().isBadRequest());
        // we verify that we called findById method once only on the repo.
        // verify(mockYumRepository, times(0)).findById(1);
        // we verify that we didnt call anything else on the repo
        verifyNoMoreInteractions(mockYumRepository);
    }

    /**
     * Test of globalsettingsPut method, of class GlobalsettingsApiController.
     * In that case, the requested body version is not the same as the version
     * the database has.
     */
    @Test
    public void testGlobalsettingsPut_ConcurrentModification() throws Exception {
        // telling Mockito to use this mock every time the findById method is called on the yum repository.
        //given(mockYumRepository.findById(1)).willReturn(mockYum);
        mockMvc.perform(put("/api/globalsettings")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"deadline\": \"12:00:00\",\n"
                        + "  \"currency\": \"&euro;\",\n"
                        + "  \"notes\": \"notes\",\n"
                        + "  \"terms\": \"terms\",\n"
                        + "  \"privacy\": \"privacy\",\n"
                        + "  \"version\": \"5\" \n"
                        + "}"))
                .andExpect(status().isConflict());
        // we verify that we called findById method once only on the repo.
        //verify(mockYumRepository, times(1)).findById(1);
        // we verify that we didnt call anything else on the repo
        verifyNoMoreInteractions(mockYumRepository);
    }

}
