/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.api;

import java.util.List;
import java.util.ArrayList;
import java.math.BigDecimal;

import org.joda.time.LocalDate;
import org.joda.time.LocalTime;

import org.junit.Rule;
import org.junit.Test;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import static org.junit.Assert.assertEquals;

import org.mockito.Mock;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.hasSize;

import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import org.bootcamp.enums.FoodEnum;
import org.bootcamp.yum.data.entity.Yum;
import org.bootcamp.yum.data.entity.User;
import org.bootcamp.yum.data.entity.Food;
import org.bootcamp.yum.service.YumService;
import org.bootcamp.yum.service.UserService;
import org.bootcamp.yum.service.MenusService;
import org.bootcamp.yum.data.entity.DailyMenu;
import org.bootcamp.yum.data.entity.OrderItem;
import org.bootcamp.yum.data.entity.DailyOrder;
import org.bootcamp.yum.data.entity.OrderItemKey;
import org.bootcamp.yum.data.repository.YumRepository;
import org.bootcamp.yum.data.repository.UserRepository;
import org.bootcamp.yum.data.repository.DailyMenuRepository;
import org.bootcamp.test.MockAuthRule;
import org.bootcamp.test.annotation.WithMockAuth;
import org.junit.Ignore;

@RunWith(MockitoJUnitRunner.class)
public class MenusApiControllerTest {

    @Rule
    public final MockAuthRule mockAuth = new MockAuthRule();

    @InjectMocks
    private final MenusService menusService = new MenusService();
    @Mock
    private final UserService userService = new UserService();
    @Mock
    private final YumService yumService = new YumService();

    @Mock
    private DailyMenuRepository mockDailyMenuRepo;
    @Mock
    private UserRepository mockUserRepo;
    @Mock
    private YumRepository mockYumRepo;

    private MockMvc mockMvc;

    private static List<DailyMenu> mockDailyMenuList;
    private static User user1;
    private static Yum yum;
    private static List<Food> mockFoodList;  // Do I need this list??
    private static List<DailyOrder> mockDailyOrders;
    private static List<OrderItem> orderItems;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(new MenusApiController(menusService)).build();
    }

    @BeforeClass
    public static void initMockData() {
        // Mock User
        user1 = new User(1L);
        user1.setEmail("test@bootcamp.jrt");

        // Mock Yum table row
        yum = new Yum(1L);
        yum.setDeadline(LocalTime.parse("14:00:00"));

        // Mock List of DailyMenus
        mockDailyMenuList = new ArrayList<>();
        // Adding mockDailyMenu1 instance
        DailyMenu mockDailyMenu1 = new DailyMenu(1);
        mockDailyMenu1.setOfDate(new LocalDate(2017, 4, 5));
        mockDailyMenu1.setExpired(true);
        mockDailyMenu1.setVersion(1);
        mockDailyMenuList.add(mockDailyMenu1);

        // Mock List of Foods
        mockFoodList = new ArrayList<>();
        // Adding mockFood1 instance
        Food mockFood1 = new Food(10);
        mockFood1.setArchived(true);
        mockFood1.setDescription("test Pastitsio");
        mockFood1.setName("Pastitsio");
        mockFood1.setType(FoodEnum.MAIN);
        mockFood1.setPrice(new BigDecimal("5.65"));
        System.out.println("mockFood1 " + mockFood1);
        System.out.println("mockDailyMenu1 " + mockDailyMenu1);
        mockFoodList.add(mockFood1);  // Do I need this list??
        // Add mockFood1 to mockDailyMenu1
        mockDailyMenu1.addFood(mockFood1);
        // Adding mockFood2 instance
        Food mockFood2 = new Food(1);
        mockFood2.setArchived(true);
        mockFood2.setDescription("test Pastitsio2");
        mockFood2.setName("Pastitsio2");
        mockFood2.setType(FoodEnum.MAIN);
        mockFood2.setPrice(new BigDecimal("6.65"));
        mockFoodList.add(mockFood2);  // Do I need this list??
        // Add mockFood2 to mockDailyMenu1
        mockDailyMenu1.addFood(mockFood2);

        // Mock List of DailyOrders
        mockDailyOrders = new ArrayList<>();
        // Adding mockDailyOrder1 instance
        DailyOrder mockDailyOrder1 = new DailyOrder();
        mockDailyOrder1.setFinalOrder(true);
        mockDailyOrder1.setVersion(1);
        mockDailyOrder1.setUser(user1);
        mockDailyOrder1.setDailyMenu(mockDailyMenu1);
        mockDailyOrders.add(mockDailyOrder1);  // Do I need this list??
        // Add mockFoods to mockDailyMenu1
        mockDailyMenu1.addDailyOrder(mockDailyOrder1);

        // Mock List of OrderItems
        orderItems = new ArrayList<>();
        // Adding mockOrderItem1 instance
        OrderItem mockOrderItem1 = new OrderItem(new OrderItemKey(1L, 10L), 2);
        mockOrderItem1.setDailyOrder(mockDailyOrder1);
        mockOrderItem1.setFood(mockFood1);
        orderItems.add(mockOrderItem1);
        // Add mockOrderItem1 to mockDailyOrder1
        mockDailyOrder1.addOrderItem(mockOrderItem1);
    }

    /**
     * Test of menusWeeklyGet method, of class MenusApiController. Case a list
     * of DailyMenus has been returned.
     */
    @Test
    @Ignore
    @WithMockAuth(id = "1")
    public void testMenusWeeklyGet() throws Exception {

        // telling Mockito to use these mock lists
        given(mockDailyMenuRepo.findByOfDate(LocalDate.now())).willReturn(mockDailyMenuList.get(0));

        given(mockUserRepo.findById(1L)).willReturn(user1);
        given(userService.getLoggedInUser()).willReturn(user1);

        given(mockYumRepo.findById(1)).willReturn(yum);
        given(yumService.getDeadline()).willReturn(yum.getDeadline());

        mockMvc.perform(get("/api/menus/weekly")).andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].foods[0].id", is(10)));

        // we verify that we called findByOfDate method only once on the repo.
        verify(mockDailyMenuRepo, times(1)).findByOfDate(LocalDate.now());
        // we verify that we didnt modify the first food item in the repository
        assertEquals(mockDailyMenuList.get(0), mockDailyMenuRepo.findByOfDate(LocalDate.now()));

    }

    /**
     * Test of menusWeeklyWeekYearGet method, of class MenusApiController.
     */
    @Test
    public void testMenusWeeklyWeekYearGet() {

    }

    /**
     * Test of menusMonthlyGet method, of class MenusApiController.
     */
    @Test
    public void testMenusMonthlyGet() {

    }

    /**
     * Test of menusMonthlyMonthYearGet method, of class MenusApiController.
     */
    @Test
    public void testMenusMonthlyMonthYearGet() {

    }

}
