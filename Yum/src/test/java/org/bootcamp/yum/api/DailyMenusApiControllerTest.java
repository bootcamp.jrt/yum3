/*
 * Copyright (C) 2017 JR Technologies.
 * This file is part of Yum.
 * 
 * Yum is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
 * 
 * Yum is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License along with Yum. 
 * If not, see <http://www.gnu.org/licenses/>.
 */
package org.bootcamp.yum.api;

import java.util.List;
import java.math.BigDecimal;
import java.util.ArrayList;

import org.joda.time.LocalDate;

import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.hasSize;

import org.joda.time.LocalTime;

import org.junit.Rule;
import org.junit.Test;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import org.mockito.Mock;
import org.mockito.InjectMocks;
import org.mockito.runners.MockitoJUnitRunner;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.BDDMockito.given;

import org.bootcamp.enums.FoodEnum;
import org.bootcamp.test.MockAuthRule;
import org.bootcamp.yum.data.entity.Food;
import org.bootcamp.yum.data.entity.DailyMenu;
import org.bootcamp.yum.data.entity.OrderItem;
import org.bootcamp.yum.data.entity.DailyOrder;
import org.bootcamp.yum.data.entity.OrderItemKey;
import org.bootcamp.yum.data.entity.Yum;
import org.bootcamp.yum.data.repository.FoodRepository;
import org.bootcamp.yum.data.repository.DailyMenuRepository;
import org.bootcamp.yum.data.repository.OrderItemRepository;
import org.bootcamp.yum.data.repository.YumRepository;
import org.bootcamp.yum.exception.ExceptionControllerAdvice;
import org.bootcamp.yum.service.DailyMenusService;
import org.bootcamp.yum.service.YumService;

/**
 *
 * @author user
 */
@RunWith(MockitoJUnitRunner.class)
public class DailyMenusApiControllerTest {

    @Rule
    public final MockAuthRule mockAuth = new MockAuthRule();

    @InjectMocks
    private final DailyMenusService dailyMenusService = new DailyMenusService();
    @Mock
    private DailyMenuRepository mockDailyMenuRepo;
    @Mock
    private FoodRepository mockFoodRepo;
    @Mock
    private OrderItemRepository mockOrderItemRepo;
    @Mock
    private YumRepository mockYumRepo;
    @Mock
    private final YumService yumService = new YumService();

    private MockMvc mockMvc;

    private static Yum yum;
    private static List<DailyMenu> mockDailyMenuList;
    //private static User user1;
    private static List<Food> mockFoodList;
    private static List<DailyOrder> mockDailyOrders;
    private static List<OrderItem> orderItems;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders.standaloneSetup(new DailyMenusApiController(dailyMenusService))
                .setControllerAdvice(new ExceptionControllerAdvice())
                .build();
    }

    @BeforeClass
    public static void initMockData() {

        // Mock Yum table row
        yum = new Yum(1L);
        yum.setDeadline(LocalTime.parse("14:00:00"));

        // Mock List of DailyMenus
        mockDailyMenuList = new ArrayList<>();
        // Adding mockDailyMenu1 instance
        DailyMenu mockDailyMenu1 = new DailyMenu(1);
        mockDailyMenu1.setOfDate(new LocalDate(2017, 5, 30));
        mockDailyMenu1.setVersion(1);
        mockDailyMenuList.add(mockDailyMenu1);

        // Mock List of Foods
        mockFoodList = new ArrayList<>();
        // Adding the first mock Food instance
        Food mockFood1 = new Food(1);
        mockFood1.setArchived(true);
        mockFood1.setDescription("test Pastitsio");
        mockFood1.setName("Pastitsio");
        mockFood1.setType(FoodEnum.MAIN);
        mockFood1.setPrice(new BigDecimal("5.65"));
        mockFood1.setVersion(1);
        mockFood1.addDailyMenu(mockDailyMenu1);
        mockFoodList.add(mockFood1);
        // Add mockFood1 to mockDailyMenu1
        mockDailyMenu1.addFood(mockFood1);
        // Adding mockFood2 instance
        Food mockFood2 = new Food(2);
        mockFood2.setArchived(false);
        mockFood2.setDescription("test Pastitsio2");
        mockFood2.setName("Pastitsio2");
        mockFood2.setType(FoodEnum.MAIN);
        mockFood2.setPrice(new BigDecimal("7.65"));
        mockFood2.setVersion(1);
        mockFood2.addDailyMenu(mockDailyMenu1);
        mockFoodList.add(mockFood2);
        // Add mockFood2 to mockDailyMenu1
        mockDailyMenu1.addFood(mockFood2);
        // Adding mockFood3 instance
        Food mockFood3 = new Food(3);
        mockFood3.setArchived(false);
        mockFood3.setDescription("test Pastitsio3");
        mockFood3.setName("Pastitsio3");
        mockFood3.setType(FoodEnum.MAIN);
        mockFood3.setPrice(new BigDecimal("8.65"));
        mockFood3.setVersion(1);
        mockFood3.addDailyMenu(mockDailyMenu1);
        mockFoodList.add(mockFood3);

        // Mock List of DailyOrders
        mockDailyOrders = new ArrayList<>();
        // Adding mockDailyOrder1 instance
        DailyOrder mockDailyOrder1 = new DailyOrder();
        mockDailyOrder1.setFinalOrder(true);
        mockDailyOrder1.setVersion(1);
        //mockDailyOrder1.setUser(user1);
        mockDailyOrder1.setDailyMenu(mockDailyMenu1);
        mockDailyOrders.add(mockDailyOrder1);  // Do I need this list??
        // Add mockFoods to mockDailyMenu1
        mockDailyMenu1.addDailyOrder(mockDailyOrder1);

        // Mock List of OrderItems
        orderItems = new ArrayList<>();
        // Adding mockOrderItem1 instance
        OrderItem mockOrderItem1 = new OrderItem(new OrderItemKey(1L, 1L), 2);
        mockOrderItem1.setDailyOrder(mockDailyOrder1);
        mockOrderItem1.setFood(mockFood1);
        orderItems.add(mockOrderItem1);
        // Add mockOrderItem1 to mockDailyOrder1
        mockDailyOrder1.addOrderItem(mockOrderItem1);
    }

    /**
     * Test of dailyMenusMonthlyGet method, of class DailyMenusApiController.
     */
    @Test
    public void testDailyMenusMonthlyGet() {
        System.out.println("dailyMenusMonthlyGet");
    }

    /**
     * Test of dailyMenusMonthlyMonthYearGet method, of class
     * DailyMenusApiController.
     */
    @Test
    public void testDailyMenusMonthlyMonthYearGet() {
        System.out.println("dailyMenusMonthlyMonthYearGet");
    }

    /**
     * Test of dailyMenusIdPut method, of class DailyMenusApiController. Code:
     * 200
     */
    @Test
    public void testDailyMenusIdPut_Ok() throws Exception {
        System.out.println("dailyMenusIdPut_Ok");

        given(mockDailyMenuRepo.findById(any(Long.class))).willReturn(mockDailyMenuList.get(0));
        given(mockFoodRepo.findById(1)).willReturn(mockFoodList.get(0));
        given(mockFoodRepo.findById(2)).willReturn(mockFoodList.get(1));
        given(mockFoodRepo.findById(3)).willReturn(mockFoodList.get(2));

        given(mockYumRepo.findById(1)).willReturn(yum);
        given(yumService.getDeadline()).willReturn(yum.getDeadline());

        mockMvc.perform(put("/api/dailyMenus/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"date\": \"2017-05-30\",\n"
                        + "  \"version\": 1,\n"
                        + "  \"foods\": [\n"
                        + "    {\n"
                        + "      \"id\": 1\n"
                        + "    },\n"
                        + "    {\n"
                        + "      \"id\": 3\n"
                        + "    }\n"
                        + "  ]\n"
                        + "}"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id", is(1)))
                .andExpect(jsonPath("$.foods", hasSize(2)))
                .andExpect(jsonPath("$.foods[0].foodId", is(1)));

    }

    /**
     * Test of dailyMenusIdPut method, of class DailyMenusApiController. Code:
     * 400 Reason: given foodId doesn't exist in the database
     */
    @Test
    public void testDailyMenusIdPut_400() throws Exception {
        System.out.println("dailyMenusIdPut_400");
        System.out.println("mockedDailyMenu version: " + mockDailyMenuList.get(0).getVersion());
        given(mockDailyMenuRepo.findById(any(Long.class))).willReturn(mockDailyMenuList.get(0));
        given(mockFoodRepo.findById(1)).willReturn(mockFoodList.get(0));
        given(mockFoodRepo.findById(4)).willReturn(null);

        mockMvc.perform(put("/api/dailyMenus/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"version\": 2,\n"
                        + "  \"foods\": [\n"
                        + "    {\n"
                        + "     \"id\": 1\n"
                        + "    },\n"
                        + "    {\n"
                        + "     \"id\": 4\n"
                        + "    }\n"
                        + "  ]\n"
                        + "}"))
                .andExpect(status().isBadRequest());

    }

    /**
     * Test of dailyMenusIdPut method, of class DailyMenusApiController. Code:
     * 410 Reason: given DailyMenu id doesn't exist
     */
    @Test
    public void testDailyMenusIdPut_410() throws Exception {
        System.out.println("dailyMenusIdPut_400");

        given(mockDailyMenuRepo.findById(any(Long.class))).willReturn(null);

        mockMvc.perform(put("/api/dailyMenus/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"date\": \"2017-05-25\",\n"
                        + "  \"version\": 15,\n"
                        + "  \"foods\": [\n"
                        + "    {\n"
                        + "     \"id\": 1\n"
                        + "    },\n"
                        + "    {\n"
                        + "     \"id\": 4\n"
                        + "    }\n"
                        + "  ]\n"
                        + "}"))
                .andExpect(status().isGone());

    }

    /**
     * Test of dailyMenusPost method, of class DailyMenusApiController. Code:
     * 200
     */
    @Test
    public void testDailyMenusPost_Ok() throws Exception {
        System.out.println("dailyMenusPost_Ok");

        given(mockDailyMenuRepo.findByOfDate(any(LocalDate.class))).willReturn(null);
        given(mockFoodRepo.findById(any(Long.class))).willReturn(mockFoodList.get(1));
        given(mockDailyMenuRepo.save(any(DailyMenu.class))).willReturn(new DailyMenu(1));

        given(mockYumRepo.findById(1)).willReturn(yum);
        given(yumService.getDeadline()).willReturn(yum.getDeadline());

        mockMvc.perform(post("/api/dailyMenus")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"date\": \"2017-09-06\",\n"
                        + "  \"foods\": [\n"
                        + "    {\n"
                        + "     \"id\": 2\n"
                        + "    },\n"
                        + "    {\n"
                        + "     \"id\": 3\n"
                        + "    }\n"
                        + "  ]\n"
                        + "}"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(jsonPath("$.id", is(0)))
                .andExpect(jsonPath("$.foods", hasSize(2)))
                .andExpect(jsonPath("$.foods[0].foodId", is(2)));

        verify(mockDailyMenuRepo, times(1)).findByOfDate(any(LocalDate.class));
        verify(mockDailyMenuRepo, times(1)).save(any(DailyMenu.class));
    }

    /**
     * Test of dailyMenusPost method, of class DailyMenusApiController. Code:
     * 400 Bad Request Reason: Empty fields
     */
    @Test
    public void testDailyMenusPost_400_badRequest_emptyFields() throws Exception {
        System.out.println("dailyMenusPost_500_internalServerError_emptyFields");

        given(mockDailyMenuRepo.findByOfDate(any(LocalDate.class))).willReturn(null);
        given(mockFoodRepo.findById(any(Long.class))).willReturn(mockFoodList.get(1));
        given(mockDailyMenuRepo.save(any(DailyMenu.class))).willReturn(new DailyMenu(1));

        mockMvc.perform(post("/api/dailyMenus")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"date\": \"\",\n" // Empty Date field
                        + "  \"foods\": [\n"
                        + "    {\n"
                        + "     \"id\": 2\n"
                        + "    },\n"
                        + "    {\n"
                        + "     \"id\": 3\n"
                        + "    }\n"
                        + "  ]\n"
                        + "}"))
                .andExpect(status().isBadRequest());

        verify(mockDailyMenuRepo, times(0)).findByOfDate(any(LocalDate.class));
        verify(mockDailyMenuRepo, times(0)).save(any(DailyMenu.class));
    }

    /**
     * Test of dailyMenusPost method, of class DailyMenusApiController. Code:
     * 400 BadRequest Reason: Archived Food
     */
    @Test
    public void testDailyMenusPost_400_badRequest_archivedFood() throws Exception {
        System.out.println("dailyMenusPost_400_badRequest_archivedFood");

        given(mockDailyMenuRepo.findByOfDate(any(LocalDate.class))).willReturn(null);
        given(mockFoodRepo.findById(0)).willReturn(mockFoodList.get(0));
        given(mockFoodRepo.findById(1)).willReturn(mockFoodList.get(1));
        given(mockDailyMenuRepo.save(any(DailyMenu.class))).willReturn(new DailyMenu(1));

        mockMvc.perform(post("/api/dailyMenus")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content("{\n"
                        + "  \"date\": \"2017-09-06\",\n" // Empty Date field
                        + "  \"foods\": [\n"
                        + "    {\n"
                        + "     \"id\": 1\n"
                        + "    },\n"
                        + "    {\n"
                        + "     \"id\": 2\n"
                        + "    }\n"
                        + "  ]\n"
                        + "}"))
                .andExpect(status().isBadRequest());

        verify(mockDailyMenuRepo, times(1)).findByOfDate(any(LocalDate.class));
        verify(mockDailyMenuRepo, times(0)).save(any(DailyMenu.class));
    }

}
