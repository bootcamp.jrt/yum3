import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AuthApi } from './remote';
import { BASE_PATH } from './remote/variables';

import 'hammerjs';

import {
  AppGuard,
  CanLoginGuard,
  IsLoggedInGuard,
  CanActivateChefGuard,
  CanActivateAdminGuard,
  CanActivateSettingsGuard
} from './app.guard';

import { AppComponent } from './app.component';
import { SharedModule } from './shared/shared.module';
import { AnonModule } from './anon/anon.module';
import { ChefModule } from './chef/chef.module';
import { AdminModule } from './admin/admin.module';
import { HungryModule } from './hungry/hungry.module';
import { AppRouting } from './app.routing';
import { AuthenticationService } from './shared/authentication.service';

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    HungryModule,
    AnonModule,
    ChefModule,
    AdminModule,
    AppRouting,
    SharedModule
  ],
  providers: [AuthApi,
    AuthenticationService,
    { provide: BASE_PATH, useValue: "http://localhost:8082/api" },
    AppGuard,
    CanLoginGuard,
    IsLoggedInGuard,
    CanActivateChefGuard,
    CanActivateAdminGuard,
    CanActivateSettingsGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
