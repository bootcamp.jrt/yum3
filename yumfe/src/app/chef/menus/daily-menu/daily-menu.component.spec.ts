import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DailyMenuComponentComponent } from './daily-menu-component.component';

describe('DailyMenuComponentComponent', () => {
  let component: DailyMenuComponentComponent;
  let fixture: ComponentFixture<DailyMenuComponentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DailyMenuComponentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DailyMenuComponentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
