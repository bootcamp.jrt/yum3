import { Component, OnInit, Input } from '@angular/core';
import { FoodsService } from '../../foods.service';
import * as remote from '../../../remote';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { MdSnackBar } from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';
import { CustomIcons } from '../../../shared/custom-icons/custom-icons';

@Component({
  selector: 'app-daily-menu',
  templateUrl: './daily-menu.component.html',
  styleUrls: ['./daily-menu.component.scss']
})
export class DailyMenuComponent implements OnInit {
  @Input()
  public dailyMenuChef;
  @Input()
  public date: Date;
  @Input()
  public viewMonth: Date;
  @Input()
  public activeFoodsOriginal;

  public customIcons: CustomIcons = new CustomIcons();

  public selectedValue: string;
  public dropdownFoods = []; // this is just a copy of the original active foods
  public dropdownFoodnames = []; //that array includes only the names of the dropdown
  public dropDownFoodsOriginal = new Map<string, any>();
  private menuFoodsOriginal = []; //that array includes the foods of the daily menu
  public foods = []; // this is just a copy of the original daily menu foods
  private undefinedMenu: boolean = false; // that boolean describes if the dailyMenu exists or not
  private dailyMenuDate;
  public notFinal: boolean = false;
  private menuVersion = 0;
  private menuId = 0;
  private year;
  private month;
  private deleted = false;
  private deletedDate; // the date of a deleted menu
  public buttonsActive = false; // if the menus are actives or not
  private allFoods = new Map<string, any>(); // a map that contains all the foods
  public spinnerisActive = false; // a boolean to desscribe if spinner is visible or not


  constructor(private foodService: FoodsService,
    private chefService: remote.ChefApi,
    private route: ActivatedRoute,
    public snackBar: MdSnackBar,
    private router: Router) { }

  ngOnInit() {

    //creating the dropdown names
    this.getDropdownNames();

    // if there is a DailyMenu
    if (this.dailyMenuChef) {
      this.year = this.date.getFullYear().toString();
      this.month = (this.date.getMonth() + 1).toString();
      if (Number(this.month) < 10) {
        this.month = '0' + this.month;
      }
      this.isNotFinal();
      this.getFoodNames();
      this.calculateDropDownMenu();
      //getting the menu's id and version
      this.menuId = this.dailyMenuChef.menuId;
      this.menuVersion = this.dailyMenuChef.menuVersion;
    } else { //setting the undefined menu
      this.undefinedMenu = true;
      this.createUndefinedDailyMenu();
      this.isNotFinal();
    }
    //getting all the foods of the menu and the and dropdown
    this.createAllFoodsArray();

  }


  //that method creates a table that includes
  // all the foods of dropdown menu and the dailyMenu
  createAllFoodsArray() {
    this.allFoods.clear();
    for (let foodObject of this.dropdownFoods) {
      this.allFoods.set(foodObject.name, foodObject);
    }
    for (let foodObject of this.dailyMenuChef.foods) {
      this.allFoods.set(foodObject.name, foodObject);
    }


  }


  // creating a clone of the original active foods
  getDropdownNames() {
    this.dropdownFoods = [];
    this.dropDownFoodsOriginal.clear();
    this.dropdownFoodnames = [];

    for (let activeFood of this.activeFoodsOriginal) {
      this.dropdownFoods.push(activeFood);
      this.dropDownFoodsOriginal.set(activeFood.name, activeFood);
      this.dropdownFoodnames.push(activeFood.name);
    }

  }


  // that method checks if a menu is final or not
  isNotFinal() {
    //check if the menu is undefined to calculate if it can be edited
    if (this.undefinedMenu) {
      let pickedDate = Date.parse(this.dailyMenuDate.replace(/-/g, " "));
      let todaysDate = new Date();
      todaysDate.setHours(0, 0, 0, 0);
      let dateDifference = Number(todaysDate) - pickedDate;

      // check to be after 2 days (-172800000 ms)
      if (dateDifference > -172800000) {
        return false;
      }
      //previous days
      return true;
    }

    if (this.dailyMenuChef.foods[0]) {//check an already existing menu if is final
      this.notFinal = !this.dailyMenuChef.foods[0].final;
      return this.notFinal;
    } else if (this.notFinal) { //if a menu has just been created
      return true;
    } else { // if a menu deadline has passed
      return true;
    }

  }


  //that method creates the undefined menus
  //with a date and an empty menu
  createUndefinedDailyMenu() {
    //preparing the date for the new menu
    this.year = this.date.getFullYear().toString();
    this.month = (this.date.getMonth() + 1).toString();
    if (Number(this.month) < 10) {
      this.month = '0' + this.month;
    }
    let date = this.date.getDate().toString();
    if (Number(date) < 10) {
      date = '0' + date;
    }
    this.dailyMenuDate = this.year + '-' + this.month + '-' + date;

    //creating a new DailyMenu Object
    let dailyMenuChef: remote.DailyMenuChef = {
      date: new Date(this.dailyMenuDate),
      foods: this.foods
    };
    this.dailyMenuChef = dailyMenuChef;

  }


  //that method creates a clone of the food names
  // from the original array
  getFoodNames() {
    this.foods = [];
    this.menuFoodsOriginal = [];
    for (let food of this.dailyMenuChef.foods) {
      this.foods.push(food);
      this.menuFoodsOriginal.push(food);
    }

  }


  //creating the dropdown option, so it does
  // not contain the food names of the menus
  calculateDropDownMenu() {
    for (let dailyMenuFood of this.dailyMenuChef.foods) {
      let counter = 0;
      for (let dropdownFood of this.dropdownFoods) {
        if (dropdownFood.name === dailyMenuFood.name) {
          this.dropdownFoods.splice(counter, 1);
          this.dropdownFoodnames.splice(counter, 1);
          this.dropDownFoodsOriginal.delete(dropdownFood.name);
        }
        counter++;
      }
    }

  }


  //that method removes a list item if it's possible
  removeListItem(foodObject) {
    this.selectedValue = '';
    //method to check if the menu content has changed
    let counter = 0;
    let index;

    // getting the informations about that food
    let foodItem = this.allFoods.get(foodObject.name);

    //check if the food is archived
    // so just remove it from the menu items list
    if (foodItem.archived) {
      for (let food of this.foods) {
        if (food.name === foodObject.name) {
          index = counter;
        }
        counter++;
      }
      this.foods.splice(index, 1);
      this.isMenuContentsChanged();
    }
    else { //we have to remove it from dropdown menu and the menu items list
      for (let food of this.foods) {
        if (food.name === foodObject.name) {
          index = counter;
        }
        counter++;
      }
      this.foods.splice(index, 1);

      this.dropdownFoods.push(foodObject);
      this.sortByTypeOfFood(this.dropdownFoods, true);
      this.isMenuContentsChanged();
    }

  }

  //that method always sortts the lists by foodItem type (MAIN/SALAD/DRINK)
  //the boolean is true for dropdown list
  //and it is false when it s the menuItems list to be sorted
  sortByTypeOfFood(list: any, dropDownList: boolean) {

    if (dropDownList) { // in that case we sort the dropdown list
      this.dropdownFoods = [];
      for (let listItem of list) {
        let foodItem = this.allFoods.get(listItem.name);
        if (foodItem.type === 'MAIN' || foodItem.foodType === 'MAIN') {
          this.dropdownFoods.push(foodItem);
        }
      }
      for (let listItem of list) {
        let foodItem = this.allFoods.get(listItem.name);
        if (foodItem.type === 'SALAD' || foodItem.foodType === 'SALAD') {
          this.dropdownFoods.push(foodItem);
        }
      }
      for (let listItem of list) {
        let foodItem = this.allFoods.get(listItem.name);
        if (foodItem.type === 'DRINK' || foodItem.foodType === 'DRINK') {
          this.dropdownFoods.push(foodItem);
        }
      }
    } else { // in that case we sort the menu items list
      this.foods = [];
      for (let listItem of list) {
        let foodItem = this.allFoods.get(listItem.name);
        if (foodItem.type === 'MAIN' || foodItem.foodType === 'MAIN') {
          this.foods.push(foodItem);
        }
      }
      for (let listItem of list) {
        let foodItem = this.allFoods.get(listItem.name);
        if (foodItem.type === 'SALAD' || foodItem.foodType === 'SALAD') {
          this.foods.push(foodItem);
        }
      }
      for (let listItem of list) {
        let foodItem = this.allFoods.get(listItem.name);
        if (foodItem.type === 'DRINK' || foodItem.foodType === 'DRINK') {
          this.foods.push(foodItem);
        }
      }
    }

  }


  //that method detects the changes of the select option so we can add
  // and remove objects from that list
  onChange(foodName) {

    let foodObject = this.allFoods.get(foodName.name);
    let index = this.dropdownFoods.indexOf(foodName, 0);
    this.foods.push(foodObject);
    this.sortByTypeOfFood(this.foods, false);
    this.dropdownFoods.splice(index, 1);
    this.isMenuContentsChanged();
    this.selectedValue = '';

  }


  //that method checks if there is a change on the menu
  isMenuContentsChanged() {
    let counter = 0;
    //if the sizes are not the same then the content has changed
    if (this.foods.length !== this.menuFoodsOriginal.length) {
      this.buttonsActive = true;
    } else {
      for (let foodNameOriginal of this.menuFoodsOriginal) {
        for (let food of this.foods) {
          if (foodNameOriginal.name === food.name) {
            counter++;
          }
        }
      }
      // if the counter of equality has the same size as the
      // menuFoodsOriginal.length then the content is the same
      if (counter === this.menuFoodsOriginal.length) {
        this.buttonsActive = false;
      } else {
        this.buttonsActive = true;
      }
    }

  }


  //that method checks if a food is ordered or not
  canBeRemoved(foodObject) {
    //if the menu is undefined or if it has just
    //been deleted every item can be placed and removed
    if (this.undefinedMenu || this.deleted) {
      return true;
    }


    for (let foodItem of this.dailyMenuChef.foods) {
      if (foodItem.name === foodObject.name) {
        if (foodItem.quantity > 0) {
          return false;
        }
      }
    }
    //if that foodObject was not found in the dailyMenu
    //then it came from the dropdown and it can be removed
    return true;

  }


  //that method resets the daily menu to the original form
  cancel() {
    this.selectedValue = '';
    this.buttonsActive = false;
    //check if the menu is undefined
    if (this.undefinedMenu) {
      this.foods = [];
      this.dropdownFoods = [];
      this.getDropdownNames();

    } else {
      this.dropdownFoods = [];
      this.getFoodNames();
      this.getDropdownNames();
      this.calculateDropDownMenu();
    }

  }


  saveUpdates() {

    //PUT daily menu because that dailyMenu already exists
    if (!this.undefinedMenu && !this.deleted) {
      this.spinnerisActive = true;
      this.dailyMenuPut();

      this.sortByTypeOfFood(this.foods, false);
      this.sortByTypeOfFood(this.dropdownFoods, true);
    } else if (this.deleted || this.undefinedMenu) {
      //POST a new menu for that day if the menu was originally undefined OR
      // it was previously deleted
      this.spinnerisActive = true;
      this.dailyMenuPost();

      this.sortByTypeOfFood(this.foods, false);
      this.sortByTypeOfFood(this.dropdownFoods, true);
    }
  }


  //that method updates an existing dailyMenu
  dailyMenuPut() {
    this.buttonsActive = false;
    let foods = Array<remote.DailyMenusFoods>();

    //creating the DailyMenusFoods object the foods ids
    for (let foodName of this.foods) {
      let dailyMenusFoods: remote.DailyMenusFoods = {
        id: foodName.id
      };
      foods.push(dailyMenusFoods);
    }

    //creating the UpdateDailyMenu object
    let menu: remote.UpdateDailyMenu = {
      date: this.dailyMenuChef.date,
      version: this.menuVersion,
      foods: foods
    };

    //API PUT call
    this.chefService.dailyMenusIdPut(this.menuId, menu)
      .subscribe(data => {

        if (data === undefined) { //in that case we must delete the menu
          this.deleted = true;
          this.menuId = 0;
          this.menuVersion = 0;
          this.deletedDate = menu.date;

          //reseting the new values of the original values
          this.dailyMenuChef.foods = []; // the daily menu has no menu items
          this.getDropdownNames();
          this.openSuccessSnackBar('Menu successfully deleted!', 'ok');
          this.spinnerisActive = false;
        } else { // in that case we must update that menu

          this.menuVersion = data.version;
          //reseting the new values of the original values
          this.dailyMenuChef.foods = [];
          for (let newDailyMenuFood of this.foods) {
            this.dailyMenuChef.foods.push(newDailyMenuFood);
          }
          this.getFoodNames();
          this.getDropdownNames();
          this.calculateDropDownMenu();
          this.openSuccessSnackBar('Menu successfully updated!', 'ok');
          this.spinnerisActive = false;
        }
      }, error => {
        if (error.status === 412) {
          this.openErrorSnackBar('The menu is final and cannot be updated', 'ok');
          this.spinnerisActive = false;
        }
        else if (error.status === 400) {
          this.openErrorSnackBar('Something went wrong. Please reload your page', 'ok');
          this.spinnerisActive = false;
        }
        else if (error.status === 410) {
          //case of concurrent deletion

          this.deleted = true; //we declare the menu as deleted
          this.deletedDate = menu.date;
          this.dailyMenuChef.foods = []; // reseting the original values
          this.getFoodNames();
          this.getDropdownNames();
          this.selectedValue = '';
          this.openErrorSnackBar(error.json().message, 'ok');
          this.spinnerisActive = false;

        } else {
          //case 409 of concurrent modfication
          this.openErrorSnackBar('The menu has been already modified before and now is refreshed', 'ok');
          let dailyMenu = error.json();
          this.menuVersion = dailyMenu.version; // getting the new version number of the menu
          this.menuId = dailyMenu.id;
          //in that case we have to refresh the dailyMenu
          this.foodService.getDailyMenuFoods(error.json())
            .subscribe(newMenu => {
              this.dailyMenuChef = newMenu;
              this.dropdownFoods = [];
              this.isNotFinal();
              this.getDropdownNames();
              this.getFoodNames();
              this.calculateDropDownMenu();
              this.createAllFoodsArray();

            }, error => {
            });
          this.spinnerisActive = false;
        }
      });

  }


  //that method POSTs a new dailyMenu
  dailyMenuPost() {
    this.buttonsActive = false;

    let foods = Array<remote.DailyMenusFoods>();
    //creating the DailyMenusFoods object
    for (let foodName of this.foods) {
      let dailyMenusFoods: remote.DailyMenusFoods = {
        id: foodName.id
      };
      foods.push(dailyMenusFoods);
    }
    let foodList: remote.FoodList;

    // creating the FoodList object from an empty menu
    foodList = {
      date: this.dailyMenuDate,
      foods: foods
    };
    // creating the FoodList object from a previously deleted menu
    if (this.deleted) {
      foodList = {
        date: this.deletedDate,
        foods: foods
      };
    }

    //API  POST call
    this.chefService.dailyMenusPost(foodList)
      .subscribe(newDailyMenu => {
        this.deleted = false; // we declare now that the menu was not previously deleted
        this.undefinedMenu = false; // we declare now that the menu is not any more undefined(empty)
        this.notFinal = true; // we declare that this menu is not final
        this.menuId = newDailyMenu.id; // declaring the menu's id
        this.menuVersion = newDailyMenu.version;
        this.dailyMenuChef.date = newDailyMenu.date;

        //reseting the new values of the original values
        this.dailyMenuChef.foods = [];
        for (let newDailyMenuFood of this.foods) {
          this.dailyMenuChef.foods.push(newDailyMenuFood);
        }

        this.getFoodNames();
        this.getDropdownNames();
        this.calculateDropDownMenu();
        this.openSuccessSnackBar('Menu successfuly created!', 'ok');
        this.spinnerisActive = false;
      }, error => {
        //concurrent creation error
        if (error.status === 409) {
          this.spinnerisActive = false;
          this.openErrorSnackBar(error.json().message, 'ok');

          //in that case we have to refresh the dailyMenu
          this.foodService.getDailyMenuFoods(error.json().dto)
            .subscribe(newMenu => {

              this.dailyMenuChef = newMenu;
              this.dropdownFoods = [];
              this.isNotFinal();
              this.getDropdownNames();
              this.getFoodNames();
              this.calculateDropDownMenu();
              this.createAllFoodsArray();
              this.menuId = error.json().dto.id;
              this.deleted = false;
              this.undefinedMenu = false; //we declare that the dialyMenu is not any more undefined
              this.menuVersion = error.json().dto.version;
            }, error => {
            });
        } else {
          // in that case the deadline has passed
          // and it can be edited any more
          this.notFinal = false;
          this.spinnerisActive = false;
          this.openErrorSnackBar(error.json().message, 'ok');
        }
      },
      () => {
        this.buttonsActive = false;
      });
  }


  // Opens an Error SnackBar with the requested message and action
  openErrorSnackBar(message: string, action: string): void {

    this.snackBar.open(message, action, {
      extraClasses: ['error-snack-bar']
    });

  }


  // Opens a Success SnackBar with the requested message and action
  openSuccessSnackBar(message: string, action: string): void {

    this.snackBar.open(message, action, {
      extraClasses: ['success-snack-bar'],
      duration: 5000
    });
  }


}
