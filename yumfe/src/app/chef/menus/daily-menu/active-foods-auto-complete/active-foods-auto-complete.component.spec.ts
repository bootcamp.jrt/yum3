import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ActiveFoodsAutoCompleteComponent } from './active-foods-auto-complete.component';

describe('ActiveFoodsAutoCompleteComponent', () => {
  let component: ActiveFoodsAutoCompleteComponent;
  let fixture: ComponentFixture<ActiveFoodsAutoCompleteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ActiveFoodsAutoCompleteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ActiveFoodsAutoCompleteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
