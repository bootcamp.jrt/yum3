import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';

import 'rxjs/add/operator/startWith';
import 'rxjs/add/operator/map';
import { MdAutocompleteModule } from '@angular/material';

@Component({
  selector: 'app-active-foods-auto-complete',
  templateUrl: './active-foods-auto-complete.component.html'
})
export class ActiveFoodsAutoCompleteComponent implements OnInit {

  @Input()
  public dropdownFoods;

  @Output()
  public foodItem = new EventEmitter<any>();

  foodNamesControl: FormControl;
  foods: any = [];
  foodNames = [];

  constructor() {
    this.foodNamesControl = new FormControl();
    this.foods = this.foodNamesControl.valueChanges
      .startWith(null)
      .map(name => this.filterFoods(name));
  }

  filterFoods(val: string) {
    return val ? this.foodNames.filter(s => new RegExp(`^${val}`, 'gi').test(s))
      : this.foodNames;
  }

  ngOnInit() {
    for (let foodItem of this.dropdownFoods) {
      this.foodNames.push(foodItem.name);
    }


  }
  hello( event: any){
     this.foodItem.emit(event);
  }

  onChange(event: any){
    this.foodItem.emit(event);
  }

}
