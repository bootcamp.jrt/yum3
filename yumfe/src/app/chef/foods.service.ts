import {
  Injectable,
  OnInit
} from '@angular/core';
import { DatePipe } from '@angular/common';

import { Observable } from 'rxjs/Observable';

import * as remote from '../remote';

@Injectable()
export class FoodsService {

  version: number = 0;
  foodsMap: Map<number, remote.FoodItem>;

  constructor(
    private chefService: remote.ChefApi,
    private datePipe: DatePipe) { }

  ngOnInit() {

  }

  getFoods(): Observable<Map<number, remote.FoodItem>> {
    let observable = Observable.create((observer) => {

      // // connect to server
      this.chefService.foodsGet(this.version).subscribe(foodItems => {

        this.version = foodItems.foodsVersion;
        this.foodsMap = new Map(foodItems.foodItems
          .map(foodItem => [foodItem.id, foodItem] as [number, remote.FoodItem]));
        observer.next(this.foodsMap);
        observer.complete();

      }, error => {

        if (error.status === 304) {
          observer.next(this.foodsMap);
          observer.complete();
        } else {
          observer.error(error);
        }

      });

    });
    return observable;
  }

  getFoodById(id: number): Observable<remote.FoodItem> {
    let observable = Observable.create((observer) => {
      this.getFoods().subscribe(foodsMap => {
        let foundFood = foodsMap.get(id);
        if (foundFood === undefined)
          observer.error("Found not found");
        else {
          observer.next(foundFood);
          observer.complete();
        }
      });
    });
    return observable;
  }

  getAllFoods(): Observable<Array<remote.FoodItem>> {
    let observable = Observable.create((observer) => {

      this.getFoods().subscribe(foodsMap => {

        let foodsArray = !foodsMap ? [] : Array.from(foodsMap.values());
        observer.next(foodsArray);
        observer.complete();

      }, error => {
        if (error.status === 304) {
          let foodsArray = Array.from(this.foodsMap.values());
          observer.next(foodsArray);
          observer.complete();
        } else {
          observer.error("Internal Server Error");
        }

      })
    });

    return observable;
  }

  //that method gets all the active foods that can be populated
  //to the dropdown menus and sorts them by type
  getActiveFoods(): Observable<Array<remote.FoodItem>> {
    return this.getAllFoods().map(data => {

      let activeFoods = [];
      let sortedByTypeFoods = [];
      for (let item of data) {
        if (!item.archived) {
          activeFoods.push({
            'description': item.description,
            'type': item.foodType,
            'id': item.id,
            'name': item.name,
            'archived': item.archived
          });
        }
      }

      for (let food of activeFoods) {
        if (food.type === 'MAIN') {
          sortedByTypeFoods.push(food);
        }
      }
      for (let food of activeFoods) {
        if (food.type === 'SALAD') {
          sortedByTypeFoods.push(food);
        }
      }
      for (let food of activeFoods) {
        if (food.type === 'DRINK') {
          sortedByTypeFoods.push(food);
        }
      }
      return sortedByTypeFoods;
    });
  }

  //that method creates a map of the date and the acailable menu
  getDailyMenuFoodsMap(monthlyMenu): Observable<Map<string, Object>> {

    let sortedFoods = [];
    let menus = [];
    return this.getAllFoods()
      .map(foodItems => {
        for (let dailyMenu of monthlyMenu) {
          let foods = [];
          for (let foodItem of foodItems) {
            for (let orderItem of dailyMenu.foods) {
              // compare the foodItem(.id) and orderItem(.foodId) by its id
              if (foodItem.id === orderItem.foodId) {
                foods.push({
                  'final': dailyMenu.final,
                  'name': foodItem.name,
                  'description': foodItem.description,
                  'type': foodItem.foodType,
                  'quantity': orderItem.quantity,
                  'id': foodItem.id,
                  'archived': foodItem.archived
                });
              }
            }
          }
          menus.push({
            'date': dailyMenu.date,
            'foods': foods,
            'menuId': dailyMenu.id,
            'menuVersion': dailyMenu.version,
          });
        }
        return new Map(menus
          .map(
          dailyMenu =>
            [dailyMenu.date, dailyMenu] as [string, Object]
          ));
      });
  }

  //that method creates an object that has informations
  //about the menu and the food items
  getDailyMenuFoods(dayMenu): Observable<Object> {
    let menus;
    return this.getAllFoods()
      .map(foodItems => {
        let foods = [];
        for (let foodItem of foodItems) {
          for (let orderItem of dayMenu.foods) {
            if (foodItem.id === orderItem.foodId) {
              foods.push({
                'final': dayMenu.final,
                'description': foodItem.description,
                'name': foodItem.name,
                'type': foodItem.foodType,
                'id': foodItem.id,
                'archived': foodItem.archived
              });
            }
          }
        }
        menus = {
          'date': dayMenu.date,
          'foods': foods,
          'menuVersion': dayMenu.version,
          'menuId': dayMenu.id,
        };

        return menus;
      });

  }



  /**
  *Change the getAllFoods method return array according to the
  * summary ('daily order summary') order items,
  * return a custom object that contains the total price of
  * all order items and another custom object with the food name, price
  * and quantity.
  *
  */
  getDailyOrdersDetails(summary): Observable<Object> {

    return this.getAllFoods()
      .map(foodItems => {
        let newSummary = [];
        let total = 0;
        for (let summaryItem of summary) {
          let newOrderItems = [];
          //loop through the getAllFoods response  array
          for (let foodItem of foodItems) {
            //loop through the summary orderItems array
            for (let orderItem of summaryItem.orderItems) {
              // compare the foodItem(.id) and orderItem(.foodId) by its id
              if (foodItem.id === orderItem.foodId) {
                //if we have a match, we push the response foodItem and order's item quantity to the
                // new order items array.
                newOrderItems.push({
                  'foodItem': foodItem,
                  'quantity': orderItem.quantity
                });
                //calculate the total price of all the order items.
                total += (foodItem.price * orderItem.quantity);
              }
            }
          }
          // when the updated order items array is ready
          //re-create the summary object with the new data
          newSummary.push({
            'foodType': summaryItem.foodType,
            'orderItems': newOrderItems
          });
        }
        // return a custom type object that contains the
        // extented/updated daily orders summary and the total price
        return {
          'summary': newSummary,
          'total': Math.round(total * 100) / 100
        };
      });
  }

  getUserOrdersDetails(usersOrders): Observable<Object> {

    return this.getAllFoods()
      .map(foodItems => {

        let updatedUserOrders = [];

        for (let userOrders of usersOrders) {
          updatedUserOrders.push({
            'firstName': userOrders.firstName,
            'lastName': userOrders.lastName,
            'foods': this.setupUserOrderedFoods(foodItems, userOrders.foods).updatedFoods,
            'total': this.setupUserOrderedFoods(foodItems, userOrders.foods).total
          })
        }

        return updatedUserOrders;
      });
  }

  private setupUserOrderedFoods(foodsList, userFoods) {
    let updatedOrderItems = [];
    let updatedFoodsList = [];
    let newFoods = [];
    let total;

    total = 0;
    for (let summaryItem of userFoods) {
      let newOrderItems = [];
      //loop through the getAllFoods response  array
      for (let foodItem of foodsList) {
        //loop through the summary orderItems array
        for (let orderItem of summaryItem.orderItems) {
          // compare the foodItem(.id) and orderItem(.foodId) by its id
          if (foodItem.id === orderItem.foodId) {
            //if we have a match, we push the response foodItem and order's item quantity to the
            // new order items array.
            newOrderItems.push({
              'name': foodItem.name,
              'price': foodItem.price,
              'quantity': orderItem.quantity
            });
            //calculate the total price of the order items by type.
            total += (foodItem.price * orderItem.quantity);
          }
        }
      }
      // when the updated order items array is ready
      //re-create the foods object with the new data
      newFoods.push({
        'foodType': summaryItem.foodType,
        'orderItems': newOrderItems,
      });
    }
    return {
      'updatedFoods': newFoods,
      'total': total
    };
  }

  setupDailyOrdersChefFoods(dailyOrdersChefData): Observable<Map<string, Object>> {

    return this.getAllFoods().map(
      foodItems => {
        let updatedDailyOrdersChef = [];
        //loop through daily orders array
        for (let dailyOrder of dailyOrdersChefData) {
          let updatedFoodsList = [];
          let total = 0;
          //loop through ordered foods of its day
          for (let orderItem of dailyOrder.foods) {
            //loop through all foods
            for (let foodItem of foodItems) {
              // if orderItem foodId and foodItem id match
              if (orderItem.foodId === foodItem.id) {
                //push to updatedFoodsList the food details (type, name, price)
                updatedFoodsList.push({
                  'type': foodItem.foodType,
                  'name': foodItem.name,
                  'price': foodItem.price,
                  'quantity': orderItem.quantity
                });
                // calc the total price of the orderd items
                total += (foodItem.price * orderItem.quantity);
              }

            }
          }
          // push the new/updated data(foods array) to the DailyOrdersChef array.
          updatedDailyOrdersChef.push({
            'dailyMenuId': dailyOrder.dailyMenuId,
            'date': dailyOrder.date,
            'foods': updatedFoodsList,
            'total': Math.round(total * 100) / 100
          });
        }
        //convert the updatedDailyOrdersChef array to a map
        //with key the daily order's date and value the whole object for this day.
        return new Map(updatedDailyOrdersChef
          .map(
          updatedDailyOrders => [updatedDailyOrders.date, updatedDailyOrders] as [string, Object]));
      }
    );

  }
}
