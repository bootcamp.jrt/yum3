import { Routes, RouterModule } from '@angular/router';

import { CanActivateChefGuard } from '../app.guard';

import { LoggedComponent } from '../shared/logged/logged.component';

import { HomeComponent } from './home/home.component';
import { DailyOrdersComponent } from './ordersday/daily-orders.component';
import { OrdersComponent } from './orders/orders.component';
import { MenusComponent } from '../chef/menus/menus.component';

const chefRoutes: Routes = [
  {
    path: 'chef',
    canActivateChild: [CanActivateChefGuard],
    component: LoggedComponent,
    children: [
      { path: '', component: HomeComponent },
      { path: 'menus', component: MenusComponent },
      { path: 'menus/:year/:month', component: MenusComponent },
      { path: 'orders/day/:day', component: DailyOrdersComponent },
      { path: 'orders', component: OrdersComponent },
      { path: 'orders/:year/:month', component: OrdersComponent },
    ]
  }
];

export const ChefRouting = RouterModule.forChild(chefRoutes);
