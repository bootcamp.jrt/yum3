import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { MdSnackBar } from '@angular/material';

import * as remote from '../../remote';

import { MonthlyGridCalculator } from '../../shared/monthly-grid-calculator';

import { FoodsService } from '../foods.service';


@Component({
  selector: 'app-chef-orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {

  public updatedDailyOrdersChef: Map<string, Object>;
  public monthlyGrid: Array<Date>;
  public ready: boolean = false;
  public month: number = new Date().getMonth() + 1;
  public year: number = new Date().getFullYear();
  public basePath: string = 'chef/orders';
  public viewMonth: Date;
  public hasOrders: boolean = false;
  private monthlyGridCalculator = new MonthlyGridCalculator();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private chefService: remote.ChefApi,
    private datePipe: DatePipe,
    private foodsService: FoodsService,
    public snackBar: MdSnackBar
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {

      if (params['year'] === undefined && params['month'] === undefined) {
        // Undefined parameters
        this.getMonthlyOrders();
        this.month = new Date().getMonth();
        this.year = new Date().getFullYear();

      } else if (!Number(params['year']) || !Number(params['month'])
        || params['year'].length !== 4 || params['month'].length !== 2
        || params['year'] < 1900 || params['month'] < 1 || params['month'] > 12) {
        // Wrong parameters
        this.router.navigate(['chef/orders']);

      } else if (Number(params['year']) === new Date().getFullYear()
        && Number(params['month']) === (new Date().getMonth() + 1)) {
        // Parameters refer to current month and year
        this.router.navigate(['/chef/orders']);

      } else {

        this.getMonthlyOrdersByMonthYear(params['month'], params['year']);
        this.month = +params['month'] - 1;
        this.year = +params['year'];
      }

      this.viewMonth = new Date(this.year, this.month);

    });
  }

  //Request current's month orders
  getMonthlyOrders() {

    this.chefService.ordersMonthlyGet().subscribe(

      dailyOrdersChef => {

        this.foodsService.setupDailyOrdersChefFoods(dailyOrdersChef)
          .subscribe(
          data => {
            this.updatedDailyOrdersChef = data;
            this.monthlyGrid = this.monthlyGridCalculator.setDaysInGrid(new Date());

          },
          () => this.ready = true);
      },
      error => this.openErrorSnackBar("An error occurred", "ok")
    );

  }

  // Requests the preffered month's orders
  getMonthlyOrdersByMonthYear(month, year) {

    let monthYear = month + '-' + year;

    this.chefService.ordersMonthlyMonthYearGet(monthYear)
      .subscribe(

      dailyOrdersChef => {
        //refactor response data  method
        this.foodsService.setupDailyOrdersChefFoods(dailyOrdersChef)
          .subscribe(
          data => {

            this.updatedDailyOrdersChef = data;
            this.monthlyGrid = this.monthlyGridCalculator.setDaysInGrid(new Date(year, month - 1));
          },
          () => this.ready = true);
      },
      error => this.openErrorSnackBar("An error occurred", "ok")
      );

  }
  getDailyOrders(day) {
    let keyDay = this.datePipe.transform(new Date(day), 'yyyy-MM-dd');
    this.hasOrders = this.updatedDailyOrdersChef.get(keyDay) !== undefined &&
      this.updatedDailyOrdersChef.get(keyDay)['total'] > 0 ? true : false;
    return this.updatedDailyOrdersChef.get(keyDay);
  }

  // Opens an Error SnackBar with the requested message and action
  openErrorSnackBar(message: string, action: string): void {

    this.snackBar.open(message, action, {
      extraClasses: ['error-snack-bar']
    });
  }

}
