import {
  Component,
  OnInit,
  Input } from '@angular/core';

import {
  Router,
  ActivatedRoute
} from '@angular/router';

import { CustomIcons } from '../../../shared/custom-icons/custom-icons';

import { GlobalSettingsService } from '../../../shared/globalSettings.service';

@Component({
  selector: 'app-chef-order-total',
  templateUrl: './order-total.component.html',
  styleUrls: ['./order-total.component.scss']
})
export class OrderTotalComponent implements OnInit {

  @Input()
  public dailyOrderTotal;
  @Input()
  public hasOrders: boolean;
  @Input()
  public date: Date;
  @Input()
  public viewMonth: Date;
  public customIcons: CustomIcons = new CustomIcons();

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    public globalSettingsService: GlobalSettingsService
  ) { }

  ngOnInit() {
  }

  viewDetails() {
    this.router.navigate(['chef/orders', 'day', this.dailyOrderTotal.date]);
  }
}
