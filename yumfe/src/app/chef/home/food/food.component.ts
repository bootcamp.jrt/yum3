import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter
} from '@angular/core';

import {
  MdDialog,
  MdDialogRef,
  MdDialogConfig,
  MdSnackBar
} from '@angular/material';


import { Observable } from 'rxjs/Observable';

import * as remote from '../../../remote';

import { DeleteDialogComponent } from '../../../shared/delete-dialog/delete-dialog.component';
import { GlobalSettingsService } from '../../../shared/globalSettings.service';
import { CustomIcons } from '../../../shared/custom-icons/custom-icons';

import { FoodsService } from '../../foods.service'

import { FoodEditComponent } from '../food-edit/food-edit.component';


@Component({
  selector: 'app-chef-food',
  templateUrl: './food.component.html',
  styleUrls: ['./food.component.scss'],
})
export class FoodComponent implements OnInit {

  private dialogConfiguration = new MdDialogConfig();


  public edit: boolean = false;
  public clone: boolean;
  public foodItemEditable: remote.FoodItemEditable;
  public ready: boolean = false;
  public disableButtons: boolean = false;
  public customIcons: CustomIcons = new CustomIcons();

  @Input()
  public foodItem: remote.FoodItem;
  @Output()
  public needRefresh = new EventEmitter();

  constructor(
    private chefService: remote.ChefApi,
    public dialog: MdDialog,
    public snackBar: MdSnackBar,
    public globalSettingsService: GlobalSettingsService
  ) { }



  ngOnInit() {
    //make the dialog to close only when a user click one of the
    //options
    this.dialogConfiguration.disableClose = true;

  }

  editFood(): void {

    this.edit = true;
    //get food with editable property
    this.chefService.foodsIdGet(this.foodItem.id, true)
      .subscribe(foodItemEditable => {
        this.foodItemEditable = foodItemEditable;
        //if the food can't be fully modified
        // display food edit dialog
        if (!foodItemEditable.editable) {
          this.openDialog();
        }
      },
      error => { },
      () => {
        //if we have an foodEditable object
        //open foodEdit form...
        if (this.foodItemEditable.editable) {
          this.ready = true;
        }
      });

  }

  updateFoodItem(foodItem): void {
    if (foodItem !== undefined && !this.clone) {
      this.foodItem = foodItem;
    } else if (foodItem !== undefined && this.clone) {
      // update the food list and keep the current page size
      this.needRefresh.emit({ fullRefresh: true, keepPageSize: true });
    } else if (!foodItem) {
      this.needRefresh.emit({ fullRefresh: false, keepPageSize: true });
    }
    this.edit = false;
    this.clone = false;
    this.ready = false;
  }

  openDialog(): void {
    let dialogRef = this.dialog.open(EditFoodDialog, this.dialogConfiguration);
    dialogRef.afterClosed().subscribe(result => {
      //if we want to clone the food
      if (result === "clone") {
        this.clone = true;
      } else
        // if we want only to edit the description
        if (result === "editDescr") {
          this.clone = false;
        } else
          // do nothing
          if (result === "cancel") {
            this.edit = false;
            this.clone = false;
          }
      this.ready = true;
    });
  }

  openArchivedDialog(): void {
    let dialogRef = this.dialog.open(ArchivedFoodDialog, this.dialogConfiguration);
    this.disableButtons = true;
    dialogRef.afterClosed().subscribe(result => {
      if (result === "yes") {
        //archive food request
        this.chefService.foodsIdDelete(this.foodItem.id, true)
          .subscribe(result => {
            //update food list with the current selected controls
            this.needRefresh.emit({ fullRefresh: false });
          }, error => { });
      }
      this.disableButtons = false
    });
  }

  openDeleteDialog(foodItem: remote.FoodItem): void {
    this.disableButtons = true;
    let dialogRef = this.dialog.open(DeleteDialogComponent, this.dialogConfiguration);
    dialogRef.afterClosed().subscribe(result => {
      if (result === "yes") {
        this.chefService.foodsIdDelete(foodItem.id)
          .subscribe(result => {
            //request to  refresh the food list
            //without change the controls values
            this.needRefresh.emit({ fullRefresh: false });
          }, error => {
            //if we have food can't deleted error(412)
            // display the archive food dialog
            if (error.status === 412) {
              this.openArchivedDialog();
            } else
              // concurent deletion case
              // the food has been deleted (from other tab or user) and
              // the currunt food list hasn't updated.
              if (error.status === 404) {
                this.snackBar.open("This food item has been deleted.", "ok", {
                  extraClasses: ['error-snack-bar']
                });
                this.needRefresh.emit({ fullRefresh: false });
              }
          });
      }
      this.disableButtons = false
    });
  }

}


@Component({
  selector: 'app-chef-food-dialog',
  templateUrl: './edit-food.dialog.html',
})
export class EditFoodDialog {
  constructor(public dialogRef: MdDialogRef<EditFoodDialog>) { }
}

@Component({
  selector: 'app-chef-food-archive-dialog',
  templateUrl: './archive-food.dialog.html',
})
export class ArchivedFoodDialog {
  constructor(public dialogRef: MdDialogRef<ArchivedFoodDialog>) { }
}
