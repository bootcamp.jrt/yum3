import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { MdSnackBar } from '@angular/material';

import * as remote from '../../remote';

import { FoodsService } from '../foods.service';

@Component({
  selector: 'app-chef-daily-orders',
  templateUrl: './daily-orders.component.html',
  styleUrls: ['./daily-orders.component.scss']
})
export class DailyOrdersComponent implements OnInit {

  public ordersDay: string;
  public title: string;
  public currentDay: string;
  public updatedDailyOrders;
  public complete: boolean = false;
  public isSummaryReady: boolean = false;
  public printMode: boolean = false;

  public userOrders;
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private chefService: remote.ChefApi,
    private datePipe: DatePipe,
    private foodsService: FoodsService,
    public snackBar: MdSnackBar
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.ordersDay = params['day'];

      //date validation pattern for yyyy-mm-dd date format
      let datePattern = /^([12]\d{3}-(0[1-9]|1[0-2])-(0[1-9]|[12]\d|3[01]))$/g;
      //validate route day param.
      if (this.ordersDay === undefined || !datePattern.test(this.ordersDay)) {
        // Wrong parameters
        this.router.navigate(['chef/orders']);
      } else {
        //convert the param day to a valid format
        this.currentDay = this.datePipe.transform(new Date(this.ordersDay), 'yyyy-MM-dd');
        this.title = this.currentDay;
        //get the daily orders for the conveted date.
        this.chefService.ordersDailyDayGet(this.title)
          .subscribe(
          dailyOrders => {
            this.updatedDailyOrders = dailyOrders.summary;
            this.userOrders = dailyOrders.orders;
            //conver's the dailyOrders (response) summary to a more proper
            //structure.
            this.foodsService.getDailyOrdersDetails(dailyOrders.summary)
              .subscribe(
              dailyOrdersUpdated => this.updatedDailyOrders = dailyOrdersUpdated,
              error => this.openErrorSnackBar("An error occurred", "ok"),
              () => this.isSummaryReady = true);

            this.foodsService.getUserOrdersDetails(dailyOrders.orders)
              .subscribe(
              orders => this.userOrders = orders,
              error => this.openErrorSnackBar("An error occurred", "ok"),
              () => this.complete = true
              );
          },
          error => {
            this.openErrorSnackBar(error.json().message, "ok")
            this.router.navigate(['chef/orders']);
          }
          );

      }
    });
  }

  backToOrders() {
    let today = new Date();
    let fromParams = new Date(this.currentDay);
    //if same year month go to default orders route
    if (today.getFullYear() === fromParams.getFullYear() &&
      (today.getMonth()) === fromParams.getMonth()) {
      this.router.navigate(['chef/orders']);
    }
    // if not the same go to the month year from url params
    else {
      let year, month;
      //get the year and month from currentDay
      //using the array destructuring over the splitted currentDay array
      [year, month] = this.currentDay.split('-');
      this.router.navigate(['chef/orders', year, month]);
    }

  }

  print() {
    this.printMode = true;
    let printContents, popupWin;
    printContents = document.getElementById('toPrint').innerHTML;
    popupWin = window.open('', '_blank', 'top=0,left=0,height=100%,width=auto');
    popupWin.document.open();
    popupWin.document.write(`
     <html>
       <head>
         <title>Daily Order Summary</title>
         <style>
         .no-print {
                     display: none;
                   }
         app-chef-food-item, app-chef-user-order {
           display: block;
         }

       div[class*="food-heading"]{
           font-weight: bold;

         }

         div[class*="quantity"]{
           position: relative;
           right: 40px;
         }

         div[class="print-food"]{
           border-bottom: solid 1px;
         }

         div[class="total"]{
           font-weight: bold;
           padding: 10px 5px;
           font-size: 18px;
         }

         div[class="print-users"]{
           border-top: solid 3px;
           padding: 15px 0px;
         }

         div[class*="print-user"]{
           border-top: solid 1px;
         }


         </style>
       </head>
   <body onload="window.print(); window.close();">${printContents}</body>
     </html>`
    );
    popupWin.document.close();
    this.printMode = false;
  }

  openErrorSnackBar(message: string, action: string): void {
    this.snackBar.open(message, action, {
      extraClasses: ['error-snack-bar']
    });
  }
}
