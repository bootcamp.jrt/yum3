import {
  Component,
  OnInit,
  Input
} from '@angular/core';

import * as remote from '../../../remote';

import { GlobalSettingsService } from '../../../shared/globalSettings.service';

@Component({
  selector: 'app-chef-user-order',
  templateUrl: './user-order.component.html',
  styleUrls: ['./user-order.component.scss']
})
export class UserOrderComponent implements OnInit {

  @Input()
  public userOrders: Array<remote.DailyOrder>;
  constructor(public globalSettingsService: GlobalSettingsService) { }

  ngOnInit() {
  }

  getMainFoodsOrdersDetails(index) {

    for (let food of this.userOrders[index].foods) {

      if (food.foodType === "MAIN") {
        return food.orderItems;
      }
    }
    return undefined;

  }

  getSaladFoodsOrdersDetails(index) {

    for (let food of this.userOrders[index].foods) {

      if (food.foodType === "SALAD") {
        return food.orderItems;
      }
    }
    return undefined;
  }

  getDrinksOrdersDetails(index) {

    for (let food of this.userOrders[index].foods) {

      if (food.foodType === "DRINK") {
        return food.orderItems;
      }
    }
    return undefined;
  }

}
