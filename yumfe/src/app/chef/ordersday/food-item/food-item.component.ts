import {
  Component,
  OnInit,
  Input
} from '@angular/core';

import * as remote from '../../../remote';

import { GlobalSettingsService } from '../../../shared/globalSettings.service';

import { FoodsService } from '../../foods.service';

@Component({
  selector: 'app-chef-food-item',
  templateUrl: './food-item.component.html',
  styleUrls: ['./food-item.component.scss']
})
export class FoodItemComponent implements OnInit {

  public foodTypes = {
    "MAIN": "Main dishes",
    "SALAD": "Salads",
    "DRINK": "Drinks"
  }

  @Input()
  updatedDailyOrders;

  constructor(
    private foodsService: FoodsService,
    public globalSettingsService: GlobalSettingsService
  ) { }

  ngOnInit() {
  }

  getMainFoodsOrdersDetails() {
    let mainFoodsDetails = [];
    let mainFoodsSummaryItem;
    for (let summaryItem of this.updatedDailyOrders.summary) {

      if (summaryItem.foodType === "MAIN") {
        mainFoodsSummaryItem = summaryItem;
        break;
      }
    }

    for (let orderItem of mainFoodsSummaryItem.orderItems) {
      mainFoodsDetails.push({
        'foodItem': orderItem.foodItem,
        'quantity': orderItem.quantity
      });
    }
    return mainFoodsDetails;

  }

  getSaladFoodsOrdersDetails() {
    let saladFoodsDetails = [];
    let saladFoodsSummaryItem;
    for (let summaryItem of this.updatedDailyOrders.summary) {

      if (summaryItem.foodType === "SALAD") {
        saladFoodsSummaryItem = summaryItem;
        break;
      }
    }

    for (let orderItem of saladFoodsSummaryItem.orderItems) {
      saladFoodsDetails.push({
        'foodItem': orderItem.foodItem,
        'quantity': orderItem.quantity
      });
    }
    return saladFoodsDetails;
  }

  getDrinksOrdersDetails() {
    let drinksDetails = [];
    let drinksSummaryItem;
    for (let summaryItem of this.updatedDailyOrders.summary) {

      if (summaryItem.foodType === "DRINK") {
        drinksSummaryItem = summaryItem;
        break;
      }
    }

    for (let orderItem of drinksSummaryItem.orderItems) {
      drinksDetails.push({
        'foodItem': orderItem.foodItem,
        'quantity': orderItem.quantity
      });
    }
    return drinksDetails;
  }

}
