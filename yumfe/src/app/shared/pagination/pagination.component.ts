import {
  Component,
  OnInit,
  Input,
  Output,
  EventEmitter
} from '@angular/core';

@Component({
  selector: 'app-shared-pagination',
  templateUrl: './pagination.component.html',
  styleUrls: ['./pagination.component.scss']
})
export class PaginationComponent implements OnInit {

  @Input()
  pageSize: number;
  @Input()
  totalPages: number;
  @Input()
  currentPage: number;
  @Output()
  updatePage = new EventEmitter();
  pages: Array<number>;

  constructor() { }

  ngOnInit() {
    let startPage: number, endPage: number;
    if (this.totalPages <= 5) {
      // less than 10 total pages so show all
      startPage = 1;
      endPage = this.totalPages;
    } else {
      // more than 10 total pages so calculate start and end pages
      if (this.currentPage <= 3) {
        startPage = 1;
        endPage = 5;
      } else if (this.currentPage + 2 >= this.totalPages) {
        startPage = this.totalPages - 4;
        endPage = this.totalPages;
      } else {
        startPage = this.currentPage - 2;
        endPage = this.currentPage + 2;
      }
    }

    // calculate start and end item indexes
    let startIndex = (this.currentPage - 1) * this.pageSize;
    let endIndex = Math.min(startIndex + this.pageSize - 1, this.pageSize * 10 - 1);

    // create an array of pages to ng-repeat in the pager control
    // let pages = _.range(startPage, endPage + 1);
    let pagesLength: number = startPage + endPage - 1;
    pagesLength = pagesLength > 5 ? 5 : pagesLength;
    this.pages = Array.from({ length: pagesLength }, (v, i) => i + startPage);

  }

  changePage(page) {
    this.currentPage = page;
    this.updatePage.emit(this.currentPage);
  }

}
