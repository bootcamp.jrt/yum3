import { Injectable, Inject } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable, BehaviorSubject } from 'rxjs/Rx';
import { BASE_PATH } from '../remote/variables';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';

import * as remote from '../remote';

const CURRENTUSER: string = 'currentUser3';
class DisplayedUser {
  public user?: remote.LoggedUser;
  public picUrl?: string;
}
@Injectable()
export class AuthenticationService {
  private displayedUser: DisplayedUser = {};
  private loggedUserSubject = new BehaviorSubject<DisplayedUser>({user:{}});
  public picUrl = '';
  public basePicUrl: string;

  private loggedInUser: remote.LoggedUser;

  constructor(private authService: remote.AuthApi,
    private conf: remote.Configuration,
    private hungryService: remote.HungryApi,
    @Inject(BASE_PATH) private basePath: string) { }

  login(username: string, password: string): Observable<boolean> {
    let creds: remote.Login = { email: username, password: password };
    return this.authService.authLoginPost(creds).map(loggedUser => {
      // login successful if there's a jwt token in the response
      if (loggedUser.token) {
        this.loggedInUser = loggedUser;
        this.prepareDisplayedUser();
        this.emitSubject();
        // store the logged in user in local storage to keep user logged in between page refreshes
        this.persistUser(loggedUser);
        this.conf.apiKey = "Bearer " + loggedUser.token;
        // return true to indicate successful login
        return true;
      } else {
        // return false to indicate failed login
        return false;
      }
    }).catch((error: any) => {

      return Observable.throw(error.json());

    });
  }

  public getLoggedUserObservable(): Observable<DisplayedUser> {
    return this.loggedUserSubject.asObservable();
  }
  private completeGetUrl(): string {
    return this.basePicUrl + "/token?token=" + this.loggedInUser.token + "&ts=" + (new Date().getTime());
  }

  public prepareUrls() {
    this.basePicUrl = this.basePath + '/settings/picture';
    this.picUrl = this.completeGetUrl();

  }
  public refreshImg() {
    this.picUrl = this.completeGetUrl();
  }
  private getPersistedUser(): remote.LoggedUser {
    return JSON.parse(localStorage.getItem(CURRENTUSER));
  }

  private persistUser(loggedUser: remote.LoggedUser) {
    localStorage.setItem(CURRENTUSER, JSON.stringify(loggedUser));
  }

  updateUserDetails(firstName: string, lastName: string) {
    this.loggedInUser.firstName = firstName;
    this.loggedInUser.lastName = lastName;
    this.persistUser(this.loggedInUser);
    this.displayedUser.user = this.loggedInUser;
    this.loggedUserSubject.next(this.displayedUser);
  }

  updateHasPicture(hasPic: boolean) {
    this.loggedInUser.hasPicture = hasPic;
    this.persistUser(this.loggedInUser);
    this.refreshImg();
    this.displayedUser.picUrl = this.picUrl;
    this.displayedUser.user = this.loggedInUser;
    this.loggedUserSubject.next(this.displayedUser);

  }

  isLogged(): boolean {
    let ls = localStorage.getItem(CURRENTUSER);
    if (ls === null) {
      this.loggedInUser = undefined;
      this.conf.apiKey = undefined;
      return false;
    }
    this.loggedInUser = JSON.parse(ls);
    return ((this.loggedInUser !== undefined) && this.loggedInUser.token !== undefined);
  }

  prepareDisplayedUser() {
    this.prepareUrls();
    this.displayedUser.user = this.loggedInUser;
    this.displayedUser.picUrl = this.picUrl;

  }

  emitSubject() {
    this.loggedUserSubject.next(this.displayedUser);
  }

  getLoggedInUser() {
    let isLogged = this.isLogged();
    if (isLogged) this.prepareDisplayedUser();
    return isLogged ? this.loggedInUser : undefined;
  }

  bootstrapUser(): void {
    let currentUser: remote.LoggedUser = this.getPersistedUser();

    if (currentUser === null) return;

    let token = currentUser.token;
    if (token && token !== "") {
      this.conf.apiKey = "Bearer " + token;
    }

    this.loggedInUser = currentUser;

    // API call to get lastest user details
    this.hungryService.settingsGet().subscribe(settings => {
      this.loggedInUser.firstName = settings.firstName;
      this.loggedInUser.lastName = settings.lastName;
      this.loggedInUser.role = settings.role;
      this.loggedInUser.hasPicture = settings.hasPicture;
      // save it in currentUser
      this.persistUser(this.loggedInUser);
      this.prepareDisplayedUser();
      this.emitSubject();
    },
      error => {
      }
    );

  }

  getLoggedInRole(): String {
    if (!this.isLogged()) return null;
    return this.loggedInUser ? this.loggedInUser.role.toLowerCase() : null;
  }


  logout(): void {
    // clear token remove user from local storage to log user out
    localStorage.removeItem(CURRENTUSER);
    this.conf.apiKey = undefined;
    this.loggedInUser = undefined;
  }
}
