export class CustomIcons {

  getCustomIcon(type: string): string {

    switch (type) {
      case 'MAIN':
        return 'icon-Meal';
      case 'SALAD':
        return 'icon-Salad';
      case 'DRINK':
        return 'icon-Drink';
      case 'LOGOUT':
        return 'icon-Logo-out';
      default:
        return '';
    }

  }

}
