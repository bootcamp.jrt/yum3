import {
  Component,
  OnInit
} from '@angular/core';
import {
  MdDialog,
  MdDialogRef,
  MdDialogConfig,
} from '@angular/material';

@Component({
  templateUrl: './tos-dialog.component.html',
  styleUrls: ['./tos-dialog.component.scss']
})
export class TosDialogComponent implements OnInit {

  public terms: string;
  public policy: string;
  public selectedIndex = 0;

  constructor(public dialogRef: MdDialogRef<TosDialogComponent>) { }

  ngOnInit() {
  }


}
