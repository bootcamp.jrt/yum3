import { Component } from '@angular/core';

import { MdDialogRef } from '@angular/material';

@Component({
  selector: 'app-shared-delete-dialog',
  templateUrl: './delete-dialog.component.html',
  styleUrls: ['./delete-dialog.component.scss']
})
export class DeleteDialogComponent {

  constructor(public dialogRef: MdDialogRef<DeleteDialogComponent>) { }

}
