import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormGroup, FormControl, Validators, ReactiveFormsModule } from '@angular/forms';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import * as remote from '../../remote';
import { AuthenticationService } from '../../shared/authentication.service';
import { MdCardModule } from '@angular/material';

@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.scss']
})
export class SettingsComponent implements OnInit {
  // flags
  loadingData = false;
  isProfileValid = true;
  hasProfileChanged = false;

  selectedOption: any;
  settingsForm: FormGroup;
  error: any;
  userJSON = '';
  user: remote.User = {};
  userVersion;
  concurrent: boolean = false;
  accountSettings: remote.AccountSettings = {};
  disabled: boolean = false;
  showSpinner = false;




  constructor(
    private hungryService: remote.HungryApi,
    public snackBar: MdSnackBar,
    private router: Router,
    private authService: AuthenticationService,
    public dialog: MdDialog) { }

  ngOnInit() {
    this.loadingData = true;
    this.update();

    this.settingsForm = new FormGroup({
      pwd1: new FormControl('', Validators.compose([
        Validators.minLength(6),
        Validators.maxLength(25)
      ])),
      pwd2: new FormControl('', Validators.compose([
        Validators.minLength(6),
        Validators.maxLength(25)
      ]))
    });

  }


  updateAccountSettings() {
    this.showSpinner = true;

    let value = this.settingsForm.value;

    if (value.pwd1 !== value.pwd2) {
      this.openErrorSnackBar("Passwords do not match!");
      this.showSpinner = false;
    }else if(value.pwd1 === value.pwd2 && value.pwd1.length > 25){
      this.openErrorSnackBar("Password Length is invalid!");
    }
     else {
      let accountDetails: remote.AccountDetails = {
        firstName: this.accountSettings.firstName,
        lastName: this.accountSettings.lastName,
        email: this.accountSettings.email,
        password: value.pwd1,
        version: this.accountSettings.version
      };
      return this.hungryService.settingsPut(accountDetails)
        .subscribe(() => {
          //persist new detals
          this.authService.updateUserDetails(accountDetails.firstName, accountDetails.lastName);
          this.settingsForm.setValue({pwd1: '', pwd2: ''});
          this.settingsForm.markAsPristine();
          this.accountSettings.version++;
          this.hasProfileChanged = false;
          this.showSpinner = false;
          this.openSuccessSnackBar('Settings Successfully updated!', 'ok');
        }, error => {
          if (error.status === 409) { //concurrent modification
            //reseting values of user
            this.openRefreshingDataDialog(error.json());
            this.showSpinner = false;
          }else{
            this.showSpinner = false;
            this.openErrorSnackBar('There was an error in: ' + error.json().dto + ' input.');
          }
        });
    }
  }

  checkValid(state) {
    this.isProfileValid = state;
    this.hasProfileChanged = true;

  }

  home() {
    this.router.navigate(['/']);
  }

  update() {
    this.loadingData = true;
    this.hungryService.settingsGet().subscribe(settings => {
      this.userVersion = settings.version;
      this.user.role = settings.role;
      this.accountSettings = settings;
      this.loadingData = false;
      this.authService.updateHasPicture(settings.hasPicture);
    },
      error => {
        this.loadingData = false;
        this.openErrorSnackBar('An error occured!');
         this.home();
      }
    );
  }

  public updateUserVersion() {
   this.hungryService.settingsGet().subscribe(settings => {
      this.userVersion = settings.version;
      this.user.role = settings.role;
      this.accountSettings = settings;

      this.authService.updateHasPicture(settings.hasPicture);
    },
      error => {
        this.openErrorSnackBar('An error occured!');
        this.home();
      }
    );
  }

  getRole(role:string): string {
    if (role === 'ADMIN') {
      return 'You are an Admin';
    } else if (role === 'CHEF') {
      return 'You are a Chef';
    } else if (role === 'HUNGRY') {
      return 'You are a Hungry';
    }
  }

  // Opens a Success SnackBar with the requested message and action
  openSuccessSnackBar(message: string, action: string): void {
    this.snackBar.open(message, action, {
      extraClasses: ['success-snack-bar'],
      duration: 5000
    });
  }

  public openErrorSnackBar(message: string) {
    this.snackBar.open(message, 'ok', {
      extraClasses: ['error-snack-bar'],
      duration: 5000
    });

  }

  //asking if the user wants to updates his data or not
  public openRefreshingDataDialog(errorDTO: any) {
    let dialogRef = this.dialog.open(RefreshingDataDialog);
    dialogRef.afterClosed().subscribe(result => {
      if (result === 'yes') {
        this.loadingData = true;
        this.userVersion = errorDTO.version;
        this.user.role = errorDTO.role;
        this.accountSettings = errorDTO;
        this.loadingData = false;
        this.authService.updateHasPicture(errorDTO.hasPicture);
      } else {
        this.openErrorSnackBar('Your data has not been updated! You will not be able to save the changes');
      }
    });

  }

}

@Component({
  selector: 'app-refreshing-data-dialog',
  templateUrl: './refreshing-data-dialog.html',
})
export class RefreshingDataDialog {
  constructor(public dialogRef: MdDialogRef<RefreshingDataDialog>) { }
}


