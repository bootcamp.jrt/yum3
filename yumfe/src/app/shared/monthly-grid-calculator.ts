export class MonthlyGridCalculator {

  // Returns the the first day (Monday) of the given week-year
  setDaysInGrid(date: Date): Array<Date> {

    let firstDay = new Date(date.getFullYear(), date.getMonth(), 1);

    // Set first day to the corresponding Monday
    if (firstDay.getDay() === 6) {
      // If first day is Saturday hop to the following Monday
      firstDay.setDate(firstDay.getDate() + 2);
    } else if (firstDay.getDay() === 0) {
      // If first day is Sunday hop to the following Monday
      firstDay.setDate(firstDay.getDate() + 1);
    } else {
      // If first day is Monday to Friday hop to the previous Monday
      firstDay.setDate(firstDay.getDate() - (firstDay.getDay() - 1));
    }

    let lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);

    let currentWeek;
    let currentMonday;
    let i = 0;
    let daysInGrid = new Array<Date>();

    do {

      // Calculate current week's number
      currentWeek = this.getWeek(new Date(firstDay.getFullYear(), firstDay.getMonth(), firstDay.getDate() + (7 * i)));

      // Calculate the Monday for the current week
      currentMonday = new Date(firstDay.getFullYear(), firstDay.getMonth(), firstDay.getDate() + (7 * i));

      // Loop through Monday to Friday of the current week
      for (let j = 0; j < 5; j++) {
        let currentDay = new Date(currentMonday.getFullYear(), currentMonday.getMonth(), currentMonday.getDate() + j);
        currentDay.setHours(0);
        daysInGrid.push(currentDay);
      }

      i++;

    } while (currentWeek !== this.getWeek(lastDay));

    return daysInGrid;

  }

  // Returns the ISO week of the date.
  getWeek(dt: Date): number {

    let date = new Date(dt.getTime());
    date.setHours(0, 0, 0, 0);
    // Thursday in current week decides the year.
    date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
    // January 4 is always in week 1.
    let week1 = new Date(date.getFullYear(), 0, 4);
    // Adjust to Thursday in week 1 and count number of weeks from date to week1.
    return 1 + Math.round(((date.getTime() - week1.getTime()) / 86400000
      - 3 + (week1.getDay() + 6) % 7) / 7);

  };

}