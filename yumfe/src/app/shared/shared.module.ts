import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotFoundComponent } from './not-found/not-found.component';
import { LoggedComponent } from './logged/logged.component';
import { ProfileComponent } from './profile/profile.component';
import { ChefRoutingComponent } from '../chef/shared/chef-routing/chef-routing.component';
import { RouterModule } from '@angular/router';
import { AuthenticationService } from './authentication.service';
import { GlobalSettingsService } from './globalSettings.service';
import { Configuration } from '../remote/configuration';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { FileUploadModule } from 'ng2-file-upload';
import {
  MdCheckboxModule,
  MdInputModule,
  MdRadioModule,
  MdSelectModule,
  MdSlideToggleModule,
  MdMenuModule,
  MdToolbarModule,
  MdListModule,
  MdGridListModule,
  MdCardModule,
  MdButtonModule,
  MdButtonToggleModule,
  MdIconModule,
  MdProgressSpinnerModule,
  MdDialogModule,
  MdTooltipModule,
  MdSnackBarModule,
  MdTabsModule
} from '@angular/material';
import { PaginationComponent } from './pagination/pagination.component';
import { DeleteDialogComponent } from './delete-dialog/delete-dialog.component';
import { ImageDeletionDialog } from './profile/profile.component';
import { MonthlyPaginationComponent } from './monthly-pagination/monthly-pagination.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { TosDialogComponent } from './tos-dialog/tos-dialog.component';
import { TosService } from './tos.service';
import { SettingsComponent, RefreshingDataDialog } from '../shared/settings/settings.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FlexLayoutModule,
    MdGridListModule,
    MdButtonModule,
    FormsModule,
    ReactiveFormsModule,
    MdMenuModule,
    FileUploadModule,
    MdTabsModule,
    MdCardModule,
    MdInputModule,
    MdSelectModule,
    MdProgressSpinnerModule,
    MdTooltipModule
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    MdCheckboxModule,
    MdInputModule,
    MdRadioModule,
    MdSelectModule,
    MdSlideToggleModule,
    MdMenuModule,
    MdToolbarModule,
    MdListModule,
    MdGridListModule,
    MdCardModule,
    MdButtonModule,
    MdButtonToggleModule,
    MdIconModule,
    MdProgressSpinnerModule,
    MdDialogModule,
    MdTooltipModule,
    MdSnackBarModule,
    ChefRoutingComponent,
    FlexLayoutModule,
    PaginationComponent,
    DeleteDialogComponent,
    ProfileComponent,
    MonthlyPaginationComponent,
    FileUploadModule,
    MdTabsModule,
    SettingsComponent

  ],
  declarations: [
    NotFoundComponent,
    LoggedComponent,
    ChefRoutingComponent,
    PaginationComponent,
    DeleteDialogComponent,
    ProfileComponent,
    MonthlyPaginationComponent,
    HeaderComponent,
    FooterComponent,
    TosDialogComponent,
    SettingsComponent,
    ImageDeletionDialog,
    RefreshingDataDialog

  ],
  providers: [
    GlobalSettingsService,
    AuthenticationService,
    Configuration,
    TosService

  ],
  entryComponents: [
    DeleteDialogComponent,
    TosDialogComponent,
    ImageDeletionDialog,
    RefreshingDataDialog
  ]
})
export class SharedModule { }
