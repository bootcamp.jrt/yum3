import { Injectable } from '@angular/core';
import {
  MdDialog,
  MdDialogRef,
  MdDialogConfig
} from '@angular/material';

// import { Observable } from 'rxjs/Observable';

import * as remote from '../remote';
import { TosDialogComponent } from './tos-dialog/tos-dialog.component'

@Injectable()
export class TosService {

  constructor(
    public dialog: MdDialog,
    private authService: remote.AuthApi
  ) { }

  //if the flag === true make terms tab selected,
  //else the policy table from tos-dialog
  displayTos(flag: boolean) {
    this.authService.authTermsGet()
      .subscribe(
      termsAndPolicy => {

        let dialogRef = this.dialog.open(TosDialogComponent);
        // add the terms and policy to dialog component,
        //set the selected tab
        dialogRef.componentInstance.terms = termsAndPolicy.terms;
        dialogRef.componentInstance.policy = termsAndPolicy.policy;
        dialogRef.componentInstance.selectedIndex = flag === true ? 0 : 1;
      }
      )

  }

}
