import { Component, OnInit, Inject } from '@angular/core';
import { AuthenticationService } from '../../shared/authentication.service';
import { LoggedUser } from '../../remote/model/LoggedUser'
import { Observable } from 'rxjs/Rx';
@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  public user: LoggedUser;
  public obs: Observable<any>;

  constructor(
    public authenticationService: AuthenticationService
  ) { }

  ngOnInit() {

    this.user = this.authenticationService.getLoggedInUser();
    this.obs = this.authenticationService.getLoggedUserObservable();
  }

  public logout() {
    this.authenticationService.logout();
  }

}
