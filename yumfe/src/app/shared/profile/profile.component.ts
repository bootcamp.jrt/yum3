import { Component, OnInit, Input, Output, SimpleChanges, OnChanges, EventEmitter, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthenticationService } from '../authentication.service';
import * as remote from '../../remote';
import { ActivatedRoute } from '@angular/router';
import { FileUploader, FileSelectDirective, FileUploaderOptions } from 'ng2-file-upload';
import { BASE_PATH } from '../../remote/variables';
import { MdSnackBar, MdDialog, MdDialogRef } from '@angular/material';
import { CustomIcons } from '../custom-icons/custom-icons';


@Component({
  selector: 'app-shared-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  // flag to indicate that a request for setting the profile picture is on progress
  settingPic = false;
  // flag to toggle uploader
  showUploader = false;

  userRoles = [
    {value: 'HUNGRY',  viewValue: 'Hungry'},
    {value: 'CHEF', viewValue: 'Chef'  },
    {value:'ADMIN', viewValue: 'Admin'}
    ];
  picUrl = '';
  basePicUrl: string;
  uploader: FileUploader;
  options: FileUploaderOptions = {};
  userForm: FormGroup;
  _user: remote.AccountSettings;

  @Input('user')
  set user(newUser: remote.AccountSettings) {
    if (this._user && this._user.version != newUser.version)
      this.refreshImg();

    this.settingPic = false;
    this._user = newUser;
  }

  get user(): remote.AccountSettings {
    return this._user;
  }

  @Input()
  showRoleOptions: number;

  @Output()
  userChange: EventEmitter<remote.AccountSettings> = new EventEmitter<remote.AccountSettings>();

  @Output()
  isValid: EventEmitter<boolean> = new EventEmitter<boolean>();

  @Output()
  picChanged: EventEmitter<boolean> = new EventEmitter();

  @Output()
  active: EventEmitter<boolean> = new EventEmitter();

  constructor(private authenticationService: AuthenticationService,
    private route: ActivatedRoute,
    @Inject(BASE_PATH) private basePath: string,
    private snackBar: MdSnackBar,
    private adminService: remote.AdminApi,
    private hungryService: remote.HungryApi,
    private dialog: MdDialog) { }

  ngOnInit() {
    // create FormGroup object
    this.userForm = new FormGroup({
      firstName: new FormControl(this.user.firstName, Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z  .-]+$'),
        Validators.minLength(2),
        Validators.maxLength(25)
      ])),
      lastName: new FormControl(this.user.lastName, Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z  .-]+$'),
        Validators.minLength(2),
        Validators.maxLength(25)
      ])),
      email: new FormControl(this.user.email, Validators.compose([
        Validators.required,
        Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
      ])),
      role: new FormControl(this.user.role, Validators.compose([
        Validators.required,
      ]))
    });
    // prepare urls
    let authToken = "Bearer " + this.authenticationService.getLoggedInUser().token;
    this.prepareUrls();

    // create uploader and define callbacks
    this.uploader = new FileUploader({ url: this.basePicUrl, authToken: authToken, autoUpload: true, removeAfterUpload: true });
    this.uploader.onAfterAddingFile = () => {
      this.settingPic = true;
      this.active.emit(true);
    };
    this.uploader.onCompleteAll = () => {
      //this.refreshImg();
      this.showUploader = false;
      this.active.emit(false);
    };
    this.uploader.onErrorItem = () => {
      this.settingPic = false;
      this.openErrorSnackBar('An unexpected error occured, please try again later.');
    }
    this.uploader.onSuccessItem = () => {
      this.openSuccessSnackBar('Profile picture was set succesfully.');
      this.picChanged.emit();
    }

  }
  public refreshImg() {
    this.picUrl = this.completeGetUrl();
  }

  public deleteImg() {
    this.settingPic = true;
    if (this.showRoleOptions) {
      this.adminService.usersIdPictureDelete(this.showRoleOptions).subscribe(res => {
        this.openSuccessSnackBar('Profile picture was deleted');
        this.active.emit(false);
        this.settingPic = false;
        this.picChanged.emit();
      }, error => {
        this.openErrorSnackBar('An unexpected error occured, please try again later.');
        this.settingPic = false;
        this.active.emit(false);
      });

    } else {
      this.hungryService.settingsPictureDelete().subscribe(res => {
        this.openSuccessSnackBar('Profile picture was deleted');
        this.active.emit(false);
        this.settingPic = false;
        this.picChanged.emit();
      }, error => {
        this.openErrorSnackBar('An unexpected error occured, please try again later.');
         this.active.emit(false);
        this.settingPic = false;
      });
    }
  }

  public inputChanged() {
    if (this.userForm.dirty) {
      this.userChange.emit(this.user);
      this.isValid.emit(this.userForm.valid);
    }
  }
  private completeGetUrl(): string {
    return this.basePicUrl + "/token?token=" + this.authenticationService.getLoggedInUser().token + "&ts=" + (new Date().getTime());
  }

  public prepareUrls() {

    if (this.showRoleOptions) {

      this.basePicUrl = this.basePath + '/users/' + this.showRoleOptions + '/picture';

    } else {
      this.basePicUrl = this.basePath + '/settings/picture';

    }
    this.picUrl = this.completeGetUrl();

  }

  public toggleUploader() {
    if (!this.showUploader) {
      this.showUploader = true;
    } else {
      this.showUploader = false;
    }
  }

  public openSuccessSnackBar(message: string) {
    this.snackBar.open(message, 'ok', {
      extraClasses: ['success-snack-bar'],
      duration: 5000
    });

  }
  public openErrorSnackBar(message: string) {
    this.snackBar.open(message, 'ok', {
      extraClasses: ['error-snack-bar'],
      duration: 5000
    });

  }

  public openDeleteDialog() {
    this.settingPic = true;
    this.active.emit(true);
    let dialogRef = this.dialog.open(ImageDeletionDialog);
    dialogRef.afterClosed().subscribe(result => {
      if (result === 'ok') {
        this.deleteImg();
      }else {
         this.settingPic = false;
         this.active.emit(false);
      }
    });

  }

}
@Component({
  selector: 'app-image-deletion-dialog',
  templateUrl: './image-deletion.dialog.html',
})
export class ImageDeletionDialog {
  constructor(public dialogRef: MdDialogRef<ImageDeletionDialog>) { }
}
