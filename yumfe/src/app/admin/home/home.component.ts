import { Component, OnInit, Output } from '@angular/core';
import * as remote from '../../remote';
import { UserComponent } from './user/user.component';
import { PaginationComponent } from '../../shared/pagination/pagination.component';
import { Router } from '@angular/router';
import { MdSnackBar } from '@angular/material';
import { TosService } from '../../shared/tos.service';

import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-admin-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  // flags
  public ready = false;
  public submitting = false;
  public blurred = false;
  public disableSubmit = true;
  // select input options
  public pageOptions: number[] = [10, 20, 50, 100];
  public orderByOptionsVal: string[] = ['regdate', 'name', 'role', 'status'];
  public orderByOptions: string[] = ['Registration date', 'User name', 'Role', 'Approval status'];

  public users: Array<remote.User>;
  public userForm: FormGroup;

  public totalPages: number;
  public currentPage = 1;
  public selectedPageSize = 10;
  public orderby = 'regdate';
  public sort = 'desc';
  constructor(private router: Router,
    private adminService: remote.AdminApi,
    private snackBar: MdSnackBar,
    public tosService: TosService) { }

  ngOnInit() {
    this.updatePage();

    this.initForm();

  }
  public updatePage() {
    this.ready = false;
    this.adminService.usersGet(this.sort, this.currentPage - 1, this.selectedPageSize, this.orderby).subscribe(users => {
      this.users = users.users;
      this.totalPages = users.totalPagesNumber;

    }, error =>{ this.openErrorSnackBar('An error occured.');},
      () => { this.ready = true; });

  }

  public onSubmit() {
    this.submitting = true;
    this.adminService.usersPost(this.userForm.value).subscribe(res => {
      this.submitting = false;
      this.openSuccessSnackBar('User created');
      this.currentPage = 1;
      this.selectedPageSize = 10;
      this.orderby = 'regdate';
      this.sort = 'desc';
      this.userForm.get('role').setValue('HUNGRY');
      this.initForm();
      this.updatePage();
    }, error => {
      this.submitting = false;
      if (error.status === 412) {
        this.openErrorSnackBar('A user with the given e-mail already exists');
      } else {
        this.openErrorSnackBar('An unexpected error occured, please try again later');
      }
    });
  }
  // Method to toggle the disabled property of password confirmation input
  public toggleConfirmDisabled() {
    if (this.userForm.get('password').valid) {
      this.userForm.get('confirmPassword').enable();
    } else {
      this.disableSubmit = true;
      this.userForm.get('confirmPassword').setValue(null);
      this.userForm.get('confirmPassword').disable();
      this.userForm.get('confirmPassword').reset();
    }
  }
  // Custom validator of password confirmation
  public validateEqual(c: FormControl) {
    let mainPassword: string = c.parent.get('password').value;
    return (mainPassword === (c.value)) ? null : {
      validateEqual: {
        valid: false
      }
    };
  }
  // Custom validator of password confirmation
  public validateStart(c: FormControl) {
    let mainPassword: string = c.parent.get('password').value;
    return (mainPassword.startsWith(c.value)) ? null : {
      validateStart: {
        valid: false
      }
    };

  }
  // Method to reset the touched property of password confirmation input
  public resetTouched() {
    let c = this.userForm.get('confirmPassword');
    c.markAsUntouched();

  }

  // Handler for pagination component event
  public pageChanged(page: number) {
    this.currentPage = page;
    this.updatePage();
  }
  // Handler for page size select event
  public changePageSize(event) {
    this.selectedPageSize = event.value;
    this.updatePage();
  }
  // Handler for orderBy select event
  public changeOrder(event) {
    this.orderby = event.value;
    this.updatePage();
  }
  // Handler for sorting icons event (descending)
  public changeSortingDesc() {
    this.sort = 'desc';
    this.updatePage();
  }
  // Handler for sorting icons event (ascending)
  public changeSortingAsc() {
    this.sort = 'asc';
    this.updatePage();
  }

  public displayTermsAndPolicy(flag) {
    this.tosService.displayTos(flag);
  }

  public navigateToGlobalSetting() {
    this.router.navigate(['admin/settings']);
  }

  public openSuccessSnackBar(message: string) {
    this.snackBar.open(message, 'ok', {
      extraClasses: ['success-snack-bar'],
      duration: 5000
    });

  }
  public openErrorSnackBar(message: string) {
    this.snackBar.open(message, 'ok', {
      extraClasses: ['error-snack-bar'],
      duration: 5000
    });

  }

  public initForm() {
    // create FormGroup object
    this.userForm = new FormGroup({
      firstName: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z  .-]+$'),
        Validators.minLength(2),
        Validators.maxLength(25)
      ])),
      lastName: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z  .-]+$'),
        Validators.minLength(2),
        Validators.maxLength(25)
      ])),
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
      ])),
      password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(25)
      ])),
      confirmPassword: new FormControl({ value: null, disabled: true }, Validators.compose([
        Validators.required,
        this.validateStart,
        this.validateEqual
      ])),
      role: new FormControl('HUNGRY', Validators.compose([
        Validators.required
      ]))
    });

    this.userForm.get('confirmPassword').valueChanges.subscribe(value => {
      if (this.userForm.get('confirmPassword').value !== null) {
        this.userForm.get('confirmPassword').markAsTouched();
        this.disableSubmit = false;
      }
    });

    this.userForm.get('password').valueChanges.subscribe(value => {
      this.toggleConfirmDisabled();
    });
  }
}
