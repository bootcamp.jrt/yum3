import { Component, OnInit, Input, Output, Inject, EventEmitter } from '@angular/core';
import * as remote from '../../../remote';
import { Router } from '@angular/router';
import { AuthenticationService } from '../../../shared/authentication.service';
import { MdDialog, MdDialogRef, MdSnackBar } from '@angular/material';
import { BASE_PATH } from '../../../remote/variables';

@Component({
  selector: 'app-admin-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {
  // flags
  public notRemovable = false;
  public notEditable = false;
  public removing = false;

  private picUrlStart = "/users/";
  private picUrlEnd = "/picture/token?token=";
  profileRoute: (string | number)[];

  public picUrl: string;

  @Input() user: remote.User;

  @Output() userDeleted: EventEmitter<boolean> = new EventEmitter();

  constructor(private router: Router,
    private authService: remote.AuthApi,
    private authenticationService: AuthenticationService,
    @Inject(BASE_PATH) private basePath: string,
    private adminService: remote.AdminApi,
    private dialog: MdDialog,
    private snackBar: MdSnackBar) { }

  ngOnInit() {
    if (this.user.id === 1 || this.user.id === this.authenticationService.getLoggedInUser().id) {
      this.notRemovable = true;
    }
    if (this.user.id === this.authenticationService.getLoggedInUser().id) {
      this.profileRoute = ['/settings'];
    } else {
      this.profileRoute = ['/admin/users', this.user.id];
    }
    if (this.user.id === 1 && this.authenticationService.getLoggedInUser().id !== 1) {
      this.notEditable = true;
    }
    this.picUrlEnd += this.authenticationService.getLoggedInUser().token;
    this.picUrlStart = this.basePath + this.picUrlStart;
    this.renewPicUrl();
  }

  public renewPicUrl() {
    this.picUrl = this.picUrlStart + this.user.id + this.picUrlEnd + "&ts=" + (new Date().getTime());
  }

  public routeToUserSettings() {
    this.router.navigate(['admin/users', this.user.id]);
  }

  public removeUser() {
    this.removing = true;
    this.adminService.usersIdDelete(this.user.id).subscribe(success => {
      this.removing = false;
      this.userDeleted.emit();
      this.openSuccessSnackBar('User deleted!');

    }, error => {
      if (error.status === 402 || error.status === 409 || error.status === 412) {
        if (error.status === 412 && !this.user.approved) {
          this.openErrorSnackBar('The user has final orders and cannot be deleted.');
        } else {
          this.openDialog(error.status);
        }
      } else {
        this.openErrorSnackBar('The page has expired!');
      }
      this.removing = false;
    });
  }
  public openSuccessSnackBar(message: string) {
    this.snackBar.open(message, 'ok', {
      extraClasses: ['success-snack-bar'],
      duration: 5000
    });

  }

  public openErrorSnackBar(message: string) {
    this.snackBar.open(message, 'ok', {
      extraClasses: ['error-snack-bar'],
      duration: 5000
    });

  }

  public openDialog(code: number) {
    let dialogRef = this.dialog.open(UserDeletionDialog);
    dialogRef.componentInstance.code = code;
    dialogRef.afterClosed().subscribe(result => {
      if (result === 'ok') {
        this.removing = true;
        if (code === 402) {
          this.adminService.usersIdApprovePut(this.user.id, false, true).subscribe(success => {
            this.removing = false;
            this.openSuccessSnackBar('User account status changed!');
            this.user.approved = false;
          }, error => {
            this.removing = false;
            this.openErrorSnackBar('An unexpected error occured, please try again later');
          });
        } else if (code === 409) {
          this.adminService.usersIdDelete(this.user.id, true).subscribe(success => {
            this.removing = false;
            this.openSuccessSnackBar('User deleted!');
            this.userDeleted.emit();

          }, error => {
            this.openErrorSnackBar('An unexpected error occured, please try again later');
            this.removing = false;
          });
        } else if (code === 412){
          this.adminService.usersIdApprovePut(this.user.id, false, true).subscribe(success => {
            this.removing = false;
            this.openSuccessSnackBar('User account status changed!');
            this.user.approved = false;
          }, error => {
            this.removing = false;
            this.openErrorSnackBar('An unexpected error occured, please try again later');
          });
        } else {
          this.removeUser();
        }
      }
    });

  }
}

@Component({
  selector: 'app-user-deletion-dialog',
  templateUrl: './user-deletion.dialog.html',
})
export class UserDeletionDialog {
  public code: number;
  constructor(public dialogRef: MdDialogRef<UserDeletionDialog>) { }
}
