import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home/home.component';
import { AdminApi, HungryApi } from '../remote';
import { AdminRouting } from './admin.routing';
import { UserComponent } from './home/user/user.component';
import { SharedModule } from '../shared/shared.module';
import { UsersComponent } from './users/users.component';
import { GlobalSettingsComponent } from './settings/global-settings.component';
import { UserApprovalComponent } from './user-approval/user-approval.component';
import { UserApprovalDialog } from './user-approval/user-approval.component';
import { UserDeletionDialog } from './home/user/user.component';
import { TinymceDirective } from './settings/tinymce.directive';
import { AdminRoutingComponent } from './admin-routing/admin-routing.component';


@NgModule({
  imports: [
    CommonModule,
    AdminRouting,
    SharedModule
  ],
  declarations: [
    HomeComponent,
    UserComponent,
    UsersComponent,
    GlobalSettingsComponent,
    UserApprovalComponent,
    UserApprovalDialog,
    UserDeletionDialog,
    TinymceDirective,
    AdminRoutingComponent
  ],
  entryComponents: [
    UserApprovalDialog,
    UserDeletionDialog
  ],
  providers: [
    AdminApi,
    HungryApi
  ]
})
export class AdminModule { }
