import { Routes, RouterModule } from '@angular/router';

import { CanActivateAdminGuard } from '../app.guard';

import { LoggedComponent } from '../shared/logged/logged.component';

import { UsersComponent } from './users/users.component';
import { HomeComponent } from './home/home.component';
import { GlobalSettingsComponent } from './settings/global-settings.component';

const adminRoutes: Routes = [
  {
    path: 'admin',
    canActivateChild: [CanActivateAdminGuard],
    component: LoggedComponent,
    children: [
      { path: '', component: HomeComponent },
      { path: 'users/:id', component: UsersComponent },
      { path: 'settings', component: GlobalSettingsComponent }
    ]
  }
];

export const AdminRouting = RouterModule.forChild(adminRoutes);
