import { Component, OnInit, Input } from '@angular/core';
import { MdSnackBar } from '@angular/material';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import * as remote from '../../remote';

@Component({
  selector: 'app-admin-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  // flag to indicate that data is loading
  public loadingData = true;
  // flag to indicate that submit button is pressed
  public submitting = false;
  // flag to indicate that reset button is pressed
  public reseting = false;
  // flag to indicate that all buttons are disabled
  public disableAll =false;
  // flag to indicate wether the user can be removed/disapproved
  public notRemovable = false;
  public userId = -1;
  public editUser: remote.UpdateAccountSettings = {};
  public user: remote.AccountSettings;
  public isProfileValid = false;

  constructor(private router: Router, private route: ActivatedRoute,
    private adminService: remote.AdminApi, private snackBar: MdSnackBar) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.userId = +params['id'];
    });
    // make api request and fill data
    this.update();

  }
  public onSubmit() {
    // turn on submit flag
    this.submitting = true;
    // make DTO
    this.editUser.firstName = this.user.firstName;
    this.editUser.lastName = this.user.lastName;
    this.editUser.role = this.user.role;
    this.editUser.email = this.user.email;
    this.editUser.version = this.user.version;
    // do the api call
    this.adminService.usersIdPut(this.userId, this.editUser).subscribe(res => {
      this.openSuccessSnackBar('Changes saved succesfully');
      this.navigateToHome();
    }, error => {
      if (error.status === 412) {
        this.openErrorSnackBar('A user with the given e-mail already exists');
      } else if (error.status === 409) {
        this.openErrorSnackBar('The page has expired. Please try again.');
        this.updateUser(JSON.parse(error._body));
      } else {
        this.openErrorSnackBar('An unexpected error occured, please try again later');
        this.update();
      }
      this.submitting = false;
    }, () => { this.submitting = false; });
  }
  public resetPassword() {
    this.reseting = true;
    this.adminService.usersIdForgotpwdPost(this.userId).subscribe(res => {
      this.reseting = false;
      this.user.version += 1;
      this.openSuccessSnackBar('Password reseted');
    }, error => {
      this.openErrorSnackBar('An unexprected error occured, please try again later');
      this.reseting = false;
    });
  }
  public navigateToHome() {
    this.router.navigate(['admin']);
  }
  public navigateToGlobalSetting() {
    this.router.navigate(['admin/settings']);
  }
  public openSuccessSnackBar(message: string) {
    this.snackBar.open(message, 'ok', {
      extraClasses: ['success-snack-bar'],
      duration: 5000
    });

  }
  public openErrorSnackBar(message: string) {
    this.snackBar.open(message, 'ok', {
      extraClasses: ['error-snack-bar'],
      duration: 5000
    });

  }

  public checkValid(state) {
    this.isProfileValid = state;
  }

  public update() {
    this.loadingData = true;
    this.isProfileValid = false;
    this.adminService.usersIdGet(this.userId).subscribe(user => {
      this.user = user;

    }, error => {
      this.loadingData = false;
      this.openErrorSnackBar('An unexprected error occured, please try again later');
      this.navigateToHome();
    },
      () => { this.loadingData = false; });

  }
  public updateUser(user: remote.AccountSettings) {
    this.user = user;
    this.isProfileValid = false;
  }
  public updateUseVersion(){
      this.disableAll = true;
      this.adminService.usersIdGet(this.userId).subscribe(user => {
        if (this.user.version + 1 === user.version){
          let updatedUser = user;
            updatedUser.firstName = this.user.firstName;
            updatedUser.lastName = this.user.lastName;
            updatedUser.email = this.user.email;
            this.user = updatedUser;

        }else {
          this.user = user;
        }
        this.disableAll = false;
    }, error => {
       this.disableAll = false;
      this.openErrorSnackBar('An unexprected error occured, please try again later');
      this.navigateToHome();
    });
  }
}
