import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MdSnackBar } from '@angular/material';
import { FormGroup, FormControl, Validators } from '@angular/forms';

import * as remote from '../../remote';

@Component({
  selector: 'app-admin-globalsettings',
  templateUrl: './global-settings.component.html',
  styleUrls: ['./global-settings.component.scss']
})
export class GlobalSettingsComponent implements OnInit {
  // flags
  public loadingData = true;
  public submitting = false;
  // select input options
  currencyList = ['&euro;', '&dollar;', '&pound;', '&yen;', '&#8381;', '&#8377;', '&#20803;'];

  public globalSettings: remote.GlobalSettings = {};
  public settingsForm: FormGroup;

  constructor(private router: Router, private adminService: remote.AdminApi,
    private hungryService: remote.HungryApi, private snackBar: MdSnackBar) { }

  ngOnInit() {
    this.initForm();
    this.update();
  }
  public navigateToHome() {
    this.router.navigate(['admin']);
  }

  public navigateToGlobalSetting() {
    this.router.navigate(['admin/settings']);
  }
  public onSubmit() {
    this.submitting = true;
    this.globalSettings.deadline = this.settingsForm.value.deadline;
    this.globalSettings.notes = this.settingsForm.value.notes;
    this.globalSettings.privacy = this.settingsForm.value.privacy;
    this.globalSettings.terms = this.settingsForm.value.terms;
    // make the api call
    this.adminService.globalsettingsPut(this.globalSettings).subscribe(res => {
      this.openSuccessSnackBar('Changes saved succesfully');
      this.navigateToHome();
    }, error => {
      this.submitting = false;
      if (error.status === 409) {
        this.openErrorSnackBar('The page has expired, please try again.');
        this.updateSettings(JSON.parse(error._body));
      } else {
        this.openErrorSnackBar('Unexpected error, please try again later');
        this.update();
      }
    },
      () => { this.submitting = false; });
  }

  public initForm() {
    // create FormGroup object
    this.settingsForm = new FormGroup({
      deadline: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$')
      ])),
      currency: new FormControl('', Validators.compose([
        Validators.required
      ])),
      notes: new FormControl('', Validators.compose([
        Validators.required
      ])),
      terms: new FormControl('', Validators.compose([
        Validators.required
      ])),
      privacy: new FormControl('', Validators.compose([
        Validators.required
      ]))
    });
  }

  public update() {
    this.loadingData = true;
    this.hungryService.globalsettingsGet().subscribe(settings => {
      this.globalSettings = settings;
      this.globalSettings.deadline = this.globalSettings.deadline.slice(0, 5);
      this.resetForm();
    }, error => {
      this.openErrorSnackBar('Unexpected error, please try again later'); this.navigateToHome();
    },
      () => { this.loadingData = false; });

  }
  public updateSettings(globalSettings: remote.GlobalSettings) {
    this.globalSettings = globalSettings;
    this.globalSettings.deadline = this.globalSettings.deadline.slice(0, 5);
    this.resetForm();
  }

  public resetForm() {
    this.settingsForm.markAsPristine();
    this.settingsForm.setValue({
      deadline: this.globalSettings.deadline,
      currency: this.globalSettings.currency,
      notes: this.globalSettings.notes,
      terms: this.globalSettings.terms,
      privacy: this.globalSettings.privacy
    });
  }

  public openSuccessSnackBar(message: string) {
    this.snackBar.open(message, 'ok', {
      extraClasses: ['success-snack-bar'],
      duration: 5000
    });

  }
  public openErrorSnackBar(message: string) {
    this.snackBar.open(message, 'ok', {
      extraClasses: ['error-snack-bar'],
      duration: 5000
    });

  }

  home() {
    this.router.navigate(['/']);
  }

}
