import { FormControl } from '@angular/forms';

export class NumberValidator {

  public static isInteger(control: FormControl) {

    // Restrict values <0 or >99
    if (control.value < 0) {
      control.setValue(0);
    }
    if (control.value > 99) {
      let numberArray = (control.value).toString().split('').slice(0, 2);
      let twoDigitNumber = Number((numberArray[0] + numberArray[1]));
      if (twoDigitNumber !== null || twoDigitNumber !== NaN) {
        control.setValue(twoDigitNumber);
      } else {
        control.setValue(0);
      }
    }

    // convert string to number
    let number: number = Math.floor(control.value);

    // check if it has decimals
    let hasDecimals: boolean = control.value % 1 > 0 ? true : false;

    // get result of isInteger()
    let integer: boolean = Number.isInteger(number);

    // validate conditions
    let valid: boolean = !hasDecimals && integer && number >= 0 && number <= 99;

    if (!valid) {
      return { isInteger: true };
    }
    return null;
  }
}
