import { Food } from '../../../remote/model/Food';

export class Foods {

  foods: Map<number, Food> = new Map<number, Food>();

  addFood(food: Food): void {
    this.foods.set(food.id, food);
  }

  removeFood(foodId: number): void {
    this.foods.delete(foodId);
  }

  getFood(foodId: number): Food {
    return this.foods.get(foodId);
  }

  removeAll(): void {
    this.foods.clear();
  }

  getFoodsAsArray(): Array<Food> {
    let foodsArray: Array<Food> = new Array<Food>();
    this.foods.forEach((value: Food) => {
      foodsArray.push(value);
    });
    return foodsArray;
  }

  size(): number {
    return this.foods.size;
  }

}
