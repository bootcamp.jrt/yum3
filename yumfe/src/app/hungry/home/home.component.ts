import { Component, OnInit, AfterViewChecked, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MdSnackBar } from '@angular/material';

import * as remote from '../../remote';
import { GlobalSettingsService } from '../../shared/globalSettings.service';

@Component({
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, AfterViewChecked, OnDestroy {

  private week: number;
  private year: number;
  private mondayOfWeek: Date;
  private routeFragmentSubscription;
  private redirectToElement: string = '';

  public datesOfWeek: Date[] = [];
  public dailyMenusMap: Map<number, remote.DailyMenu> = new Map();
  public currentWeek: boolean = false;
  public weeklyTotalMap: Map<number, number> = new Map();
  public weeklyTotal: number = 0;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private hungryService: remote.HungryApi,
    public globalSettingsService: GlobalSettingsService,
    public snackBar: MdSnackBar
  ) { }

  ngOnInit() {

    this.route.params.subscribe(params => {

      if (params['year'] === undefined && params['week'] === undefined) {
        // Undefined parameters
        this.menusWeeklyGet();
        this.week = this.getWeek(new Date());
        this.year = this.getWeekYear(new Date());
        this.mondayOfWeek = this.getDateOfISOWeek(this.week, this.year);
        this.currentWeek = true;

      } else if (!Number(params['year']) || !Number(params['week'])
        || params['year'].length !== 4 || params['week'].length !== 2
        || params['year'] < 1900 || params['week'] < 1 || params['week'] > 53) {
        // Wrong parameters
        this.router.navigate(['hungry']);

      } else if (Number(params['year']) === this.getWeekYear(new Date())
        && Number(params['week']) === this.getWeek(new Date())
        && !(/#/.test(self.location.href))) {
        // Parameters refer to current week and year
        // and there is no fragment in the url
        this.router.navigate(['hungry']);

      } else {
        // Valid parameters
        this.menusWeeklyWeekYearGet(params['week'] + '-' + params['year']);
        this.week = +params['week'];
        this.year = +params['year'];
        this.mondayOfWeek = this.getDateOfISOWeek(this.week, this.year);
        this.currentWeek = false;
      }

    });

  }

  // After View is loaded, navigate to appropriate route fragment
  ngAfterViewChecked() {
    this.routeFragmentSubscription = this.route.fragment
      .subscribe(fragment => {
        if (fragment && this.redirectToElement !== fragment) {
          let element = document.getElementById(fragment);
          if (element) {
            element.scrollIntoView();
            this.redirectToElement = fragment;
          }
        }
      });
  }

  // Unsubscribe from route fragments to avoid multiple subscriptions
  ngOnDestroy() {

    this.routeFragmentSubscription.unsubscribe();

  }

  // Requests current week's menus
  menusWeeklyGet(): void {

    this.hungryService.menusWeeklyGet().subscribe(dailymenus => {

      this.dailymenusProcessor(dailymenus);

    }, error => {
      this.openErrorSnackBar('An error occurred', 'ok');
    });

  }

  // Requests the preffered week's menus
  menusWeeklyWeekYearGet(weekYear: string): void {

    this.hungryService.menusWeeklyWeekYearGet(weekYear).subscribe(dailymenus => {

      this.dailymenusProcessor(dailymenus);

    }, error => {
      this.openErrorSnackBar('An error occurred', 'ok');
    });

  }

  // Populates a Map of dailymenus and an array of the current week's days
  dailymenusProcessor(dailymenus: remote.DailyMenus): void {

    // Populate dailyMenusMap
    for (let dailymenu of dailymenus) {

      let dateKey = new Date(dailymenu.date);
      dateKey.setHours(0);
      this.dailyMenusMap.set(dateKey.getTime(), dailymenu);

      // Calculate and store daily menu total order price
      let dailyTotal = 0;
      for (const food of dailymenu.foods) {
        dailyTotal += food.quantity * food.price;
      }
      this.weeklyTotalMap.set(dateKey.getTime(), dailyTotal);
      this.weeklyTotal += dailyTotal;

    };

    // Populate dates of week
    this.datesOfWeek.splice(0); // Remove all items
    this.datesOfWeek.push(this.mondayOfWeek);
    for (let i = 1; i < 5; i++) {
      this.datesOfWeek.push(new Date(
        this.datesOfWeek[0].getFullYear(),
        this.datesOfWeek[0].getMonth(),
        this.datesOfWeek[0].getDate() + i)
      );
    }

  }

  // Catch updates on a daily menu's order total and updates the weekly total
  updateDailyTotal(dailyTotal: any) {

    let dateKey = new Date(dailyTotal.date);
    dateKey.setHours(0);

    this.weeklyTotal -= this.weeklyTotalMap.get(dateKey.getTime());
    this.weeklyTotalMap.set(dateKey.getTime(), dailyTotal.total);
    this.weeklyTotal += this.weeklyTotalMap.get(dateKey.getTime());

  }

  // Returns the ISO week of the date.
  getWeek(dt: Date): number {

    let date = new Date(dt.getTime());
    date.setHours(0, 0, 0, 0);
    // Thursday in current week decides the year.
    date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
    // January 4 is always in week 1.
    let week1 = new Date(date.getFullYear(), 0, 4);
    // Adjust to Thursday in week 1 and count number of weeks from date to week1.
    return 1 + Math.round(((date.getTime() - week1.getTime()) / 86400000
      - 3 + (week1.getDay() + 6) % 7) / 7);

  };

  // Returns the four-digit year corresponding to the ISO week of the date.
  getWeekYear(dt: Date): number {

    let date = new Date(dt.getTime());
    date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
    return date.getFullYear();

  };

  // Returns the the first day (Monday) of the given week-year
  getDateOfISOWeek(w, y): Date {

    let simple = new Date(y, 0, 1 + (w - 1) * 7);
    let dow = simple.getDay();
    let ISOweekStart = simple;
    if (dow <= 4) {
      ISOweekStart.setDate(simple.getDate() - simple.getDay() + 1);
    } else {
      ISOweekStart.setDate(simple.getDate() + 8 - simple.getDay());
    }
    return ISOweekStart;

  }

  // Navigates to another week
  changeWeek(value: number): void {

    let weekToRequest: string;
    let yearToRequest: number;

    if (value === 0) {
      // Navigate to current week
      this.router.navigate(['hungry']);

    } else if (value === -1) {
      // Navigate to previous week
      if (this.week === 1) {
        weekToRequest = this.getWeek(new Date(this.year - 1, 11, 28)).toString();
        yearToRequest = this.year - 1;
        this.router.navigate(['hungry', yearToRequest, weekToRequest]);
      } else if (this.week <= 10) {
        weekToRequest = '0' + (this.week - 1);
        yearToRequest = this.year;
        this.router.navigate(['hungry', yearToRequest, weekToRequest]);
      } else {
        weekToRequest = '' + (this.week - 1);
        yearToRequest = this.year;
        this.router.navigate(['hungry', yearToRequest, weekToRequest]);
      }

    } else if (value === 1) {
      // Navigate to next week
      if (this.week === this.getWeek(new Date(this.year, 11, 28))) {
        weekToRequest = '01';
        yearToRequest = this.year + 1;
        this.router.navigate(['hungry', yearToRequest, weekToRequest]);
      } else if (this.week < 9) {
        weekToRequest = '0' + (this.week + 1);
        yearToRequest = this.year;
        this.router.navigate(['hungry', yearToRequest, weekToRequest]);
      } else {
        weekToRequest = '' + (this.week + 1);
        yearToRequest = this.year;
        this.router.navigate(['hungry', yearToRequest, weekToRequest]);
      }

    } else {
      // Wrong parameter - redirect to current week
      this.router.navigate(['hungry']);
    }

  }

  // Opens an Error SnackBar with the requested message and action
  openErrorSnackBar(message: string, action: string): void {

    this.snackBar.open(message, action, {
      extraClasses: ['error-snack-bar']
    });

  }

}
