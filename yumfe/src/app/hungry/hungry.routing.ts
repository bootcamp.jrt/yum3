import { Routes, RouterModule } from '@angular/router';

import { IsLoggedInGuard } from '../app.guard'
import { LoggedComponent } from '../shared/logged/logged.component';

import { HomeComponent } from './home/home.component';
import { HistoryComponent } from './history/history.component';

const hungryRoutes: Routes = [
  {
    path: 'hungry',
    canActivateChild: [IsLoggedInGuard],
    component: LoggedComponent,
    children: [
      { path: '', component: HomeComponent },
      { path: ':year/:week', component: HomeComponent },
      { path: 'history', component: HistoryComponent },
      { path: 'history/:year/:month', component: HistoryComponent }
    ]
  }
];

export const HungryRouting = RouterModule.forChild(hungryRoutes);
