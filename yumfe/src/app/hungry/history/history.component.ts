import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { MdSnackBar } from '@angular/material';

import * as remote from '../../remote';
import { MonthlyGridCalculator } from '../../shared/monthly-grid-calculator';
import { GlobalSettingsService } from '../../shared/globalSettings.service';

@Component({
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit {

  private monthlyGridCalculator: MonthlyGridCalculator = new MonthlyGridCalculator();

  public month: number = new Date().getMonth() + 1;
  public year: number = new Date().getFullYear();
  public basePath: string = 'hungry/history';
  public viewMonth: Date;
  public daysInGrid: Array<Date>;
  public dailyMenusMap: Map<number, remote.DailyMenu> = new Map();
  public monthlyTotal: number = 0;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private hungryService: remote.HungryApi,
    public globalSettingsService: GlobalSettingsService,
    public snackBar: MdSnackBar
  ) { }

  ngOnInit() {

    this.route.params.subscribe(params => {

      if (params['year'] === undefined && params['month'] === undefined) {
        // Undefined parameters
        this.menusMonthlyGet();
        this.month = new Date().getMonth();
        this.year = new Date().getFullYear();

      } else if (!Number(params['year']) || !Number(params['month'])
        || params['year'].length !== 4 || params['month'].length !== 2
        || params['year'] < 1900 || params['month'] < 1 || params['month'] > 12) {
        // Wrong parameters
        this.router.navigate(['hungry/history']);

      } else if (Number(params['year']) === new Date().getFullYear()
        && Number(params['month']) === (new Date().getMonth() + 1)) {
        // Parameters refer to current month and year
        this.router.navigate(['/hungry/history']);

      } else {
        // Valid parameters
        this.menusMonthlyMonthYearGet(params['month'] + '-' + params['year']);
        this.month = +params['month'] - 1;
        this.year = +params['year'];
      }

      this.viewMonth = new Date(this.year, this.month);

    });

  }

  // Requests current month's menus
  menusMonthlyGet(): void {

    this.hungryService.menusMonthlyGet().subscribe(dailymenus => {

      this.dailymenusProcessor(dailymenus);

    }, error => {
      this.openErrorSnackBar('An error occurred', 'ok');
    });

  }

  // Requests the preffered month's menus
  menusMonthlyMonthYearGet(weekYear: string): void {

    this.hungryService.menusMonthlyMonthYearGet(weekYear).subscribe(dailymenus => {

      this.dailymenusProcessor(dailymenus);

    }, error => {
      this.openErrorSnackBar('An error occurred', 'ok');
    });

  }

  // Populates a Map of dailymenus and an array of the current month's days
  dailymenusProcessor(dailymenus: remote.DailyMenus): void {

    // Populate dailyMenusMap
    for (let dailymenu of dailymenus) {

      let dateKey = new Date(dailymenu.date);
      dateKey.setHours(0);
      this.dailyMenusMap.set(dateKey.getTime(), dailymenu);

      // Calculate and store daily menu total order price
      for (const food of dailymenu.foods) {
        this.monthlyTotal += food.quantity * food.price;
      }

    };

    // Populate dates of month (weekly view)
    this.daysInGrid = this.monthlyGridCalculator.setDaysInGrid(new Date(this.year, this.month));

  }

  // Opens an Error SnackBar with the requested message and action
  openErrorSnackBar(message: string, action: string): void {

    this.snackBar.open(message, action, {
      extraClasses: ['error-snack-bar']
    });

  }

}
