import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

import * as remote from '../../../remote';
import { GlobalSettingsService } from '../../../shared/globalSettings.service';
import { CustomIcons } from '../../../shared/custom-icons/custom-icons';

@Component({
  selector: 'app-daily-order-history',
  templateUrl: './daily-order-history.component.html',
  styleUrls: ['./daily-order-history.component.scss']
})
export class DailyOrderHistoryComponent implements OnInit {

  public customIcons: CustomIcons = new CustomIcons();

  @Input()
  public dailyMenu: remote.DailyMenu;
  @Input()
  public date: Date;
  @Input()
  public viewMonth: Date;
  public dailyTotal: number = 0;

  constructor(
    private router: Router,
    public globalSettingsService: GlobalSettingsService
  ) { }

  ngOnInit() {

    if (this.dailyMenu !== undefined) {
      // Calculate dailyTotal
      for (const food of this.dailyMenu.foods) {
        this.dailyTotal += food.quantity * food.price;
      }
    }

  }

  // Redirect to the appropriate week when the grid tile is selected
  tileRedirectToWeek() {

    if (this.dailyMenu !== undefined && this.dailyMenu.orderId > -1) {
      this.redirection();
    }

  }

  // Redirect to the appropriate week when the order button is selected
  buttonRedirectToWeek() {

    if (this.dailyMenu !== undefined && this.dailyMenu.orderId === -1) {
      this.redirection();
    }

  }

  // Make a redirection to hungry home with year, week and day parameters
  redirection() {

    this.router.navigate([
      'hungry',
      this.date.getFullYear(),
      this.getWeek(new Date(this.date))],
      { fragment: this.date.getDate().toString() });

  }

  // Returns the ISO week of the date.
  getWeek(dt: Date): number {

    let date = new Date(dt.getTime());
    date.setHours(0, 0, 0, 0);
    // Thursday in current week decides the year.
    date.setDate(date.getDate() + 3 - (date.getDay() + 6) % 7);
    // January 4 is always in week 1.
    let week1 = new Date(date.getFullYear(), 0, 4);
    // Adjust to Thursday in week 1 and count number of weeks from date to week1.
    return 1 + Math.round(((date.getTime() - week1.getTime()) / 86400000
      - 3 + (week1.getDay() + 6) % 7) / 7);

  };

}
