import { Routes, RouterModule } from '@angular/router';

import {
  AppGuard,
  CanLoginGuard,
  IsLoggedInGuard,
  CanActivateSettingsGuard
} from './app.guard';

import { NotFoundComponent } from './shared/not-found/not-found.component';
import { SettingsComponent } from './shared/settings/settings.component';

import { LoginComponent } from './anon/login/login.component';
import { RegisterComponent } from './anon/register/register.component';
import { ForgotpwdComponent } from './anon/forgotpwd/forgotpwd.component';
import { ChangepwdComponent } from './anon/changepwd/changepwd.component';


const appRoutes: Routes = [
  { path: '', component: NotFoundComponent, pathMatch: 'full', canActivate: [AppGuard] },
  { path: 'settings', component: SettingsComponent, canActivate: [CanActivateSettingsGuard] },
  { path: 'forgotpwd', component: ForgotpwdComponent },
  { path: 'changepwd/:secret', component: ChangepwdComponent },
  { path: 'login', component: LoginComponent, canActivate: [CanLoginGuard] },
  { path: 'register', component: RegisterComponent },
  { path: '**', component: NotFoundComponent }, //always last
];

export const AppRouting = RouterModule.forRoot(appRoutes);
