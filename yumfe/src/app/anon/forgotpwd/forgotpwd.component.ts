import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { MdDialog, MdDialogRef, MdSnackBar } from '@angular/material';

import * as remote from '../../remote';


@Component({
  selector: 'app-forgotpwd',
  templateUrl: './forgotpwd.component.html',
  styleUrls: ['./forgotpwd.component.scss']
})
export class ForgotpwdComponent implements OnInit {

  public forgotpwdForm: FormGroup;
  public loading = false;


  constructor(
    private authService: remote.AuthApi,
    private router: Router,
    public dialog: MdDialog,
    public snackBar: MdSnackBar
  ) { }

  ngOnInit() {

    this.forgotpwdForm = new FormGroup({
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
      ]))
    });

  }

  forgotPwd() {

    this.loading = true;

    let email: remote.Email = {
      email: this.forgotpwdForm.get('email').value
    };

    this.authService.authForgotpwdPost(email).subscribe(() => {

      this.loading = false;

      // Show success dialog
      let dialogRef = this.dialog.open(ForgotPasswordDialog);

      dialogRef.afterClosed().subscribe( () =>
        this.router.navigate(['/login'])
      );

    }, error => {
      this.loading = false;
      if (error.status === 404) {
        // Display SnackBar
        this.openErrorSnackBar('Email not found', 'ok');
      } else {
        this.openErrorSnackBar('Invalid email', 'ok');
      }
    });

  }

  // Opens an Error SnackBar with the requested message and action
  openErrorSnackBar(message: string, action: string): void {

    this.snackBar.open(message, action, {
      extraClasses: ['error-snack-bar']
    });

  }

  gotoLogin() {

    this.router.navigate(['/login']);

  }

}

@Component({
  templateUrl: './dialog.html',
})
export class ForgotPasswordDialog {
  constructor(public dialogRef: MdDialogRef<ForgotPasswordDialog>) { }
}
