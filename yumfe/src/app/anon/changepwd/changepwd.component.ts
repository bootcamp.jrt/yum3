import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { FormControl, Validators, FormGroup } from '@angular/forms';
import { MdSnackBar } from '@angular/material';

import * as remote from '../../remote';

@Component({
  selector: 'app-changepwd',
  templateUrl: './changepwd.component.html',
  styleUrls: ['./changepwd.component.scss']
})
export class ChangepwdComponent implements OnInit {

  public changepwdForm: FormGroup;
  public loading = false;
  public blurred = false;
  public disableSubmit = true;
  private secret = '';

  constructor(
    private authService: remote.AuthApi,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    public snackBar: MdSnackBar
  ) { }

  ngOnInit() {

    /* Get the query params from URL
     * e.g: changepwd?secret=d3s-da8qwc23-awe93
     */
    this.activatedRoute.params.subscribe((params: Params) => {

      this.secret = params['secret'];

    });


    this.changepwdForm = new FormGroup({
      password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(25)
      ])),
      passwordConfirm: new FormControl({ value: '', disabled: true }, Validators.compose([
        Validators.required,
        this.validateStart,
        this.validateEqual
      ]))
    });

    this.changepwdForm.get('passwordConfirm').valueChanges.subscribe(value => {
      if (this.changepwdForm.get('passwordConfirm').value !== null) {
        this.changepwdForm.get('passwordConfirm').markAsTouched();
        this.disableSubmit = false;
      }
    });

    this.changepwdForm.get('password').valueChanges.subscribe(value => {
      this.toggleConfirmDisabled();
    });

  }

  changePwd() {

    this.loading = true;

    let pwd1 = this.changepwdForm.get('password').value;
    let pwd2 = this.changepwdForm.get('passwordConfirm').value;

    if (pwd1 !== pwd2) {
      this.openErrorSnackBar('Passwords don\'t match', 'ok');
      this.loading = false;
    } else {
      let changePassword: remote.ChangePassword = {
        password: pwd1,
        secret: this.secret
      };

      this.authService.authChangepwdPut(changePassword).subscribe(() => {

        this.openSuccessSnackBar('Password changed successfully', 'ok');
        this.router.navigate(['/login']);

      }, error => {
        this.loading = false;
        this.openErrorSnackBar('Invalid data', 'ok');
      });
    }
  }

  // Custom validator of password confirmation
  public validateStart(c: FormControl) {
    let mainPassword: string = c.root.get('password').value;
    return (mainPassword.startsWith(c.value)) ? null : {
      validateStart: {
        valid: false
      }
    };
  }

  // Custom validator of password confirmation
  public validateEqual(c: FormControl) {
    let mainPassword: string = c.root.get('password').value;
    return (mainPassword === (c.value)) ? null : {
      validateEqual: {
        valid: false
      }
    };
  }

  // Method to toggle the disabled property of password confirmation input
  public toggleConfirmDisabled() {
    if (this.changepwdForm.get('password').valid) {
      this.changepwdForm.get('passwordConfirm').enable();
    } else {
      this.disableSubmit = true;
      this.changepwdForm.get('passwordConfirm').setValue(null);
      this.changepwdForm.get('passwordConfirm').disable();
      this.changepwdForm.get('passwordConfirm').reset();
    }

  }

  // Opens a Success SnackBar with the requested message and action
  openSuccessSnackBar(message: string, action: string): void {

    this.snackBar.open(message, action, {
      extraClasses: ['success-snack-bar'],
      duration: 5000
    });

  }

  // Opens an Error SnackBar with the requested message and action
  openErrorSnackBar(message: string, action: string): void {

    this.snackBar.open(message, action, {
      extraClasses: ['error-snack-bar']
    });

  }

  gotoLogin() {

    this.router.navigate(['/login']);

  }

}
