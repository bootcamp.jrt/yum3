import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MdSnackBar } from '@angular/material';

import { AuthenticationService } from '../../shared/authentication.service';
import { TosService } from '../../shared/tos.service';

import * as remote from '../../remote';

@Component({
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {

  public registrationForm: FormGroup;
  public loading = false;
  public blurred = false;
  public disableSubmit = true;

  constructor(
    private router: Router,
    private authService: remote.AuthApi,
    public tosService: TosService,
    public snackBar: MdSnackBar
  ) { }

  ngOnInit() {

    this.registrationForm = new FormGroup({
      firstName: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z  .-]+$'),
        Validators.minLength(2),
        Validators.maxLength(25)
      ])),
      lastName: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern('^[a-zA-Z  .-]+$'),
        Validators.minLength(2),
        Validators.maxLength(25)
      ])),
      email: new FormControl('', Validators.compose([
        Validators.required,
        Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)
      ])),
      password: new FormControl('', Validators.compose([
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(25)
      ])),
      passwordConfirm: new FormControl({ value: '', disabled: true }, Validators.compose([
        Validators.required,
        this.validateStart,
        this.validateEqual
      ]))
    });


    this.registrationForm.get('passwordConfirm').valueChanges.subscribe(value => {
      if (this.registrationForm.get('passwordConfirm').value !== null) {
        this.registrationForm.get('passwordConfirm').markAsTouched();
        this.disableSubmit = false;
      }
    });

    this.registrationForm.get('password').valueChanges.subscribe(value => {
      this.toggleConfirmDisabled();
    });

  }

  register() {

    this.loading = true;

    let accountDetails: remote.AccountDetails = {
      firstName: this.registrationForm.get('firstName').value,
      lastName: this.registrationForm.get('lastName').value,
      email: this.registrationForm.get('email').value,
      password: this.registrationForm.get('password').value,
      version: 0 // dummy
    };

    this.authService.authRegisterPost(accountDetails).subscribe(() => {

      this.openSuccessSnackBar('Registration successful! You will receive an email once your account is approved.', 'ok');
      this.router.navigate(['/login']);

    }, error => {
      this.loading = false;
      if (error.status === 400) {
        this.openErrorSnackBar('You entered invalid data. Please fix them and retry', 'ok');
      } else if (error.status === 403) {
        this.openErrorSnackBar('Email already taken. Please enter a new one.', 'ok');
        this.registrationForm.get('email').setErrors({ pattern: true });
      }
    });
  }

  gotoLogin() {

    this.router.navigate(['/login']);

  }

  /*
   * if flag === true focus tab on
   * terms, else on policy
   */
  displayTermsAndPolicy(flag) {

    this.tosService.displayTos(flag);

  }

  // Custom validator of password confirmation
  public validateStart(c: FormControl) {
    let mainPassword: string = c.root.get('password').value;
    return (mainPassword.startsWith(c.value)) ? null : {
      validateStart: {
        valid: false
      }
    };
  }

  // Custom validator of password confirmation
  public validateEqual(c: FormControl) {
    let mainPassword: string = c.root.get('password').value;
    return (mainPassword === (c.value)) ? null : {
      validateEqual: {
        valid: false
      }
    };
  }

  // Method to toggle the disabled property of password confirmation input
  public toggleConfirmDisabled() {
    if (this.registrationForm.get('password').valid) {
      this.registrationForm.get('passwordConfirm').enable();
    } else {
      this.disableSubmit = true;
      this.registrationForm.get('passwordConfirm').setValue(null);
      this.registrationForm.get('passwordConfirm').disable();
      this.registrationForm.get('passwordConfirm').reset();
    }

  }

  // Opens a Success SnackBar with the requested message and action
  openSuccessSnackBar(message: string, action: string): void {

    this.snackBar.open(message, action, {
      extraClasses: ['success-snack-bar']
    });

  }

  // Opens an Error SnackBar with the requested message and action
  openErrorSnackBar(message: string, action: string): void {

    this.snackBar.open(message, action, {
      extraClasses: ['error-snack-bar']
    });

  }

}
