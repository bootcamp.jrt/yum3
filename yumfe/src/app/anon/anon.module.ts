import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { RegisterComponent } from './register/register.component';
import { LoginComponent } from './login/login.component';
import { ForgotpwdComponent } from './forgotpwd/forgotpwd.component';
import { ChangepwdComponent } from './changepwd/changepwd.component';
import { ForgotPasswordDialog } from './forgotpwd/forgotpwd.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    LoginComponent
  ],
  declarations: [
    RegisterComponent,
    LoginComponent,
    ForgotpwdComponent,
    ChangepwdComponent,
    ForgotPasswordDialog
  ],
  bootstrap: [ForgotPasswordDialog],
})
export class AnonModule { }
